<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Govihar a Travel Agency Category Flat bootstrap Responsive website Template | Products :: w3layouts</title>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Govihar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<link href="{{url('/')}}/src/govihar/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="{{url('/')}}/src/govihar/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="{{url('/')}}/src/govihar/css/flexslider.css" type="text/css" media="screen" />
<link type="text/css" rel="stylesheet" href="{{url('/')}}/src/govihar/css/JFFormStyle-1.css" />
<!-- js -->
<script src="{{url('/')}}/src/govihar/js/jquery.min.js"></script>
<script src="{{url('/')}}/src/govihar/js/modernizr.custom.js"></script>
<!-- //js -->
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //fonts -->	
<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
		});
	</script>
<!--pop-up-->
<script src="{{url('/')}}/src/govihar/js/menu_jquery.js"></script>
<!--//pop-up-->	
</head>
<body>
	<!--header-->
	<div class="header">
		<div class="container">
			<div class="header-grids">
				<div class="logo">
					<h1><a  href="index.html"><span>Go</span>vihar</a></h1>
				</div>
				<!--navbar-header-->
				<div class="header-dropdown">
					<div class="emergency-grid">
						<ul>
							<li>Toll Free : </li>
							<li class="call">+1 234 567 8901</li>
						</ul>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="nav-top">
				<div class="top-nav">
					<span class="menu"><img src="{{url('/')}}/src/govihar/images/menu.png" alt="" /></span>
					<ul class="nav1">
						<li><a href="index.html">Flights</a></li>
						<li><a href="hotels.html">Hotels</a></li>
						<li><a href="holidays.html">Holidays</a></li>
						<li><a href="flights-hotels.html">Flight+Hotel</a></li>
						<li><a href="bus.html">Bus</a></li>
						<li><a href="trains.html">Trains</a></li>
						<li><a href="weekend.html">Weekend Getaways</a></li>
						<li><a href="deals.html">Deals</a></li>
					</ul>
					<div class="clearfix"> </div>
					<!-- script-for-menu -->
							 <script> 
							   $( "span.menu" ).click(function() {
								 $( "ul.nav1" ).slideToggle( 300, function() {
								 // Animation complete.
								  });
								 });
							
							</script>
						<!-- /script-for-menu -->
				</div>
				<div class="dropdown-grids">
						<div id="loginContainer"><a href="#" id="loginButton"><span>Login</span></a>
							<div id="loginBox">                
								<form id="loginForm">
									<div class="login-grids">
										<div class="login-grid-left">
											<fieldset id="body">
												<fieldset>
													<label for="email">Email Address</label>
													<input type="text" name="email" id="email">
												</fieldset>
												<fieldset>
													<label for="password">Password</label>
													<input type="password" name="password" id="password">
												</fieldset>
												<input type="submit" id="login" value="Sign in">
												<label for="checkbox"><input type="checkbox" id="checkbox"> <i>Remember me</i></label>
											</fieldset>
											<span><a href="#">Forgot your password?</a></span>
											<div class="or-grid">
												<p>OR</p>
											</div>
											<div class="social-sits">
												<div class="facebook-button">
													<a href="#">Connect with Facebook</a>
												</div>
												<div class="chrome-button">
													<a href="#">Connect with Google</a>
												</div>
												<div class="button-bottom">
													<p>New account? <a href="signup.html">Signup</a></p>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--//header-->
	<!-- banner-bottom -->
	<div class="banner-bottom">
		<!-- container -->
		<div class="container">
			<div class="faqs-top-grids">
				<div class="product-grids">
					<div class="col-md-3 product-left">
						<div class="h-class">
							<h5>Hotel Class</h5>
							<div class="hotel-price">
								<label class="cekbok">
									<input type="checkbox">
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="starTextLabel">5 Stars </span>
								</label>
							</div>
							<div class="hotel-price">
								<label class="check">
									<input type="checkbox" data-track="HOT:SR:StarRating:5Star">
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="starTextLabel">4 Stars </span>
								</label>
							</div>
							<div class="hotel-price">
								<label class="check">
									<input type="checkbox">
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="starTextLabel">3 Stars</span>
								</label>
							</div>
							<div class="hotel-price">
								<label class="check">
									<input type="checkbox">
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="starTextLabel">2 Stars </span>
								</label>
							</div>
							<div class="hotel-price">
								<label class="check">
									<input type="checkbox">
									<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									<span class="starTextLabel">1 Stars </span>
								</label>
							</div>
						</div>


						<script>


						$(document).ready(function(){
						 	//alert("it works");
                            /*$.ajax({

                                url: "{{URL::to('testAjax')}}",
                                type: "get",
                                dataType: "JSON",
                                beforeSend: function() {

                                    //alert("it works");
                                },
                                success: function(response){
                                    //alert("it works");

                                    var len = response.length;
                                    for(var i=0; i<len; i++) {
                                        var hotel_name = response[i].hotel_name;
                                        var rating = response[i].rating;


                                        var tr_str='hotel name : '+ hotel_name +' rating :'+rating;
                                        //$("#produkTampung").append(tr_str);




                                       var tr_str2='<div class="product-right-grids"> <div class="product-right-top"> <div class="p-left"> <div class="p-right-img"><a href="p-single.html"></a></div></div><div class="p-right"> <div class="col-md-6 p-right-left"> <a href="p-single.html">'+hotel_name+'</a> <div class="p-right-price"> <span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span> </div> <p>Bangkok</p> <p class="p-call">+1 234 567 890</p> </div> <div class="col-md-6 p-right-right"> <h6>Rating : 4.1/5</h6> <p>(123) Views</p> <span class="p-offer"> $25</span><span class="p-last-price">$230</span> </div> <div class="clearfix"> </div> </div> <div class="clearfix"> </div> </div> </div>';
                                        $("#produkTampung").append(tr_str2);

                                    }




                                }

                            });
*/


                        });
                        $(".cekbok").click(function(){
                            //alert("it works");
                            $(".toRemove").remove();
                            $.ajax({

                                url: "{{URL::to('testAjax')}}",
                                type: "get",
                                dataType: "JSON",
                                beforeSend: function() {

                                    //alert("it works");
                                },
                                success: function(response){
                                    //alert("it works");

                                    var len = response.length;
                                    for(var i=0; i<len; i++) {
                                        var hotel_name = response[i].hotel_name;
                                        var rating = response[i].rating;


                                        var tr_str='hotel name : '+ hotel_name +' rating :'+rating;
                                        //$("#produkTampung").append(tr_str);




                                        var tr_str2='<div class="product-right-grids"> <div class="product-right-top"> <div class="p-left"> <div class="p-right-img"><a href="p-single.html"></a></div></div><div class="p-right"> <div class="col-md-6 p-right-left"> <a href="p-single.html">'+hotel_name+'</a> <div class="p-right-price"> <span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span> </div> <p>Bangkok</p> <p class="p-call">+1 234 567 890</p> </div> <div class="col-md-6 p-right-right"> <h6>Rating : 4.1/5</h6> <p>(123) Views</p> <span class="p-offer"> $25</span><span class="p-last-price">$230</span> </div> <div class="clearfix"> </div> </div> <div class="clearfix"> </div> </div> </div>';
                                        $("#produkTampung").append(tr_str2);

                                    }




                                }

                            });



                        });


                                </script>
						<div class="h-class p-day">
							<h5>Facility Type</h5>

							@foreach ($data_hotel_facility as $facility)
							<div class="hotel-price">
								<label class="check">
									<input type="checkbox"  data-track="HOT:SR:StarRating:5Star">
									<span class="p-day-grid">{{$facility->facility_name}} </span>
								</label>
							</div>
							@endforeach
						</div>
					</div>
					<div class="col-md-9 product-right">
						<div id="produkTampung">


						</div>
						<h3 class="toRemove">Total Hotel Found : {{$data_total_hotel}}</h3>
						<br/>

						@foreach ($data_list_hotel as $hotel)
							<div class="toRemove">

						<div class="product-right-grids">
							<div class="product-right-top">
								<div class="p-left">
									<div class="p-right-img">
										<a href="p-single.html"> </a>
									</div>
								</div>
								<div class="p-right">
									<div class="col-md-6 p-right-left">
										<a href="p-single.html">{{$hotel->hotel_name}}</a>
										<div class="p-right-price">
                                            <?php for($i=1;$i<=$hotel->rating;$i++)
                                            {?>
												<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            <?php
                                            }
                                            ?>


										</div>
										<p>{{$hotel->hotel_address}}</p>
										<p class="p-call">+1 234 567 890</p>
									</div>
									<div class="col-md-6 p-right-right">
										<h6>Rating : 4.1/5</h6>
										<p>(123) Views</p>
										<span class="p-offer">$ <?php
                                            $getprice=new \App\Helpers\PriceHelp();
                                            $cheapestPrice=$getprice->cheapestPrice($hotel->id);
                                            echo $cheapestPrice;

                                            ?></span><span class="p-last-price">$230</span>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="clearfix"> </div>
							</div>	
						</div>
							</div>
						@endforeach



					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<!-- //container -->
	</div>
	<!-- //banner-bottom -->
	<!-- footer -->
	<div class="footer">
		<!-- container -->
		<div class="container">
			<div class="footer-top-grids">
				<div class="footer-grids">
					<div class="col-md-3 footer-grid">
						<h4>Our Products</h4>
						<ul>
							<li><a href="index.html">Flight Schedule</a></li>
							<li><a href="flights-hotels.html">City Airline Routes</a></li>
							<li><a href="index.html">International Flights</a></li>
							<li><a href="hotels.html">International Hotels</a></li>
							<li><a href="bus.html">Bus Booking</a></li>
							<li><a href="index.html">Domestic Airlines</a></li>
							<li><a href="holidays.html">Holidays Trip</a></li>
							<li><a href="hotels.html">Hotel Booking</a></li>
						</ul>
					</div>
					<div class="col-md-3 footer-grid">
						<h4>Company</h4>
						<ul>
							<li><a href="about.html">About Us</a></li>
							<li><a href="faqs.html">FAQs</a></li>
							<li><a href="terms.html">Terms &amp; Conditions</a></li>
							<li><a href="privacy.html">Privacy </a></li>
							<li><a href="contact.html">Contact Us</a></li>
							<li><a href="#">Careers</a></li>
							<li><a href="blog.html">Blog</a></li>
							<li><a href="booking.html">Feedback</a></li>
						</ul>
					</div>
					<div class="col-md-3 footer-grid">
						<h4>Travel Resources</h4>
						<ul>
							<li><a href="holidays.html">Holidays Packages</a></li>
							<li><a href="weekend.html">Weekend Getaways</a></li>
							<li><a href="index.html">International Airports</a></li>
							<li><a href="index.html">Domestic Flights Booking</a></li>
							<li><a href="booking.html">Customer Support</a></li>
							<li><a href="booking.html">Cancel Bookings</a></li>
							<li><a href="booking.html">Print E-tickets</a></li>
							<li><a href="booking.html">Customer Forums</a></li>
							<li><a href="booking.html">Make a Payment</a></li>
							<li><a href="booking.html">Complete Booking</a></li>
							<li><a href="booking.html">Assurance Claim</a></li>
							<li><a href="booking.html">Retail Offices</a></li>
						</ul>
					</div>
					<div class="col-md-3 footer-grid">
						<h4>More Links</h4>
						<ul class="chf_footer_list">
							<li><a href="#">Flights Discount Coupons</a></li>
							<li><a href="#">Domestic Airlines</a></li>
							<li><a href="#">Indigo Airlines</a></li>
							<li><a href="#">Air Asia</a></li>
							<li><a href="#">Jet Airways</a></li>
							<li><a href="#">SpiceJet</a></li>
							<li><a href="#">GoAir</a></li>
							<li><a href="#">Air India</a></li>
							<li><a href="#">Domestic Flight Routes</a></li>
							<li><a href="#">Indian City Flight</a></li>
							<li><a href="#">Flight Sitemap</a></li>
						</ul>
					</div>
					<div class="clearfix"> </div>
				</div>
				<!-- news-letter -->
				<div class="news-letter">
					<div class="news-letter-grids">
						<div class="col-md-4 news-letter-grid">
							<p>Toll Free No : <span>1234-5678-901</span></p>
						</div>
						<div class="col-md-4 news-letter-grid">
							<p class="mail">Email : <a href="mailto:info@example.com">mail@example.com</a></p>
						</div>
						<div class="col-md-4 news-letter-grid">
							<form>
								<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
								<input type="submit" value="Subscribe">
							</form>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<!-- //news-letter -->
			</div>
		</div>
		<!-- //container -->
	</div>
	<!-- //footer -->
	<div class="footer-bottom-grids">
		<!-- container -->
		<div class="container">
				<div class="footer-bottom-top-grids">
					<div class="col-md-4 footer-bottom-left">
						<h4>Download our mobile Apps</h4>
						<div class="d-apps">
							<ul>
								<li><a href="#"><img src="{{url('/')}}/src/govihar/images/app1.png" alt="" /></a></li>
								<li><a href="#"><img src="{{url('/')}}/src/govihar/images/app2.png" alt="" /></a></a></li>
								<li><a href="#"><img src="{{url('/')}}/src/govihar/images/app3.png" alt="" /></a></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4 footer-bottom-left">
						<h4>We Accept</h4>
						<div class="a-cards">
							<ul>
								<li><a href="#"><img src="{{url('/')}}/src/govihar/images/c1.png" alt="" /></a></li>
								<li><a href="#"><img src="{{url('/')}}/src/govihar/images/c2.png" alt="" /></a></a></li>
								<li><a href="#"><img src="{{url('/')}}/src/govihar/images/c3.png" alt="" /></a></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4 footer-bottom-left">
						<h4>Follow Us</h4>
						<div class="social">
							<ul>
								<li><a href="#" class="facebook"> </a></li>
								<li><a href="#" class="facebook twitter"> </a></li>
								<li><a href="#" class="facebook chrome"> </a></li>
								<li><a href="#" class="facebook dribbble"> </a></li>
							</ul>
						</div>
					</div>
					<div class="clearfix"> </div>
					<div class="copyright">
						<p>Copyrights © 2015 Govihar . Design by <a href="http://w3layouts.com/">W3layouts</a></p>
					</div>
				</div>
		</div>
	</div>
	<script defer src="{{url('/')}}/src/govihar/js/jquery.flexslider.js"></script>
	<script src="{{url('/')}}/src/govihar/js/easyResponsiveTabs.js" type="text/javascript"></script>
	<script src="{{url('/')}}/src/govihar/js/jquery-ui.js"></script>
	<script type="text/javascript" src="{{url('/')}}/src/govihar/js/script.js"></script>	
</body>
</html>