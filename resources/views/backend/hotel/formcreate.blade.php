@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Hotel</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/hotel">Hotel List</a>
                    </li>
                    <li class="active">
                        Add Hotel
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Add Hotel</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('hotel.store')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('id_from_mg') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">ID from MG</label>
                                    <div class="col-md-8">
                                        <input type="text" name="id_from_mg" id="id_from_mg" class="form-control readonly" placeholder="ID from MG (exp: WSASTHBKK000087)">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('id_from_mg', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('id_to_agent') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">ID to Agent</label>
                                    <div class="col-md-8">
                                        <input type="text" name="id_to_agent" id="id_to_agent" class="form-control" placeholder="ID to Agent (exp: KOR001)">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('id_to_agent', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('hotel_name') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Hotel Name</label>
                                    <div class="col-md-8">
                                        <input type="text" name="hotel_name" id="hotel_name" class="form-control" placeholder="Hotel Name (exp: Amari, Novotel, Sheraton, etc)">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('hotel_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('rating') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Rating</label>
                                    <div class="col-md-8">
                                        <input type="number" step="0.1" name="rating" id="rating" class="form-control" placeholder="4.5">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('rating', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('latitude') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Latitude</label>
                                    <div class="col-md-8">
                                        <input type="text" name="latitude" id="latitude" class="form-control" placeholder="13.7420632511">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('longitude') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Longitude</label>
                                    <div class="col-md-8">
                                        <input type="text" name="longitude" id="longitude" class="form-control" placeholder="100.554385277">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('id_currency') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Currency</label>
                                    <div class="col-md-8">
                                        {!! Form::select('id_currency', $currency, null, ['class' => 'form-control select2','id'=>'id_currency']) !!}
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('id_currency', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop