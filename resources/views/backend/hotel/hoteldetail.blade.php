<table class="table table-striped">
<meta name="_token" content="{!! csrf_token() !!}"/>
    <tr>
        <th>#</th>
        <th>Room</th>
        <th>Address</th>
        <th>Faximile</th>
        <th>Email</th>
        <th>Website</th>
        <th>Action</th>
    </tr>
    <?php $i = 1; ?>
        @foreach($detail as $detail)
        <tr>
            <td>{{$i}}</td>
            <td>{{$detail->hotel_room}}</td>
            <td>{{$detail->hotel_address}}</td>
            <td>{{$detail->country_name}}</td>
            <td>{{$detail->hotel_email}}</td>
            <td>{{$detail->hotel_website}}</td>
            <td>
                <a href="{{ URL::to('backend/hotel/edit/'.$hotel->id. '/1/' . $detail->id) }}" class="btn-sm btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                <a href="{{ URL::to('backend/hotel/delete_detail_hotel/'.$hotel->id. '/' . $detail->id) }}" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                <a href="" data-toggle="modal" data-target="#myModal" onclick="FormDetail({{$hotel->id}},{{$detail->id}});" class="btn btn-sm btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
            </td>
        </tr>
        <?php $i++;?>
        @endforeach
</table>
<br>
        <!-- <div class="row">
            <div class="col-sm-12"> -->
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('hotel.add_detail_hotel')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('hotel_room') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Hotel Room</label>
                                    <div class="col-md-8">
                                    @if($idedit != '0')
                                        <input type="number" step="1" name="hotel_room" id="hotel_room" class="form-control" placeholder="Hotel Room" value="<?php echo $editdataitem->hotel_room; ?>">
                                    @else
                                        <input type="number" step="1" name="hotel_room" id="hotel_room" class="form-control" placeholder="Hotel Room" value="<?php echo $hotel->hotel_room; ?>">
                                    @endif
                                        <input type="hidden" name="id" id="id" class="form-control" placeholder="Name Negara" value="<?php echo $hotel->id; ?>">
                                        {!! Form::hidden('idedit',$idedit,['class'=>'form-control','id'=>'idedit','placeholder'=>'9.000']) !!}
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('hotel_room', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('hotel_address') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Hotel Address</label>
                                    <div class="col-md-8">
                                    @if($idedit != '0')
                                        <input type="text" name="hotel_address" id="hotel_address" class="form-control" placeholder="Jl. Yosodipuro No. 158 Mangkubumen Surakarta 57139" value="<?php echo $editdataitem->hotel_address; ?>">
                                    @else
                                        <input type="text" name="hotel_address" id="hotel_address" class="form-control" placeholder="Jl. Yosodipuro No. 158 Mangkubumen Surakarta 57139" value="<?php echo $hotel->hotel_address; ?>">
                                    @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('hotel_address', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('region_code') ? ' has-error' : '' }}">
                                   <label class="col-md-2 control-label">Region</label>
                                   <div class="col-md-8">
                                   @if($idedit != '0')
                                      {!! Form::select('region_code', $region, $editdataitem->region_code, ['class' => 'form-control select2','id'=>'region_code']) !!}
                                    @else
                                      {!! Form::select('region_code', $region, $hotel->region_code, ['class' => 'form-control select2','id'=>'region_code']) !!}
                                    @endif
                                   </div>
                                   <div class="col-md-8">
                                      {!! $errors->first('region_code', '<p class="help-block">:message</p>') !!}
                                   </div>
                                </div>
                                <div class="form-group {{ $errors->has('country_code') ? ' has-error' : '' }}">
                                   <label class="col-md-2 control-label">Country</label>
                                   <div class="col-md-8">
                                    @if($idedit != '0')
                                      {!! Form::select('country_code', $country, $editdataitem->country_code, ['class' => 'form-control select2','id'=>'country_code']) !!}
                                    @else
                                      {!! Form::select('country_code', $country, $hotel->country_code, ['class' => 'form-control select2','id'=>'country_code']) !!}
                                    @endif
                                   </div>
                                   <div class="col-md-8">
                                      {!! $errors->first('country_code', '<p class="help-block">:message</p>') !!}
                                   </div>
                                </div>
                                <div class="form-group {{ $errors->has('city_code') ? ' has-error' : '' }}">
                                   <label class="col-md-2 control-label">City</label>
                                   <div class="col-md-8">
                                    @if($idedit != '0')
                                      {!! Form::select('city_code', $city, $editdataitem->city_code, ['class' => 'form-control select2','id'=>'city_code']) !!}
                                    @else
                                      {!! Form::select('city_code', $city, $hotel->city_code, ['class' => 'form-control select2','id'=>'city_code']) !!}
                                    @endif
                                   </div>
                                   <div class="col-md-8">
                                      {!! $errors->first('city_code', '<p class="help-block">:message</p>') !!}
                                   </div>
                                </div>
                                <!-- <div class="form-group {{ $errors->has('area_code') ? ' has-error' : '' }}">
                                   <label class="col-md-2 control-label">Area</label>
                                   <div class="col-md-8">
                                      {!! Form::select('area_code', $area, $hotel->area_code, ['class' => 'form-control select2','id'=>'area_code']) !!}
                                   </div>
                                   <div class="col-md-8">
                                      {!! $errors->first('area_code', '<p class="help-block">:message</p>') !!}
                                   </div>
                                </div> -->
                                <div class="form-group {{ $errors->has('hotel_location') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Hotel Location</label>
                                    <div class="col-md-8">
                                    @if($idedit != '0')
                                      <input type="text" name="hotel_location" id="hotel_location" class="form-control" placeholder="Hotel Location" value="<?php echo $editdataitem->hotel_location; ?>">
                                    @else
                                      <input type="text" name="hotel_location" id="hotel_location" class="form-control" placeholder="Hotel Location" value="<?php echo $hotel->hotel_location; ?>">
                                    @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('hotel_location', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('hotel_faximile') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Hotel Faximile</label>
                                    <div class="col-md-8">
                                      @if($idedit != '0')
                                        <input type="text" name="hotel_faximile" id="hotel_faximile" class="form-control" placeholder="Hotel Faximile" value="<?php echo $editdataitem->hotel_faximile; ?>">
                                      @else
                                        <input type="text" name="hotel_faximile" id="hotel_faximile" class="form-control" placeholder="Hotel Faximile" value="<?php echo $hotel->hotel_faximile; ?>">
                                      @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('hotel_faximile', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('hotel_email') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Hotel Email</label>
                                    <div class="col-md-8">
                                      @if($idedit != '0')
                                        <input type="email" name="hotel_email" id="hotel_email" class="form-control" placeholder="korinatour@gmail.com" value="<?php echo $editdataitem->hotel_email; ?>">
                                      @else
                                        <input type="email" name="hotel_email" id="hotel_email" class="form-control" placeholder="korinatour@gmail.com" value="<?php echo $hotel->hotel_email; ?>">
                                      @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('hotel_email', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('hotel_website') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Hotel Website</label>
                                    <div class="col-md-8">
                                      @if($idedit != '0')
                                        <input type="email" name="hotel_website" id="hotel_website" class="form-control" placeholder="www.befreetour.com" value="<?php echo $editdataitem->hotel_website; ?>">
                                      @else
                                        <input type="email" name="hotel_website" id="hotel_website" class="form-control" placeholder="www.befreetour.com" value="<?php echo $hotel->hotel_website; ?>">
                                      @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('hotel_website', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="submit" class="btn btn-primary" value=<?php if($idedit != '0'){ echo "save"; } else{ echo "add"; }?>>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
       <!--      </div>
        </div> -->

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Hotel Detail Data</h4>
            </div>
            <div class="modal-body">
                <h4>Text in a modal</h4>
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>