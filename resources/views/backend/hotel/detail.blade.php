@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Hotel</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/hotel">Hotel List</a>
                    </li>
                    <li class="active">
                        Hotel Detail
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <a href="{{ URL::to('/backend/hotel') }}" class="btn btn-default waves-effect waves-light pull-right" role="button">
                            Back
                        </a>
                    <h4 class="m-t-0 header-title"><b>Hotel List</b></h4>
                    <hr>
                    <div class="table-responsive">
                        <table id="hotel" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID from MG</th>
                                    <th>ID to Agent</th>
                                    <th>Hotel Name</th>
                                    <th>Rating</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cruds as $cruds)
                                <tr>
                                    <td>{{$cruds->id_from_mg}}</td>
                                    <td>{{$cruds->id_to_agent}}</td>
                                    <td>{{$cruds->hotel_name}}</td>
                                    <td>{{$cruds->rating}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop