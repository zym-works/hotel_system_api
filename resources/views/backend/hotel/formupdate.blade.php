@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Hotel</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/hotel">Hotel List</a>
                    </li>
                    <li class="active">
                        Update Hotel
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-tabs navtab-bg nav-justified">
                <?php $tab=session('detail_tab'); ?>
                    <li <?php if(($tab==1) OR ($tab=="")){echo 'class="active"';}?>>
                        <a href="#tab_1" data-toggle="tab">
                            <span class="hidden-xs">Hotel Data</span>
                        </a>
                    </li>
                    <li <?php if($tab==2){echo 'class="active"';}?>>
                        <a href="#tab_2" data-toggle="tab">
                            <span class="hidden-xs">Hotel Detail</span>
                        </a>
                    </li>
                    <li <?php if($tab==3){echo 'class="active"';}?>>
                        <a href="#tab_3" data-toggle="tab">
                            <span class="hidden-xs">Hotel Facility</span>
                        </a>
                    </li>
                    <li <?php if($tab==4){echo 'class="active"';}?>>
                        <a href="#tab_4" data-toggle="tab">
                            <span class="hidden-xs">Room Facility</span>
                        </a>
                    </li>
                    <li <?php if($tab==5){echo 'class="active"';}?>>
                        <a href="#tab_5" data-toggle="tab">
                            <span class="hidden-xs">Hotel Images</span>
                        </a>
                    </li>
                    <li <?php if($tab==6){echo 'class="active"';}?>>
                        <a href="#tab_6" data-toggle="tab">
                            <span class="hidden-xs">Room Images</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane <?php if(($tab==1) OR ($tab=="")){ echo 'active';}?>" id="tab_1">
                        <?php session()->forget('detail_tab'); ?>
                        @include('backend.hotel.hoteldata', ['hotel'=>$hotel])
                    </div>
                    <div class="tab-pane <?php if($tab==2) { echo 'active';}?>" id="tab_2">
                        <?php session()->forget('detail_tab'); ?>
                        @include('backend.hotel.hoteldetail', ['hotel'=>$hotel])
                    </div>
                    <div class="tab-pane <?php if($tab==3) { echo 'active';}?>" id="tab_3">
                        <?php session()->forget('detail_tab'); ?>
                        @include('backend.hotel.hotelfacility', ['hotel'=>$hotel])
                    </div>
                    <div class="tab-pane <?php if($tab==4) { echo 'active';}?>" id="tab_4">
                        <?php session()->forget('detail_tab'); ?>
                        @include('backend.hotel.hotelroom', ['hotel'=>$hotel])
                    </div>
                    <div class="tab-pane <?php if($tab==5) { echo 'active';}?>" id="tab_5">
                        <?php session()->forget('detail_tab'); ?>
                        @include('backend.hotel.imagehotel', ['hotel'=>$hotel])
                    </div>
                    <div class="tab-pane <?php if($tab==6) { echo 'active';}?>" id="tab_6">
                        <?php session()->forget('detail_tab'); ?>
                        @include('backend.hotel.imageroom', ['hotel'=>$hotel])
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop