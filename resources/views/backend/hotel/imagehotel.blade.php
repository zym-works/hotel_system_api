<table class="table table-striped">
    <tr>
        <th>Image</th>
        <th>Action</th>
    </tr>
    @foreach($gambar as $x) 
        <tr>
            <td><img src="{{url('/')}}/storage/img-hotel/{{$x->hotel_name}} hotel/{{$x->name}}" style="width:10%" alt="{{$x->name}}"> </td>
            <td><a href="{{url('/backend/image/delete/').'/'.$x->id.'/'.$x->hotel_name}}" class="btn btn-warning" role="button">Delete</a></td>
        </tr>
    @endforeach
</table>    
{{-- ini --}}
{{-- <img src="{{ url('storage/img hotel/amari-boulevard-hotel/') }}/1_halo.png"> --}}

        <!-- <div class="row">
            <div class="col-sm-12"> -->
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form method="POST" action="{{url('hotel/save-image')}}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                {!! Form::hidden('id',$hotel->id,['class'=>'form-control','id'=>'id','placeholder'=>'9.000']) !!}
                                {!! Form::hidden('slug',$hotel->slug,['class'=>'form-control','id'=>'slug','placeholder'=>'9.000']) !!}
                                <h4>Resolusi Gambar 280x200</h4>
                                <div class="form-inline upload_1">
                                    <div class="form-group">
                                        <input type="file" name="file1">
                                        <input type="hidden" name="type1" value="1">
                                    </div>
                                </div>
                                <h4>Resolusi Gambar 1000x667</h4>
                                <div class="form-inline upload_1">
                                    <div class="form-group">
                                        <input type="file" name="file2">
                                        <input type="hidden" name="type2" value="2">
                                    </div>
                                </div>
                                <h4>Resolusi Gambar 1400x470</h4>
                                <div class="form-inline upload_1">
                                    <div class="form-group">
                                        <input type="file" name="file3">
                                        <input type="hidden" name="type3" value="3">
                                    </div>
                                </div>
                                <input type="submit" name="submit" value="submit -" class="btn btn-rounded waves-effect waves-light">
                            </form>
                        </div>
                    </div>
                </div>
       <!--      </div>
        </div> -->
