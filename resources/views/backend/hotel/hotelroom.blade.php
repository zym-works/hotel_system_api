<table class="table table-striped">
    <tr>
        <th>#</th>
        <th>Room</th>
        <th>Code Room</th>
        <!-- <th>BF Type</th> -->
        <th>Action</th>
    </tr>
    <?php $i = 1; ?>
        @foreach($room as $fh)
        <tr>
            <td>{{$i}}</td>
            <td>{{$fh->room_name}}</td>
            <td>{{$fh->code_room}}</td>
            <!-- <td>{{$fh->type_name }}</td> -->
            <td>
                <a href="{{ URL::to('backend/hotel/edit/'.$hotel->id. '/2/' . $fh->id) }}" class="btn-sm btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                <a href="{{ URL::to('backend/hotel/delete_detail/'.$hotel->id. '/' . $fh->id) }}" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                <a href="" data-toggle="modal" data-target="#myModal" onclick="FormDetail({{$hotel->id}},{{$fh->id}});" class="btn btn-sm btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
            </td>
        </tr>
        <?php $i++;?>
        @endforeach
</table>

        <!-- <div class="row">
            <div class="col-sm-12"> -->
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('hotel.add_room_hotel')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('room_name') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Name</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <input type="text" name="room_name" id="room_name" class="form-control" placeholder="Nama Kamar" value="<?php echo $editdataitem->room_name; ?>">
                                        @else
                                        <input type="text" name="room_name" id="room_name" class="form-control" placeholder="Nama Kamar" value="<?php echo $hotel->room_name; ?>">
                                        @endif
                                        {!! Form::hidden('id',$hotel->id,['class'=>'form-control','id'=>'id','placeholder'=>'9.000']) !!}
                                        {!! Form::hidden('idedit',$idedit,['class'=>'form-control','id'=>'idedit','placeholder'=>'9.000']) !!}
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('room_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('room_description') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Description</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <textarea class="form-control editdeskripsi" name="room_description" id="room_description" rows="5">{{$editdataitem->room_description}}</textarea>
                                        @else
                                        <textarea class="form-control editdeskripsi" name="room_description" id="room_description" rows="5">{{$hotel->room_description}}</textarea>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('id_bed_room') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Bed Type</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        {!! Form::select('id_bed_room', $bedroom, $editdataitem->id_bed_room, ['class' => 'form-control select2','id'=>'id_bed_room']) !!}
                                        @else
                                        {!! Form::select('id_bed_room', $bedroom, $hotel->id_bed_room, ['class' => 'form-control select2','id'=>'id_bed_room']) !!}
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('id_bed_room', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('bf_type') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Breakfast Type</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <!-- <input type="text" name="bf_type" id="bf_type" class="form-control" placeholder="Breakfast Type" value="<?php echo $editdataitem->bf_type; ?>"> -->
                                        {!! Form::select('bf_type', $bf, $editdataitem->bf_type, ['class' => 'form-control select2','id'=>'bf_type']) !!}
                                        @else
                                        <!-- <input type="text" name="bf_type" id="bf_type" class="form-control" placeholder="Breakfast Type" value="<?php echo $hotel->bf_type; ?>"> -->
                                        {!! Form::select('bf_type', $bf, $hotel->bf_type, ['class' => 'form-control select2','id'=>'bf_type']) !!}
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('bf_type', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('child_min_age') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Child Min Age</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <input type="number" name="child_min_age" id="child_min_age" class="form-control" placeholder="Child Min Age" value="<?php echo $editdataitem->child_min_age; ?>">
                                        @else
                                        <input type="number" name="child_min_age" id="child_min_age" class="form-control" placeholder="Child Min Age" value="<?php echo $hotel->child_min_age; ?>">
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('child_min_age', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('child_max_age') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Child Max Age</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <input type="number" name="child_max_age" id="child_max_age" class="form-control" placeholder="Child Max Age" value="<?php echo $editdataitem->child_max_age; ?>">
                                        @else
                                        <input type="number" name="child_max_age" id="child_max_age" class="form-control" placeholder="Child Max Age" value="<?php echo $hotel->child_max_age; ?>">
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('child_max_age', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('room_min_stay') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Min Stay</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <input type="number" name="room_min_stay" id="room_min_stay" class="form-control" placeholder="Room Min Stay" value="<?php echo $editdataitem->room_min_stay; ?>">
                                        @else
                                        <input type="number" name="room_min_stay" id="room_min_stay" class="form-control" placeholder="Room Min Stay" value="<?php echo $hotel->room_min_stay; ?>">
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('room_min_stay', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('net_price') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Nett Price</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <input type="text" name="net_price" id="net_price" class="form-control" placeholder="Nett Price" value="<?php echo $editdataitem->net_price; ?>">
                                        @else
                                        <input type="text" name="net_price" id="net_price" class="form-control" placeholder="Nett Price" value="<?php echo $hotel->net_price; ?>">
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('net_price', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('GrossPrice') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Gross Price</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <input type="text" name="GrossPrice" id="GrossPrice" class="form-control" placeholder="Gross Price" value="<?php echo $editdataitem->GrossPrice; ?>">
                                        @else
                                        <input type="text" name="GrossPrice" id="GrossPrice" class="form-control" placeholder="Gross Price" value="<?php echo $hotel->GrossPrice; ?>">
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('GrossPrice', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('CommPrice') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Commission Price</label>
                                    <div class="col-md-8">
                                        @if($idedit != '0')
                                        <input type="text" name="CommPrice" id="CommPrice" class="form-control" placeholder="Commission Price" value="<?php echo $editdataitem->CommPrice; ?>">
                                        @else
                                        <input type="text" name="CommPrice" id="CommPrice" class="form-control" placeholder="Commission Price" value="<?php echo $hotel->CommPrice; ?>">
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('CommPrice', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="submit" class="btn btn-primary" value=<?php if($idedit != '0'){ echo "save"; } else{ echo "add"; }?>>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
       <!--      </div>
        </div> -->
        
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Hotel Detail Data</h4>
            </div>
            <div class="modal-body">
                <h4>Text in a modal</h4>
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>