@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Country</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/country">Country List</a>
                    </li>
                    <li class="active">
                        Update Country
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Update Country</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('country.update')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('country_name') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Country Name</label>
                                    <div class="col-md-8">
                                        <input type="text" name="country_name" id="country_name" class="form-control" placeholder="Nama Negara" value="<?php echo $country->country_name; ?>">
                                        <input type="hidden" name="id" id="id" class="form-control" placeholder="Name Negara" value="<?php echo $country->id; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('country_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('mg_country_code') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">MG Country Code</label>
                                    <div class="col-md-8">
                                        <input type="text" name="mg_country_code" id="mg_country_code" class="form-control" readonly placeholder="MG Country Code" value="<?php echo $country->mg_country_code; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('mg_country_code', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop