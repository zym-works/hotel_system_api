@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Agent</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/agent">Agent List</a>
                    </li>
                    <li class="active">
                        Add Agent
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Add Agent</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('agent.store')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('agent_name') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Agent Name</label>
                                    <div class="col-md-8">
                                        <input type="text" name="agent_name" id="agent_name" class="form-control" placeholder="Agent Name" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('agent_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Username </label>
                                    <div class="col-md-8">
                                        <input type="text" name="username" id="username" class="form-control" placeholder="Username" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Email </label>
                                    <div class="col-md-8">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email" data-rule-required="true" >
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Address </label>
                                    <div class="col-md-8">
                                        <input type="text" name="address" id="address" class="form-control" placeholder="address" data-rule-required="true" >
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Telephone </label>
                                    <div class="col-md-8">
                                        <input type="text" name="telephone" id="telephone" class="form-control" placeholder="Telephone" data-rule-required="true" >
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">City </label>
                                    <div class="col-md-8">
                                        <input type="text" name="city" id="city" class="form-control" placeholder="City" data-rule-required="true" >
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Country </label>
                                    <div class="col-md-8">
                                        <input type="text" name="country" id="country" class="form-control" placeholder="Country" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Password</label>
                                    <div class="col-md-8">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" data-rule-required="true" >
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('last_balance') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Last Balance</label>
                                    <div class="col-md-8">
                                        <input type="text" name="last_balance" id="last_balance" class="form-control" placeholder="Last Balance" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('last_balance', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop