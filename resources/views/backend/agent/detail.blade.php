@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Agent</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/agent">Agent List</a>
                    </li>
                    <li class="active">
                        Agent List Detail
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
                <div class="card-box">


  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Agent Detail</a></li>
    <li><a data-toggle="tab" href="#menu1">History Top Up</a></li>
    <li><a data-toggle="tab" href="#menu2">History Booking</a></li>
    <li><a data-toggle="tab" href="#menu4">History Refund</a></li>
    <li><a data-toggle="tab" href="#menu3">List Member</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
     
                    <h4 class="m-t-0 header-title"><b>Agent List Detail</b></h4>
                    <hr>
                    <div class="table-responsive">
                       @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach
                      @foreach($agent as $data)
                    <div class="col-sm-12">
                      <div class="col-sm-2">Agent Name</div>
                      <div class="col-sm-1">:</div>
                      <div class="col-sm-9">{{ $data->agent_name }}</div>
                      <div class="col-sm-2">Username</div>
                      <div class="col-sm-1">:</div>
                      <div class="col-sm-9">{{ $data->username }}</div>
                      <div class="col-sm-2">Email</div>
                      <div class="col-sm-1">:</div>
                      <div class="col-sm-9">{{ $data->agent_email }}</div>
                      <div class="col-sm-2">Address</div>
                      <div class="col-sm-1">:</div>
                      <div class="col-sm-9">{{ $data->agent_address }}</div>
                      <div class="col-sm-2">Telephone</div>
                      <div class="col-sm-1">:</div>
                      <div class="col-sm-9">{{ $data->agent_telephone }}</div>
                      <div class="col-sm-2">City</div>
                      <div class="col-sm-1">:</div>
                      <div class="col-sm-9">{{ $data->agent_city }}</div>
                      <div class="col-sm-2">Country</div>
                      <div class="col-sm-1">:</div>
                      <div class="col-sm-9">{{ $data->agent_country }}</div>
                      <div class="col-sm-2">Last Balance</div>
                      <div class="col-sm-1">:</div>
                      <div class="col-sm-9">{{ 'Rp.'.number_format($data->last_balance) }}</div>
                  </div><br><br><p>
                      @endforeach
                  <hr>

                        

                       
                </div>
    </div>
    <div id="menu1" class="tab-pane fade">
                       <h3 align="center">History Top Up</h3>
                  
                  <table id="topup" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date Transaction</th>
                                    <th>Kredit</th>
                                    <th width="100">Picture</th>
                                    <th>Note</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

    </div>
    <div id="menu2" class="tab-pane fade">
     <h3 align="center">History Booking</h3>
                        <table class="table table-responsive" id="topdown"> 
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Date Transaction</th>
                              <th>Debit</th>
                              <th>Status</th>
                              <th>Balance Saldo</th>
                              <th>Confirm By</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3 align="center" >List Member</h3>
                        <table id="member" class="table table-responsive">
                          <thead>
                            <th>No</th>
                            <th>Account Number</th>
                            <th>Username</th>
                            <th>Fullname</th>
                          </thead>
                          <tbody></tbody>
                        </table>
    </div>
        <div id="menu4" class="tab-pane fade">
      <h3 align="center" >History Refund</h3>
                        <table id="refund" class="table table-responsive">
                          <thead>
                            <th>No</th>
                            <th>Agent Name</th>
                            <th>Refund</th>
                            <th>Created By</th>
                            <th>Date Confirm</th>
                            <th>Status</th>
                          </thead>
                          <tbody></tbody>
                        </table>
    </div>
  </div>

                </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->

<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="image-gallery-title"></h4>
            </div>
            <div class="modal-body">
                <img id="image-gallery-image" class="img-responsive" src="">
            </div>
            <div class="modal-footer">

                <div class="col-md-2">
                    <button type="button" class="btn btn-primary" id="show-previous-image">Previous</button>
                </div>

                <div class="col-md-8 text-justify" id="image-gallery-caption">
                    This text will be overwritten by jQuery
                </div>

                <div class="col-md-2">
                    <button type="button" id="show-next-image" class="btn btn-default">Next</button>
                </div>
            </div>
        </div>
    </div>
</div>

 <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
        </div>
      </div>
    </div>
</div>


@stop