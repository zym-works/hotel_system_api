@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Agent</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li class="active">
                        Report
                    </li>
                </ol>
            </div>
        </div>
        <a href="{{ route('backend.report',['download'=>'pdf']) }}">Download PDF</a>
 {{-- <a href="{{url('backend/report/sample')}}"> laporan pdf </a> --}}
        <div class="row">
                <div class="card-box">


  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Daily Reservation</a></li>
    <li><a data-toggle="tab" href="#menu1">Weekly Reservation</a></li>
    <li><a data-toggle="tab" href="#menu2">Monthly Reservation</a></li>
  </ul>
<hr>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <h4 class="m-t-0 header-title"><b>Agent List Detail</b></h4>
        <hr>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <th>No</th>
                    <th>Reservation Number</th>
                    <th>Date Booking</th>
                    <th>Agent Fullname</th>
                    <th>CheckIn</th>
                    <th>CheckOut</th>
                    <th>Person</th>
                    <th>Total Price</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <tr>
                    <?php $n=1; ?>
                    @foreach($listDaily as $data)
                        <td>{{$n}}</td>
                        <td>{{$data->reservation_number}}</td>
                        <td>{{$data->date_booking}}</td>
                        <td>{{$data->fullname}}</td>
                        <td>{{$data->check_in}}</td>
                        <td>{{$data->check_out}}</td>
                        <td>{{$data->adult_num}} Adult<br>{{$data->child_num}} Child</td>
                        <td>{{number_format($data->total_price_idr)}}</td>
                        <td><?php if($data->reservation_status==1){echo "Booked";}elseif($data->reservation_status==2){echo "Refund";} ?></td>
                    <?php $n++; ?>
                     @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="menu1" class="tab-pane fade">
                       <h3 align="center">History Top Up</h3>
                  
                  <table id="topup" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date Transaction</th>
                                    <th>Kredit</th>
                                    <th width="100">Picture</th>
                                    <th>Note</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

    </div>
    <div id="menu2" class="tab-pane fade">
     <h3 align="center">History Booking</h3>
                        <table class="table table-responsive" id="topdown"> 
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Date Transaction</th>
                              <th>Debit</th>
                              <th>Status</th>
                              <th>Balance Saldo</th>
                              <th>Confirm By</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
    </div>
   
  </div>

                </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->

@stop
                   
