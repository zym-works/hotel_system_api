@extends('backend.main') @section('content')
<table class="table">
                <thead>
                    <th>No</th>
                    <th>Reservation Number</th>
                    <th>Date Booking</th>
                    <th>Agent Fullname</th>
                    <th>CheckIn</th>
                    <th>CheckOut</th>
                    <th>Person</th>
                    <th>Total Price</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <tr>
                    <?php $n=1; ?>
                    @foreach($listDaily as $data)
                        <td>{{$n}}</td>
                        <td>{{$data->reservation_number}}</td>
                        <td>{{$data->date_booking}}</td>
                        <td>{{$data->fullname}}</td>
                        <td>{{$data->check_in}}</td>
                        <td>{{$data->check_out}}</td>
                        <td>{{$data->adult_num}} Adult<br>{{$data->child_num}} Child</td>
                        <td>{{number_format($data->total_price_idr)}}</td>
                        <td><?php if($data->reservation_status==1){echo "Booked";}elseif($data->reservation_status==2){echo "Refund";} ?></td>
                    <?php $n++; ?>
                     @endforeach
                    </tr>
                </tbody>
            </table>
@endsection