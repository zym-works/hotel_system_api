<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
        <title><?php if(isset($page['title'])){echo $page['title'];}?></title>
    <link rel="shortcut icon" href="{{url('/')}}/src/backend/assets/images/favicon_1.ico">

        {{-- CSS --}}
        <?php if(isset($css)){echo $css;} ?>
        @include('backend.component.css')
         <script src="{{url('/')}}/src/backend/assets/js/modernizr.min.js"></script>
    </head>

    <body class="fixed-left">
        <div id="wrapper">
                <!-- Header start-->
                @include('backend.component.header')
                <!-- Header end-->
                <!-- Main Sidebar start-->
                @include('backend.component.side')
                <!-- Main Sidebar end-->        
        
                <div class="content-page">
                   
                    @yield('content')
                <footer class="footer">
                    © 2017. All rights reserved.
                </footer>    
                </div>
                
                
        </div>
        <script>
            var resizefunc = [];
        </script>

        {{-- JQuery --}}
         @include('backend.component.js')
        <?php if(isset($js)){echo $js;} ?>
	<script> $('.datepicker').datepicker(); </script>
    <script src="{{ URL::asset('src/backend/assets/js/jquery.core.js') }}"></script>
    <script src="{{ URL::asset('src/backend/assets/js/jquery.app.js') }}"></script>
    </body>

</html>