@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Room</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/room">Room List</a>
                    </li>
                    <li class="active">
                        Update Room
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Update Room</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('room.update')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('room_name') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Name</label>
                                    <div class="col-md-8">
                                        <input type="text" name="room_name" id="room_name" class="form-control" placeholder="Nama Kamar" value="<?php echo $room->room_name; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('room_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('room_description') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Description</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control editdeskripsi" name="room_description" id="room_description" rows="5">{{$room->room_description}}</textarea>
                                        {!! Form::hidden('id',$room->id,['class'=>'form-control','id'=>'id','placeholder'=>'9.000']) !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('id_bed_room') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Bed Type</label>
                                    <div class="col-md-8">
                                        {!! Form::select('id_bed_room', $bedroom, $room->id_bed_room, ['class' => 'form-control select2','id'=>'id_bed_room']) !!}
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('id_bed_room', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('id_hotel') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Hotel</label>
                                    <div class="col-md-8">
                                        {!! Form::select('id_hotel', $hotel, $room->id_hotel, ['class' => 'form-control select2','id'=>'id_hotel']) !!}
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('id_hotel', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('bf_type') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Breakfast Type</label>
                                    <div class="col-md-8">
                                        <input type="text" name="bf_type" id="bf_type" class="form-control" placeholder="Breakfast Type" value="<?php echo $room->bf_type; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('bf_type', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('child_min_age') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Child Min Age</label>
                                    <div class="col-md-8">
                                        <input type="number" name="child_min_age" id="child_min_age" class="form-control" placeholder="Child Min Age" value="<?php echo $room->child_min_age; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('child_min_age', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('child_max_age') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Child Max Age</label>
                                    <div class="col-md-8">
                                        <input type="number" name="child_max_age" id="child_max_age" class="form-control" placeholder="Child Max Age" value="<?php echo $room->child_max_age; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('child_max_age', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('room_min_stay') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Min Stay</label>
                                    <div class="col-md-8">
                                        <input type="number" name="room_min_stay" id="room_min_stay" class="form-control" placeholder="Room Min Stay" value="<?php echo $room->room_min_stay; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('room_min_stay', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('net_price') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Nett Price</label>
                                    <div class="col-md-8">
                                        <input type="text" name="net_price" id="net_price" class="form-control" placeholder="Nett Price" value="<?php echo $room->net_price; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('net_price', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('GrossPrice') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Gross Price</label>
                                    <div class="col-md-8">
                                        <input type="text" name="GrossPrice" id="GrossPrice" class="form-control" placeholder="Gross Price" value="<?php echo $room->GrossPrice; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('GrossPrice', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('CommPrice') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Commission Price</label>
                                    <div class="col-md-8">
                                        <input type="text" name="CommPrice" id="CommPrice" class="form-control" placeholder="Commission Price" value="<?php echo $room->CommPrice; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('CommPrice', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                <div class="fallback">
                                <div class="dropzone" id="dropzone">
                                    <input name="file[]" type="file" multiple />
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                </div>
                                </div>
                                </div> -->
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="submit" onClick="ulang();" class="btn btn-default">Cancel</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop