@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Reservation</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/listreservation">Reservation List</a>
                    </li>
                    <li class="active">
                        Add Reservation
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Add Reservation</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('reservation.store')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('id_sales') ? ' has-error' : '' }}">
                                   <label class="col-md-2 control-label">Sales Name</label>
                                   <div class="col-md-8">
                                      {!! Form::select('id_sales', $sales, null, ['class' => 'form-control select2','id'=>'id_sales']) !!}
                                   </div>
                                   <div class="col-md-8">
                                      {!! $errors->first('id_sales', '<p class="help-block">:message</p>') !!}
                                   </div>
                                </div>
								<div class="form-group {{ $errors->has('id_hotel') ? ' has-error' : '' }}">
                                   <label class="col-md-2 control-label">Hotel Name</label>
                                   <div class="col-md-8">
                                      {!! Form::select('id_hotel', $hotel, null, ['class' => 'form-control select2','id'=>'id_hotel']) !!}
                                   </div>
                                   <div class="col-md-8">
                                      {!! $errors->first('id_hotel', '<p class="help-block">:message</p>') !!}
                                   </div>
                                </div>
								<div class="form-group {{ $errors->has('check_in') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Id Room</label>
                                    <div class="col-md-8">
                                        <input type="text" name="id_room" id="id_room" class="form-control" placeholder="Id Room" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('check_in', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
								<div class="form-group {{ $errors->has('check_in') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Check In</label>
                                    <div class="col-md-8">
                                        <input data-provide="datepicker" type="date" name="check_in" id="check_in" class="form-control date" placeholder="Check In" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('check_in', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
								<div class="form-group {{ $errors->has('check_out') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Check Out</label>
                                    <div class="col-md-8">
                                        <input type="date" name="check_out" id="check_out" class="form-control date" placeholder="Check Out" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('check_out', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
								<div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Price</label>
                                    <div class="col-md-8">
                                        <input type="text" name="price" id="price" class="form-control" placeholder="Price" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
								<div class="form-group {{ $errors->has('adult') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Adult</label>
                                    <div class="col-md-8">
                                        <input type="number" min="0" max="10" name="adult" id="adult" class="form-control" placeholder="Adult" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('adult', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
								<div class="form-group {{ $errors->has('child') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Child</label>
                                    <div class="col-md-8">
                                        <input type="number" min="0" max="10" name="child" id="child" class="form-control" placeholder="Child" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('child', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
								<div class="form-group {{ $errors->has('room_total') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Total</label>
                                    <div class="col-md-8">
                                        <input type="number" name="room_total" id="room_total" class="form-control" placeholder="Room Total" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('room_total', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Status</label>
                                    <div class="col-md-8">
                                        <select name="status" id="status" class="form-control select2">
                                            <option value="1">prosessing</option>
                                            <option value="2">booked</option>
                                            <option value="3">cancel</option>
                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop