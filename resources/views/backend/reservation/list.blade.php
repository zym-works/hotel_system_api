@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Reservation</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li class="active">
                        Reservation List
                    </li>
                    
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <a href="{{ URL::to('/listreservation/create') }}" class="btn btn-sm btn-default waves-effect waves-light pull-right" role="button">
                            Add
                        </a>
                    <h4 class="m-t-0 header-title"><b>Reservation List</b></h4>
                    <hr>
                    <div class="table-responsive">
                        <table id="listorder" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Reservation Number</th>
                                    <th>Date Booking</th>
                                    <th>Agent Fullname</th>
                                    <th>CheckIn</th>
                                    <th>CheckOut</th>
                                    <th>Person</th>
                                    <th>Total Price</th>
									<th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop