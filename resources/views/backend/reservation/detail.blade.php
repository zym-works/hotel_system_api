@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Reservation</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                         <a href="{{url('/')}}/listreservation">Reservation List</a>
                    </li>
                    <li class="active">
                        Detail
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Reservation List</b></h4>
                    <hr>
                    {{-- @foreach($detail as $data) --}}
                    <div class="row">
                        <div class="col-sm-2">Date Booking</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-9">{{ $agent->date_booking }}</div>
                        <div class="col-sm-2">Reservation Number</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-9">{{ $agent->reservation_number }}</div>
                        <div class="col-sm-2">Agent Name</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-9">{{ $agent_detail->agent_name }}</div>
                        <div class="col-sm-2">Email</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-9">{{ $agent_detail->agent_email }}</div>
                        <div class="col-sm-2">Telephone</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-9">{{ $agent_detail->agent_telephone }}</div>
                        <div class="col-sm-2">Address</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-9">{{ $agent_detail->agent_address }}</div>
                        <div class="col-sm-2">City</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-9">{{ $agent_detail->agent_city }}</div>
                        <div class="col-sm-2">Country</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-9">{{ $agent_detail->agent_country }}</div>
                    </div> <hr>
                    {{-- @endforeach --}}

                    <div class="table table-bordered">
                        @if ($hotel_detail->reservation_status==1)
                        <table id="listorder" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Date Confirm</th>
                                    <th>Hotel Name</th>
                                    <th>Check In</th>
                                    <th>Check Out</th>
                                    <th>Person</th>
                                    <th>Night</th>
                                    <th>Total Price</th>
									<th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$hotel_detail->date_confirm}}</td>
                                    <td>{{$hotel_detail->hotel_name}}</td>
                                    <td>{{$hotel_detail->check_in}}</td>
                                    <td>{{$hotel_detail->check_out}}</td>
                                    <td>
                                        {{$hotel_detail->adult_num}} Adult<br>
                                        {{$hotel_detail->child_num}} Child
                                    </td>
                                    <td>{{$hotel_detail->night}} Night</td>
                                    <td>{{'Rp.'.number_format($hotel_detail->total_price_idr)}}</td>
                                    <td> 
                                        <?php if ($hotel_detail->reservation_status==1) {
                                            echo "<label class=\"label label-success\"> booked </label>";
                                        }elseif($hotel_detail->reservation_status==2){
                                            echo "<label class=\"label label-warning\"> cancel </label>";
                                        }elseif($hotel_detail->reservation_status==3){
                                            echo "<label class=\"label label-info\"> pending </label>";
                                        }else{
                                            echo "-";
                                        }


                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        @else
                        <table id="listorder" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Date Confirm</th>
                                    <th>Hotel Name</th>
                                    <th>Check In</th>
                                    <th>Check Out</th>
                                    <th>Person</th>
                                    <th>Night</th>
                                    <th>Total Price</th>
                                    <th>Charge Type</th>
                                    <th>Total Refund</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$hotel_detail->date_confirm}}</td>
                                    <td>{{$hotel_detail->hotel_name}}</td>
                                    <td>{{$hotel_detail->check_in}}</td>
                                    <td>{{$hotel_detail->check_out}}</td>
                                    <td>
                                        {{$hotel_detail->adult_num}} Adult<br>
                                        {{$hotel_detail->child_num}} Child
                                    </td>
                                    <td>{{$hotel_detail->night}} Night</td>
                                    <td>{{'Rp.'.number_format($hotel_detail->total_price_idr)}}</td>
                                    <td>{{$hotel_detail->charge_type}}</td>
                                    <td>{{'Rp.'.number_format($hotel_detail->refund_idr)}}</td>
                                    <td> 
                                        <?php if ($hotel_detail->reservation_status==1) {
                                            echo "<label class=\"label label-success\"> booked </label>";
                                        }elseif($hotel_detail->reservation_status==2){
                                            echo "<label class=\"label label-warning\"> refund </label>";
                                        }elseif($hotel_detail->reservation_status==3){
                                            echo "<label class=\"label label-info\"> pending </label>";
                                        }else{
                                            echo "-";
                                        }


                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop