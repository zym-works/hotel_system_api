@extends('backend.main')
@section('content')
<section class="content">
    <div class="page-container">
        <div class="page-content container-fluid">
            <h3 class="mt-0">USER</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <div class="widget-heading">
                            <h3 class="widget-title">Add User</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="widget-body">

                            @foreach (['danger', 'warning', 'success', 'info'] as $msg) @if(Session::has('alert-' . $msg))

                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif @endforeach
                            <form action="{{route('user.store')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
								  <label class="col-sm-2 control-label">Username</label>

								  <div class="col-sm-5">
									<input type="text" name="username" id="username" class="form-control" placeholder="Username">
								  </div>
								  <div class="col-sm-5">
								  {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
									</div>
								</div>
								
								<div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
								  <label class="col-sm-2 control-label">User Name</label>

								  <div class="col-sm-5">
									<input type="text" name="user_name" id="user_name" class="form-control" placeholder="User Name">
								  </div>
								  <div class="col-sm-5">
								  {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
									</div>
								</div>
								
								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								  <label class="col-sm-2 control-label">Password</label>

								  <div class="col-sm-5">
									<input type="password" name="password" id="password" class="form-control" placeholder="Password">
								  </div>
								  <div class="col-sm-5">
								  {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
									</div>
								</div>
								
								<div class="form-group{{ $errors->has('user_group_id') ? ' has-error' : '' }}">
								  <label class="col-sm-2 control-label">User Group</label>

								  <div class="col-sm-5">
									{!! Form::select('user_group_id', $user_group, null, ['class' => 'form-control select2','id'=>'user_group_id']) !!}
								  </div>
								  <div class="col-sm-5">
								  {!! $errors->first('user_group_id', '<p class="help-block">:message</p>') !!}
									</div>
								</div>
								
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="submit" onClick="ulang();" class="btn btn-default">Cancel</button>
                                    </div>
                                </div>
                                <!-- /.box-footer -->
                        </div>
                        <!-- /.box-body -->

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
    </div>
</section>

	@stop