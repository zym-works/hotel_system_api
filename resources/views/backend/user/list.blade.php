@extends('backend.main')
@section('content')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="page-container">
        <div class="page-content container-fluid">
          <h3 class="mt-0">USER</h3>
			<div class="row">
	  <div class="widget">
        <div class="widget-heading">
          <h3 class="widget-title"></h3>
			 
          <div class="widget-body">
				<a href="{{url('/user/create')}}" class="btn btn-success">
					<i class="fa fa-plus"></i>
					<span class="hidden-480">
						Add User
					</span>
				</a>		  
          </div>
        </div>
			
                <div class="widget-body">
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
					@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
					@endif
				@endforeach
				  <table id="user" cellspacing="0" width="100%" class="table table-striped table-bordered table-hover table-condensed">
                    <thead>
                      <tr>
                        <th>Username</th>
						<th>User Name</th>
						<th>User Group</th>
						<th>Log</th>
						<th>Action</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      </div>
      </div>
      </div>
      <!-- /.box -->

    </section>
	@stop