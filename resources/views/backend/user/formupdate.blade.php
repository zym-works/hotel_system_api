@extends('backend.main')
@section('content')

    <!-- Main content -->
    <section class="content">
	<div class="page-container">
        <div class="page-content container-fluid">
          <h3 class="mt-0">USER</h3>
    <div class="row">
		<div class="col-md-12">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">Update User</h3>
                </div>
                <div class="widget-body">
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				  @if(Session::has('alert-' . $msg))

				  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				  @endif
				@endforeach
                  <form action="{{route('user.update')}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
					<div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
								  <label class="col-sm-2 control-label">User Name</label>

								  <div class="col-sm-5">
									<input type="text" name="user_name" id="user_name" class="form-control" placeholder="User Name" value="<?php echo $user->user_name; ?>">
								  </div>
								  <div class="col-sm-5">
								  {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
									</div>
								</div>
								<div class="form-group{{ $errors->has('user_group_id') ? ' has-error' : '' }}">
								  <label class="col-sm-2 control-label">User Group</label>

								  <div class="col-sm-5">
									{!! Form::select('user_group_id', $user_group, $user->user_group_id, ['class' => 'form-control select2','id'=>'user_group_id']) !!}
								  </div>
								  <div class="col-sm-5">
								  {!! $errors->first('user_group_id', '<p class="help-block">:message</p>') !!}
									</div>
								</div>	
                  
                    <div class="form-group">
                      <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>		
		
	</div>
	</div>
	</div>

    </section>
	@stop