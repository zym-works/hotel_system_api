@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Region</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li class="active">
                        Region List
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <a href="{{ URL::to('/backend/region/create') }}" class="btn btn-sm btn-default waves-effect waves-light pull-right" role="button">
                            Add
                        </a>
                    <h4 class="m-t-0 header-title"><b>Region List</b></h4>
                    <hr>
                    <div class="table-responsive">
                        <table id="region" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Region Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop