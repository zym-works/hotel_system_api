@extends('backend.main')
@section('content')
<script>
function ulang(){
	$('#currency_code').val('');
	$('#currency_name').val('');
}
</script>

<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Currency</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/currency">Currency List</a>
                    </li>
                    <li class="active">
                        Add Currency
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Add Currency</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('currency.store')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('currency_name') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Currency Name</label>
                                    <div class="col-md-8">
                                        <input type="text" name="currency_name" id="currency_name" class="form-control" placeholder="Nama Kurensi" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('currency_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('currency_code') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Currency Code</label>
                                    <div class="col-md-8">
                                        <input type="text" name="currency_code" id="currency_code" class="form-control" placeholder="Kode Kurensi">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('currency_code', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('currency_margin') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Currency Margin</label>
                                    <div class="col-md-8">
                                        <input type="text" name="currency_margin" id="currency_margin" class="form-control" placeholder="1.00">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('currency_margin', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop