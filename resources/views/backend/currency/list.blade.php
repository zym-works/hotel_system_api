@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Currency</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li class="active">
                        Currency List
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <a href="{{ URL::to('/backend/currency/create') }}" class="btn btn-default waves-effect waves-light pull-right" role="button">
                            Add
                        </a>
                    <h4 class="m-t-0 header-title"><b>Currency List</b></h4>
                    <hr>
                    <div class="table-responsive">
                        <table id="currency" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Currency Code</th>
                                    <th>Currency Name</th>
                                    <th>Currency Margin</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop