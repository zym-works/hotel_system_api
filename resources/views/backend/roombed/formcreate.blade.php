@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Room Type</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/roombed">Room Type List</a>
                    </li>
                    <li class="active">
                        Add Room Type
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Add Room Type</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('country.store')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('room_type') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Room Bed Type</label>
                                    <div class="col-md-8">
                                        <input type="text" name="room_type" id="room_type" class="form-control" placeholder="Room Bed Type">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('room_type', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('max_adult') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Max Adult</label>
                                    <div class="col-md-8">
                                        <input type="text" name="max_adult" id="max_adult" class="form-control" placeholder="Max Adult">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('max_adult', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('max_child') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Max Child</label>
                                    <div class="col-md-8">
                                        <input type="text" name="max_child" id="max_child" class="form-control" placeholder="Max Child">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('max_child', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('avrNightPrice') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Avr Night Price</label>
                                    <div class="col-md-8">
                                        <input type="text" name="avrNightPrice" id="avrNightPrice" class="form-control" placeholder="Avr Night Price">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('avrNightPrice', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop