@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Room Bed</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li class="active">
                        Room Bed List
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <a href="{{ URL::to('/backend/roombed/create') }}" class="btn btn-sm btn-default waves-effect waves-light pull-right" role="button">
                            Add
                        </a>
                    <h4 class="m-t-0 header-title"><b>Room Bed List</b></h4>
                    <hr>
                    <div class="table-responsive">
                        <table id="roombed" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Room Bed Type</th>
                                    <th>Max Adult</th>
                                    <th>Max Child</th>
                                    <th>Avr Night Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop