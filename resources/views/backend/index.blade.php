@extends('backend.main')
@section('content')
<!-- Start content -->
				<div class="content">
					<div class="container">

						<!-- Page-Title -->
						<div class="row">
                            <div class="col-sm-12">

                                <h4 class="page-title">Dashboard </h4>
                            </div>
                        </div>

                        
                        <div class="row">
							<div class="col-lg-4">
								<div class="card-box">
									<div class="bar-widget">
										<div class="table-box">
											<div class="table-detail">
												<div class="iconbox bg-info">
													<i class="icon-layers"></i>
												</div>
											</div>

											<div class="table-detail">
											   <h4 class="m-t-0 m-b-5"><b>{{ $daily }} transaction</b></h4>
											   <p class="text-muted m-b-0 m-t-0">Daily Reservation</p>
											</div>
                                            <div class="table-detail text-right">
                                                <span data-plugin="peity-bar" data-colors="#34d3eb,#ebeff2" data-width="120" data-height="45"></span>
                                            </div>

										</div>
									</div>
								</div>
							</div>

                            <div class="col-lg-4">
								<div class="card-box">
									<div class="bar-widget">
										<div class="table-box">
											<div class="table-detail">
												<div class="iconbox bg-custom">
													<i class="icon-layers"></i>
												</div>
											</div>

											<div class="table-detail">
											   <h4 class="m-t-0 m-b-5"><b>{{$weekly}} transaction</b></h4>
											   <p class="text-muted m-b-0 m-t-0">Weekly Reservation</p>
											</div>
                                            <div class="table-detail text-right">
                                                <span data-plugin="peity-pie" data-colors="#5fbeaa,#ebeff2" data-width="50" data-height="45"></span>
                                            </div>

										</div>
									</div>
								</div>
							</div>

                            <div class="col-lg-4">
								<div class="card-box">
									<div class="bar-widget">
										<div class="table-box">
											<div class="table-detail">
												<div class="iconbox bg-danger">
													<i class="icon-layers"></i>
												</div>
											</div>

											<div class="table-detail">
											   <h4 class="m-t-0 m-b-5"><b>{{$monthly}} transaction</b></h4>
											   <p class="text-muted m-b-0 m-t-0">Monthly Reservation</p>
											</div>
                                            <div class="table-detail text-right">
                                                <span data-plugin="peity-donut" data-colors="#f05050,#ebeff2" data-width="50" data-height="45"></span>
                                            </div>

										</div>
									</div>
								</div>
							</div>
						</div>

                        
                        <div class="row">

                            <div class="col-lg-4">
                        		<div class="card-box">
                        			<h4 class="text-dark header-title m-t-0 m-b-30">Daily Reservation</h4>

                        			<div class="widget-chart text-center">
	                                	<ul class="list-inline">
	                                		<li>
	                                			<h5 class="text-muted">Daily Income</h5>
	                                			<h4 class="m-b-0">Rp. {{number_format($incomeDaily->total)}}</h4>
	                                		</li>
	                                		<li>
	                                			<h5 class="text-muted">Daily Refund</h5>
	                                			<h4 class="m-b-0">Rp. {{number_format($refundDaily->total)}}</h4>
	                                		</li>
	                                	</ul>
                                	</div>
                        		</div>

                            </div>

                            <div class="col-lg-4">
                        		<div class="card-box">
                        			<h4 class="text-dark  header-title m-t-0 m-b-30">Weekly Reservation</h4>

                        			<div class="widget-chart text-center">
                                        <div id="sparkline2"></div>
	                                	<ul class="list-inline m-t-15">
	                                		<li>
	                                			<h5 class="text-muted m-t-20">Weekly Income</h5>
	                                			<h4 class="m-b-0">Rp. {{number_format($incomeWeekly->total)}}</h4>
	                                		</li>
	                                		<li>
	                                			<h5 class="text-muted m-t-20">Weekly Refund</h5>
	                                			<h4 class="m-b-0">Rp. {{number_format($refundWeekly->total)}}</h4>
	                                		</li>
	                                	</ul>
                                	</div>
                        		</div>

                            </div>

                            <div class="col-lg-4">
                        		<div class="card-box">
                        			<h4 class="text-dark  header-title m-t-0 m-b-30">Monthly Reservation</h4>

                        			<div class="widget-chart text-center">
                                        <div id="sparkline1"></div>
	                                	<ul class="list-inline m-t-15">
	                                		<li>
	                                			<h5 class="text-muted m-t-20">Monthly Income</h5>
	                                			<h4 class="m-b-0">Rp. {{number_format($incomeMonthly->total)}}</h4>
	                                		</li>
	                                		<li>
	                                			<h5 class="text-muted m-t-20">Monthly Refund</h5>
	                                			<h4 class="m-b-0">Rp. {{number_format($refundMonthly->total)}}</h4>
	                                		</li>
	                                	</ul>
                                	</div>
                        		</div>

                            </div>



                        </div>

                        <!-- End row -->
                        
                        <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet"><!-- /primary heading -->
                            <div class="portlet-heading">
                                <h3 class="portlet-title text-dark text-uppercase">
                                    Reservation Today
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#portlet2"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="portlet2" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Reservation Number</th>
                                                    <th>Date Booking</th>
                                                    <th>Agent Fullname</th>
                                                    <th>Checkin</th>
                                                    <th>Checkout</th>
                                                    <th>Person</th>
                                                    <th>Total Price</th>
                                                    <th>Refund</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($listToday as $x)
                                                <tr>
                                                    <td>{{ $x->reservation_number }}</td>
                                                    <td>{{ $x->date_booking }}</td>
                                                    <td>{{ $x->fullname }}</td>
                                                    <td>{{ $x->check_in }}</td>
                                                    <td>{{ $x->check_out }}</td>
                                                    <td>{{ $x->adult_num.' adult' }} <br> {{ $x->child_num.' child' }}</td>
                                                    <td>{{ 'Rp.'.number_format($x->total_price_idr) }}</td>
                                                    <td>{{ 'Rp.'.number_format($x->refund_idr) }}</td>
                                                    <td>
                                                        @if ($x->reservation_status == 1)
                                                            <span class="label label-info"> Booked </span>
                                                        @elseif($x->reservation_status == 2)
                                                            <span class="label label-warning"> Cancel </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->


                </div>

                        
                    </div> <!-- container -->
                               
                </div> <!-- content -->
@stop