========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                            <li class="text-muted menu-title">Navigation</li>

                            <li class="has_sub">
                                <a href="{{url('/')}}/home" class="waves-effect"><i class="ti-home"></i> <span> Home </span></a>
                            </li>

                            <!-- <li class="has_sub">
                                <a href="{{url('/')}}/profile" class="waves-effect"><i class="fa fa-user"></i> <span> Profile </span></a>
                            </li> -->

                            <!-- <li class="has_sub">
                                <a href="{{url('/')}}/id" class="waves-effect"><i class=" ti-shopping-cart"></i> <span> Goods Management </span></a>
                            </li> -->

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-shopping-cart"></i> <span> Management </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="{{url('/')}}/backend/currency">Currency</a></li>
                                    <li><a href="{{url('/')}}/backend/city">City</a></li>
                                    <li><a href="{{url('/')}}/backend/region">Region</a></li>
                                    <li><a href="{{url('/')}}/backend/country">Country</a></li>
                                    <li><a href="{{url('/')}}/backend/area">Area</a></li>
                                    <li><a href="{{url('/')}}/backend/facility">Facility</a></li>
                                    <!-- <li><a href="{{url('/')}}/backend/room">Room</a></li> -->
                                    <li><a href="{{url('/')}}/backend/roombed">Room Type</a></li>
                                    <li><a href="{{url('/')}}/backend/hotel">Hotel</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
								<a href="javascript:void(0);" class="waves-effect"><i class="ti-notepad"></i> <span> Reservation </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="{{url('/')}}/listreservation">List Reservation</a></li>
                                </ul>
							</li>
                            <li class="has_sub">
                                <a href="{{url('/')}}/backend/agent" class="waves-effect"><i class=" ti-comments"></i> <span> Agent </span></a>
                            </li>
                            <li class="has_sub">
                                <a href="{{url('/')}}/backend/report" class="waves-effect"><i class=" ti-comments"></i> <span> Report </span></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End