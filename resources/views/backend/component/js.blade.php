<script src="{{ URL::asset('src/backend/assets/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/detect.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/fastclick.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/waves.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/wow.min.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ URL::asset('src/backend/assets/js/jquery.scrollTo.min.js') }}"></script>

{{-- <script type="text/javascript">
                $(function() {
                      $('img').on('click', function() {
                      $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
                      $('#enlargeImageModal').modal('show');
                    });
                });
                </script> --}}