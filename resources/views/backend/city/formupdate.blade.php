@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">City</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/city">City List</a>
                    </li>
                    <li class="active">
                        Update City
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Update City</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('city.update')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('city_name') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">City Name</label>
                                    <div class="col-md-8">
                                        <input type="text" name="city_name" id="city_name" class="form-control" placeholder="Nama Kota" value="<?php echo $city->city_name; ?>">
                                        <input type="hidden" name="id" id="id" class="form-control" placeholder="Name Negara" value="<?php echo $city->id; ?>">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('city_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('id_country') ? ' has-error' : '' }}">
                                   <label class="col-md-2 control-label">Country</label>
                                   <div class="col-md-8">
                                      {!! Form::select('id_country', $country, $city->id_country, ['class' => 'form-control select2','id'=>'id_country']) !!}
                                   </div>
                                   <div class="col-md-8">
                                      {!! $errors->first('id_country', '<p class="help-block">:message</p>') !!}
                                   </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop