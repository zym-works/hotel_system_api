@extends('backend.main') @section('content')

<!-- Main content -->
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Facility</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}"><i class="ti-home"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/home">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/backend/facility">Facility List</a>
                    </li>
                    <li class="active">
                        Add Facility
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Add Facility</b></h4><hr>
                    <div class="row">
                        <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach

                            <form action="{{route('facility.store')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('facility_name') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Facility Name</label>
                                    <div class="col-md-8">
                                        <input type="text" name="facility_name" id="facility_name" class="form-control" placeholder="Sarapan gratis, WiFi, Parkir, Restoran, AC, dll" data-rule-required="true">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $errors->first('facility_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <!-- <div class="form-group {{ $errors->has('facility_type') ? ' has-error' : '' }}">
                                    <label class="col-md-2 control-label">Facility Type</label>
                                    <div class="col-md-8">
                                        {!! Form::select('facility_type', $optionview, null, ['class' => 'form-control','id'=>'facility_type']) !!}
                                    </div>
                                </div> -->
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-outline btn-success">Save</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->

</div>
<!-- content -->
@stop