@extends('citytour.main')
@section('content')
<main>
    <section id="hero" class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div id="login">
                        <div class="text-center"><img src="{{url('/')}}/src/citytour/img/logo_sticky.png" alt="Image" data-retina="true" ></div>
                        <hr>
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach
                        <form method="post" action="{{url('/register/user')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                            <div class="form-group {{ $errors->has('fullname') ? ' has-error' : '' }}">
                                <label>Full Name</label>
                                <input type="text" class=" form-control" name="fullname"  placeholder="Full Name">
                                <div class="col-md-8">
                                        {!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                <label>Username</label>
                                <input type="text" class=" form-control" name="username" placeholder="Username">
                                <div class="col-md-8">
                                        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>Email</label>
                                <input type="email" class=" form-control" name="email" placeholder="Email">
                                <div class="col-md-8">
                                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                <label>Telephone</label>
                                <input type="text" class=" form-control" name="telephone" placeholder="Telephone">
                                <div class="col-md-8">
                                        {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                <label>Address</label>
                                <textarea class=" form-control" name="address" placeholder="Address"></textarea>
                                <div class="col-md-8">
                                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                <label>Country</label>
                                <input type="text" class=" form-control" name="country" placeholder="Country">
                                <div class="col-md-8">
                                        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                <label>City</label>
                                <input type="text" class=" form-control" name="city" placeholder="City">
                                <div class="col-md-8">
                                        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <input type="password" class=" form-control" name="password" id="password1" placeholder="Password">
                                <div class="col-md-8">
                                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Confirm password</label>
                                <input type="password" class=" form-control" id="password2" placeholder="Confirm password">
                            </div>
                            <div id="pass-info" class="clearfix"></div>
                            <button class="btn_full">Create an account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End main -->
@endsection