@extends('citytour.main')
@section('content')

<section id="search_container">
        <div id="search">
            <ul class="nav nav-tabs">

                <li class="active"><a href="#hotels" data-toggle="tab">Hotel</a></li>
                <li ><a href="#tours" data-toggle="tab">Pesawat</a></li>
                <li><a href="#tours" data-toggle="tab">Kereta Api</a></li>

            </ul>

            <div class="tab-content" style="background-color:RGBA(255,255,255,0.90)">

                <!-- End rab -->
                <div class="tab-pane active" id="hotels">

                    <h3>Cari Hotel Murah & Nyaman . . .</h3>
                    <div class="row">
                        

                        <div class="col-md-5">
                            <div class="form-group">
                                <label><i class="icon-calendar-7"></i> Check in</label>
                                <input type="text" class="date-pick form-control" id="stayDate" value="">

{{--                                <input class="date-pick form-control" data-date-format="M d, D" type="text">--}}
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label>Kamar</label>
                                <div class="numbers-row">
                                    <input type="text" value="1" id="rooms" class="qty2 form-control" name="rooms">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-5">
                            <div class="form-group">
                                <label>Dewasa</label>
                                <div class="numbers-row">
                                    <input type="text" value="1" id="adults" class="qty2 form-control" name="adults_2">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <div class="form-group">
                                 <label>Anak</label>
                                            
                                                <div class="input_fields_wrap">
                                                    <select class="form-control" id="child">
                                                        <option value="0" selected>Tanpa Anak</option>
                                                        <option value="1">1 Anak</option>
                                                        <option value="2">2 Anak</option>
                                                    </select>
                                               

                                                <script type="text/javascript">
                                                        $('#child').on('change', function() {
                                                            var val = this.value;
                                                            // alert(val);
                                                            // return;
                                                            // for(var a=1;a<=val;a++){
                                                            //     $("#age"+a).remove();
                                                            // }
                                                            for(var b=1;b<=10;b++){
                                                                // alert("#age");
                                                            $("#age"+b).remove();
                                                        }

                                                            for(var a=1;a<=val;a++){
                                                                // $("#age"+a).remove();

                                                                
                                                                var selectList = document.createElement("select");
                                                                selectList.setAttribute("id", "age"+a);
                                                                selectList.setAttribute("class","form-control a");

                                                                // alert("age"+a);
                                                                document.getElementById('lc').appendChild(selectList);
                                                                // $(document.createElement('div')).addClass("col-md-3 col-sm-3 a")
                                                            
                                                            for (var i = 1; i <= 17; i++) {
                                                                var option = document.createElement("option");
                                                                option.setAttribute("value", i);
                                                                option.text = i+" tahun";
                                                                selectList.appendChild(option);
                                                            }
                                                        }
                                                            // var count_child = document.getElementById("childs").value;
                                                            // var total=parseInt(count_child)-1;
                                                            // var x =document.getElementById("childs").value = total;
                                                            // $("#age"+val).remove();
                                                        });
                                                </script>
                                                 
                                        </div>
                                        <br>
                                        
                            </div>
                        </div>


                    </div>
                                        <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Hotel / Destinasi</label>

                                    <select data-placeholder="Choose a Hotel..." class="hotelnya form-control" id="mg_code" >
                                    @foreach($hotel as $x)
                                    <option></option>
                                      <option value="{{$x->id_from_mg.','.$x->id.','.$x->slug }}">{{$x->hotel_name}}</option>
                                    @endforeach
                                      <option value="WSASTHBKK">Bangkok</option>
                                    </select>
                                  <script type="text/javascript">
                                        $(".hotelnya").chosen();
                                  </script>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                               <div id="lc"></div>
                            </div>
                        </div>
                    </div>
                     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                                        <script src="{{url('/')}}/src/citytour/js/nprogress.js"></script>
                                        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                                        <script src="{{url('/')}}/src/citytour/js/fecha.js"></script>
                                        <script src="{{url('/')}}/src/citytour/js/hotel-datepicker.js"></script>
                   <script>

                                            $(document).ready(function(){
                                         /*       $(".numbers-rowz").append('<button style="width: 33px;height: 40px;-webkit-transform:translateX(66px);-webkit-transform:translateY(-40px);"class="inc button_incX" onclick="added()">+</button><button class="dec button_incX" onclick="minus()">-</button>');*/
                                                $(".numbers-row").append('<div class="inc button_inc">+</div><div class="dec button_inc">-</div>');
                                                $(".button_inc").on("click", function () {
                                                    var $button = $(this);

                                                     var oldValue = $button.parent().find("input").val();
                                                    var ini = $button.parent().find("#rooms").val();

                                                     // $('#child').find('option').not(':first').remove(); // remove option child except first
                                                  if ($button.text() == "+") {
                                                        var newVal = parseFloat(oldValue) + 1;
                                                        var xx = parseFloat(ini) + 1;
                                                    }
                                                    else {
                                                        if (oldValue > 1) {
                                                            var newVal = parseFloat(oldValue) - 1;
                                                            var xx = parseFloat(ini) - 1;
                                                        } else {
                                                            newVal = 1;
                                                            xx = 1;
                                                        }
                                                    }
                                                    $button.parent().find("input").val(newVal);

                                                    //select option sesuai dengan jumlah kamar
                                                    for (var x=0; x<=xx; x+=1 ){
                                                        // alert(x);
                                                        // return;
                                                         $('#child').find('option').not(':first').remove();

                                                         if (xx == x){ 
                                                            // var ini=2;
                                                            var select, i, option;
                                                            select = document.getElementById( 'child' );
                                                            // $('#child').empty().append('<option selected="selected">Tanpa Anak</option>');
                                                            var ss = xx*2;
                                                            // if (ss==4){
                                                            //     ss = 2;
                                                            // }
                                                            // alert(ss);
                                                            // return;
                                                            for ( i = 1; i <= ss; i += 1 ) {

                                                               var option = document.createElement("option");
                                                                option.setAttribute("value", i);
                                                                option.text = i+" anak";
                                                                select.appendChild(option);
                                                            }
                                                         }
                                                     }


                                                    if(newVal>$("#adults").val())
                                                    {
                                                        $("#adults").val(newVal);
                                                    }
                                                    if($("#adults").val()< $("#rooms").val())
                                                    {
                                                        swal ("Info","Jumlah kamar harus lebih sedikit dari jumlah tamu" ,{

                                                            button: false,
                                                            timer: 3000,

                                                        });
                                                        $("#adults").val($("#rooms").val());
                                                    }

                                                    var x;

                                                    var tambahSatu = +$("#rooms").val() + 1;
                                                    var kurangSatu = +$("#rooms").val() - 1;
                                                    //var kamarTambah;

                                                    for(x=1;x<=10;x++)
                                                    {

                                                        if($("#rooms").val()==x){
                                                            //swal ("Info","It works" );
                                                            batasDewasa=x*4;
                                                            tambahKamar=batasDewasa/2;
                                                            if ($button.text() == "+") {
                                                            if($("#adults").val()>batasDewasa)
                                                            {

                                                                swal ("Info","Melebihi jumlah kapasitas kamar atau\n Anda bisa menambahkan 1 Kamar Lagi?",{
                                                                    buttons: {
                                                                        cancel: "Tidak",
                                                                        Ya: true,
                                                                    }

                                                                })
                                                                    .then((value) => {
                                                                    switch (value) {
                                                                    case "Ya":
                                                                        //var num = +$("#rooms").val() + 1;
                                                                        $("#rooms").val(tambahSatu);
                                                                        $("#adults").val(batasDewasa+1);
                                                                        //swal("Ok 1 kamar ditambahkan");
                                                                        break;
                                                                    default:
                                                                        $("#adults").val(batasDewasa);
                                                                    }
                                                                });

                                                            }

                                                            }// tutup button

                                                            else     if ($button.text() == "-") {

                                                                if($("#adults").val()>batasDewasa)
                                                                {
                                                                    swal ("Info","Mengurangi jumlah kamar akan berpengaruh ke jumlah tamu?",{
                                                                        buttons: {
                                                                            cancel: "ok",

                                                                        }

                                                                    })
                                                                        .then((value) => {
                                                                        switch (value) {
                                                                        case "Ya":
                                                                            //var num = +$("#rooms").val() + 1;

                                                                            $("#rooms").val(kurangSatu);
                                                                            $("#adults").val(batasDewasa/2);
                                                                            //swal("Ok 1 kamar ditambahkan");
                                                                            break;
                                                                        default:
                                                                            $("#adults").val(batasDewasa);
                                                                        }
                                                                    });

                                                                }
                                                            }



                                                        }

                                                    }

                                                });

                                                (function() {
                                                    var today = new Date();
                                                    var tomorrow = new Date();
                                                    tomorrow.setDate(tomorrow.getDate() + 1);
                                                    var input = document.getElementById('stayDate');
                                                    input.value = fecha.format(today, 'YYYY-MM-DD') + ' - ' + fecha.format(tomorrow, 'YYYY-MM-DD');
                                                    var limitStay = new HotelDatepicker(document.getElementById('stayDate'), {
                                                        maxNights: 7

                                                    });
                                                })();

                                                $("#tombol_book").click(function(){
                                                    //$("#tombol_book").attr("disabled", true);
                                                    //NProgress.start();
                                                    $(".toRemove").remove();
                                                    var cek_in_cek_out =$('#stayDate').val();
                                                    //alert(cek_in_cek_out);
                                                    var cek_in=cek_in_cek_out.substr(0, 11);
                                                    var cek_out=cek_in_cek_out.substr(13, 26);
                                                    // alert(cek_out);
                                                    var urlGambar="{{URL::to('/')}}";
                                                    /*alert("d "+cek_out);*/
                                                    /*var cek_out =$('#cek-out').val();*/
                                                    var dewasa =$('#adults').val();
                                                    var kamar=$('#rooms').val();
                                                    var mg_code=$('#mg_code').val();
                                                     var Sum_child=$('#child').val();

                                                     var myarr = mg_code.split(",");
                                                     // alert (myarr[0]);
                                                     // return;
                                                     // alert('task completed');
                                                    // a.test();
                                                    // a.focus();
                                                    // window.opener.document.functionToBeCalledFromChildWindow('my task is over you can proceed');
                                                     var ini = cek_in.replace(' ','');
                                                    // window.close();
                                                    // a.addEventListener('load', function(){
                                                    //   a.myfunction("hi"+"heloo");
                                                    // }, true);

                                                     if (Sum_child > 0){
                                                         var age_child = [];
                                                         for (var x=1; x<=Sum_child; x++){
                                                            age_child.push($('#age'+x).val());
                                                            // age_child.replace(',','_');
                                                            // var join = age_child.replace(',','_');
                                                         }
                                                         var toStr = age_child.toString();
                                                        var AgeJoin = toStr.replace(/,/g,'~');
                                                     // alert(AgeJoin);
                                                     // return;
                                                    }
                                                    if (Sum_child == 0){
                                                        var AgeJoin = 0;
                                                     //    alert('brati no');
                                                     // return;
                                                    }

                                                    var n = mg_code.length;
                                                    if (n==9){
                                                        xx=mg_code.substr(0,9);
                                                       // list/hotel/bangkok
                                                         {{-- window.open({{URL::to('list/hotel/bangkok') }}); --}}
                                                        // var createform = document.createElement('form'); // Create New Element Form
                                                        // createform.setAttribute("action", ""); // Setting Action Attribute on Form
                                                        // createform.setAttribute("method", "post"); // Setting Method Attribute on Form
                                                        // // x.appendChild(createform);

                                                         // alert(dewasa+cek_in+cek_out+mg_code+kamar+Sum_child+AgeJoin);
                                                         // return;

                                                          // window.open("{{URL::to('list/hotel/bangkok')}}","_self");

                                                         window.open("{{URL::to('list/hotel/bangkok')}}?"+dewasa+"_"+ini+"_"+cek_out+"_"+mg_code+"_"+kamar+"_"+Sum_child+"_"+AgeJoin, "_self");
                                                         // alert(xx);
                                                         return;
                                                    }
                                                    

                                                    if (mg_code==false){
                                                        alert('Hotel Belum Dipilih !!');
                                                        return;
                                                    }

                                                    // alert(myarr[0]);
                                                    // return;

                                                    window.open("{{URL::to('detail/hotel/')}}/"+myarr[1]+"-"+myarr[2]+".html?"+myarr[0]+"_"+ini+"_"+cek_out+"_"+dewasa+"_"+kamar+"_"+Sum_child+"_"+AgeJoin,"_self");
                                                    return;
                                                  
                                                });
                                            });

                                        </script>
                            <!-- End row -->

                    <!-- End row -->
                    <hr>
                    <button class="btn_1 green" id="tombol_book"  onclick="test()><i class="icon-search"></i>Search now</button>
                    {{-- <button onclick="location.href = '{{url('detail/hotel/myarr[1]-myarr[2].html')}}';" id="tombol_book" class="float-left submit-button" >Home</button> --}}
                </div>

                <div class="tab-pane" id="transfers">
                    <h3>Search Transfers in Paris</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="select-label">Pick up location</label>
                                <select class="form-control">
                                                <option value="orly_airport">Orly airport</option>
                                                <option value="gar_du_nord">Gar du Nord Station</option>
                                                <option value="hotel_rivoli">Hotel Rivoli</option>
                                            </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="select-label">Drop off location</label>
                                <select class="form-control">
                                                <option value="orly_airport">Orly airport</option>
                                                <option value="gar_du_nord">Gar du Nord Station</option>
                                                <option value="hotel_rivoli">Hotel Rivoli</option>
                                            </select>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class="icon-calendar-7"></i> Date</label>
                                <input class="date-pick form-control" data-date-format="M d, D" type="text">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class=" icon-clock"></i> Time</label>
                                <input class="time-pick form-control" value="12:00 AM" type="text">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                            <div class="form-group">
                                <label>Adults</label>
                                <div class="numbers-row">
                                    <input type="text" value="1" id="adults" class="qty2 form-control" name="quantity">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-9">
                            <div class="form-group">
                                <div class="radio_fix">
                                    <label class="radio-inline" style="padding-left:0">
                                              <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked> One Way
                                            </label>
                                </div>
                                <div class="radio_fix">
                                    <label class="radio-inline">
                                              <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Return
                                            </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- End row -->
                    <hr>
                    <button class="btn_1 green"><i class="icon-search"></i>Search now</button>
                </div>


                <div class="tab-pane" id="restaurants">
                    <h3>Search Restaurants in Paris</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Search by name</label>
                                <input type="text" class="form-control" id="restaurant_name" name="restaurant_name" placeholder="Type your search terms">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Food type</label>
                                <select class="ddslick" name="category_2">
                                                <option value="0" data-imagesrc="{{url('/')}}/src/citytour/img/icons_search/all_restaurants.png" selected>All restaurants</option>
                                                <option value="1" data-imagesrc="{{url('/')}}/src/citytour/img/icons_search/fast_food.png">Fast food</option>
                                                <option value="2"  data-imagesrc="{{url('/')}}/src/citytour/img/icons_search/pizza_italian.png">Pizza / Italian</option>
                                                <option value="3" data-imagesrc="{{url('/')}}/src/citytour/img/icons_search/international.png">International</option>
                                                <option value="4" data-imagesrc="{{url('/')}}/src/citytour/img/icons_search/japanese.png">Japanese</option>
                                                <option value="5" data-imagesrc="{{url('/')}}/src/citytour/img/icons_search/chinese.png">Chinese</option>
                                                <option value="6" data-imagesrc="{{url('/')}}/src/citytour/img/icons_search/bar.png">Coffee Bar</option>
                                            </select>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class="icon-calendar-7"></i> Date</label>
                                <input class="date-pick form-control" data-date-format="M d, D" type="text">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class=" icon-clock"></i> Time</label>
                                <input class="time-pick form-control" value="12:00 AM" type="text">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Adults</label>
                                <div class="numbers-row">
                                    <input type="text" value="1" id="adults" class="qty2 form-control" name="adults">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Children</label>
                                <div class="numbers-row">
                                    <input type="text" value="0" id="children" class="qty2 form-control" name="children">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End row -->
                    <hr>
                    <button class="btn_1 green"><i class="icon-search"></i>Search now</button>
                </div>
            </div>
        </div>
    </section>
    <!-- End hero -->

    <main>

        <div class="container margin_60">

            <div class="main_title">
                <h2>Top <span>Popular</span> Tours</h2>
                <p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>
            </div>

            <div class="row">
                <?php $i=0 ?>
                @foreach($hotel as $data)
                @if($i==9)
                    <?php break; ?>
                @endif
                    <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                        <div class="tour_container">
                            <div class="ribbon_3 popular"><span>Popular</span></div>
                            <div class="img_container">
                                <a href="{{url('detail/hotel/')}}/{{$data->id}}-{{$data->slug}}.html">
                                    <img src="{{url('/storage/img-hotel/')}}/{{$data->hotel_name}} hotel/{{$data->hotel_name}}_280x200.jpg" style="width:200px;height:200px"  class="img-responsive" alt="Image">
                                    <div class="badge_save">Save<strong>30%</strong></div>
                                    <div class="short_info">
                                        <i class="icon_set_1_icon-43"></i>{{$data->country_name}} <small>({{$data->city_name}})</small><span class="price"><sup>Rp</sup>850,566</span>
                                    </div>
                                </a>
                            </div>
                            <div class="tour_title">
                                <h5><strong>{{$data->hotel_name}}</strong> Hotel</h5>
                                <div class="rating">
                                    {{-- {{count}} --}}
                                    @for ($a=0;$a<$data->rating;$a++)
                                        <i class="icon-star voted"></i>
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <!-- End box tour -->
                    </div>
                    <?php $i++; ?>
                @endforeach

            </div>
            <!-- End row -->
            <p class="text-center nopadding">
                <a href="#" class="btn_1 medium"><i class="icon-eye-7"></i>View all tours (144) </a>
            </p>
        </div>

        <div class="container margin_60">

            <div class="main_title">
                <h2>Top <span>Destination</span> Tours</h2>
                <p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>
            </div>

            <div class="row">

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                    <div class="ribbon_3 popular"><span>Popular</span></div>
                    <div class="tour_container">
                        <div class="img_container">
                            <a href="{{url('/')}}/list/hotel/bangkok">
                                <img src="{{url('/')}}/storage/IMG DESTINATION/BANGKOK-FRONT.jpg" class="img-responsive" alt="Image">
                                <div class="short_info">
                                    <i class="icon_set_1_icon-44"></i>Historic Buildings<span class="price"></span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Bangkok</strong> Thailand</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->
{{--
                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container">
                            <a href="single_tour.html">
                                <img src="{{url('/')}}/src/citytour/img/tour_box_2.jpg" width="800" height="533" class="img-responsive" alt="Image">
                                <div class="badge_save">Save<strong>30%</strong></div>
                                <div class="short_info">
                                    <i class="icon_set_1_icon-43"></i>Churches<span class="price"><sup>$</sup>45</span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Notredame</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.3s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container">
                            <a href="single_tour.html">
                                <img src="{{url('/')}}/src/citytour/img/tour_box_3.jpg" width="800" height="533" class="img-responsive" alt="Image">
                                <div class="badge_save">Save<strong>30%</strong></div>
                                <div class="short_info">
                                    <i class="icon_set_1_icon-44"></i>Historic Buildings<span class="price"><sup>$</sup>48</span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Versailles</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container">
                            <a href="single_tour.html">
                                <img src="{{url('/')}}/src/citytour/img/tour_box_4.jpg" width="800" height="533" class="img-responsive" alt="Image">
                                <div class="short_info">
                                    <i class="icon_set_1_icon-30"></i>Walking tour<span class="price"><sup>$</sup>36</span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Pompidue</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.5s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container">
                            <a href="single_tour.html">
                                <img src="{{url('/')}}/src/citytour/img/tour_box_14.jpg" width="800" height="533" class="img-responsive" alt="Image">
                                <div class="short_info">
                                    <i class="icon_set_1_icon-28"></i>Skyline tours<span class="price"><sup>$</sup>42</span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Tour Eiffel</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.6s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container">
                            <a href="single_tour.html">
                                <img src="{{url('/')}}/src/citytour/img/tour_box_5.jpg" width="800" height="533" class="img-responsive" alt="Image">
                                <div class="short_info">
                                    <i class="icon_set_1_icon-44"></i>Historic Buildings<span class="price"><sup>$</sup>40</span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Pantheon</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.7s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container">
                            <a href="single_tour.html">
                                <img src="{{url('/')}}/src/citytour/img/tour_box_8.jpg" width="800" height="533" class="img-responsive" alt="Image">
                                <div class="short_info">
                                    <i class="icon_set_1_icon-3"></i>City sightseeing<span class="price"><sup>$</sup>35</span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Open Bus</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.8s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container">
                            <a href="single_tour.html">
                                <img src="{{url('/')}}/src/citytour/img/tour_box_9.jpg" width="800" height="533" class="img-responsive" alt="Image">
                                <div class="short_info">
                                    <i class="icon_set_1_icon-4"></i>Museums<span class="price"><sup>$</sup>38</span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Louvre museum</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.9s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container">
                            <a href="single_tour.html">
                                <img src="{{url('/')}}/src/citytour/img/tour_box_12.jpg" width="800" height="533" class="img-responsive" alt="Image">
                                <div class="short_info">
                                    <i class="icon_set_1_icon-14"></i>Eat &amp; drink<span class="price"><sup>$</sup>25</span>
                                </div>
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Boulangerie</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div>
                            <!-- end rating -->
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                            <!-- End wish list-->
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- End col-md-4 -->--}}

            </div>
            <!-- End row -->
            <p class="text-center nopadding">
                <a href="#" class="btn_1 medium"><i class="icon-eye-7"></i>View all tours (144) </a>
            </p>
        </div>
        <!-- End container -->

        <div class="white_bg">
            <div class="container margin_60">
                <div class="main_title">
                    <h2>Other <span>Popular</span> tours</h2>
                    <p>
                        Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.
                    </p>
                </div>
                <div class="row add_bottom_45">
                    <div class="col-md-4 other_tours">
                        <ul>
                            <li><a href="#"><i class="icon_set_1_icon-3"></i>Tour Eiffel<span class="other_tours_price">$42</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-30"></i>Shopping tour<span class="other_tours_price">$35</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-44"></i>Versailles tour<span class="other_tours_price">$20</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-3"></i>Montparnasse skyline<span class="other_tours_price">$26</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-44"></i>Pompidue<span class="other_tours_price">$26</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-3"></i>Senna River tour<span class="other_tours_price">$32</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 other_tours">
                        <ul>
                            <li><a href="#"><i class="icon_set_1_icon-1"></i>Notredame<span class="other_tours_price">$48</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-4"></i>Lafaiette<span class="other_tours_price">$55</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-30"></i>Trocadero<span class="other_tours_price">$76</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-3"></i>Open Bus tour<span class="other_tours_price">$55</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-30"></i>Louvre museum<span class="other_tours_price">$24</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-3"></i>Madlene Cathedral<span class="other_tours_price">$24</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 other_tours">
                        <ul>
                            <li><a href="#"><i class="icon_set_1_icon-37"></i>Montparnasse<span class="other_tours_price">$36</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-1"></i>D'Orsey museum<span class="other_tours_price">$28</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-50"></i>Gioconda Louvre musuem<span class="other_tours_price">$44</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-44"></i>Tour Eiffel<span class="other_tours_price">$56</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-50"></i>Ladefanse<span class="other_tours_price">$16</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-44"></i>Notredame<span class="other_tours_price">$26</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End row -->

                <div class="banner colored add_bottom_30">
                    <h4>Discover our Top tours <span>from $34</span></h4>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                    </p>
                    <a href="single_tour.html" class="btn_1 white">Read more</a>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 text-center">
                        <p>
                            <a href="#"><img src="{{url('/')}}/src/citytour/img/bus.jpg" alt="Pic" class="img-responsive"></a>
                        </p>
                        <h4><span>Sightseen tour</span> booking</h4>
                        <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <p>
                            <a href="#"><img src="{{url('/')}}/src/citytour/img/transfer.jpg" alt="Pic" class="img-responsive"></a>
                        </p>
                        <h4><span>Transfer</span> booking</h4>
                        <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <p>
                            <a href="#"><img src="{{url('/')}}/src/citytour/img/guide.jpg" alt="Pic" class="img-responsive"></a>
                        </p>
                        <h4><span>Tour guide</span> booking</h4>
                        <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6 text-center">
                        <p>
                            <a href="#"><img src="{{url('/')}}/src/citytour/img/hotel.jpg" alt="Pic" class="img-responsive"></a>
                        </p>
                        <h4><span>Hotel</span> booking</h4>
                        <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                        </p>
                    </div>
                </div>
                <!-- End row -->
            </div>
            <!-- End container -->
        </div>
        <!-- End white_bg -->

        <section class="promo_full">
            <div class="promo_full_wp magnific">
                <div>
                    <h3>BELONG ANYWHERE</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                    </p>
                    <a href="https://www.youtube.com/watch?v=Zz5cu72Gv5Y" class="video"><i class="icon-play-circled2-1"></i></a>
                </div>
            </div>
        </section>
        <!-- End section -->

        <div class="container margin_60">

            <div class="main_title">
                <h2>Some <span>good</span> reasons</h2>
                <p>
                    Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.
                </p>
            </div>

            <div class="row">

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.2s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>+120</span> Premium tours</h3>
                        <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
                        </p>
                        <a href="about.html" class="btn_1 outline">Read more</a>
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.4s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-30"></i>
                        <h3><span>+1000</span> Customers</h3>
                        <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
                        </p>
                        <a href="about.html" class="btn_1 outline">Read more</a>
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.6s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-57"></i>
                        <h3><span>H24 </span> Support</h3>
                        <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
                        </p>
                        <a href="about.html" class="btn_1 outline">Read more</a>
                    </div>
                </div>

            </div>
            <!--End row -->

            <hr>

            <div class="row">
                <div class="col-md-8 col-sm-6 hidden-xs">
                    <img src="{{url('/')}}/src/citytour/img/laptop.png" alt="Laptop" class="img-responsive laptop">
                </div>
                <div class="col-md-4 col-sm-6">
                    <h3><span>Get started</span> with CityTours</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
                    </p>
                    <ul class="list_order">
                        <li><span>1</span>Select your preferred tours</li>
                        <li><span>2</span>Purchase tickets and options</li>
                        <li><span>3</span>Pick them directly from your office</li>
                    </ul>
                    <a href="all_tour_list.html" class="btn_1">Start now</a>
                </div>
            </div>
            <!-- End row -->

        </div>
        <!-- End container -->
    </main>
    <!-- End main -->



@endsection
