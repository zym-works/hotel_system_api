<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Temukan dan pesan aktivitas, rekreasi dan kegiatan seru dengan jaminan harga terbaik hanya di Befree Tour">
    <meta name="author" content="Befree Tour">
    <title>Akun Saya | Befree Tour</title>

    <!-- Favicons-->
<link rel="shortcut icon" href="{{url('/')}}/src/citytour/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{url('/')}}/src/citytour/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{url('/')}}/src/citytour/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{url('/')}}src/citytour/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{url('/')}}src/citytour/img/apple-touch-icon-144x144-precomposed.png">


    <!-- CSS -->

    <link href="http://www.befreetour.com/src/bismillah/css/base.css" rel="stylesheet">
    <link href="http://www.befreetour.com/src/bismillah/css/stylebismillah.css" rel="stylesheet">
    <link rel="stylesheet" href="http://www.befreetour.com/src/bismillah/css/floating.css">


    <!-- SPECIFIC CSS -->

    <link href="http://www.befreetour.com/src/bismillah/css/admin.css" rel="stylesheet">
    <link href="http://www.befreetour.com/src/bismillah/css/jquery.switch.css" rel="stylesheet">

    <!--Panggil Font Awesome -->
    <link rel="stylesheet" href="http://www.befreetour.com/src/bismillah/fonts/font-awesome/css/font-awesome.min.css">

    <!-- Google web fonts -->

   <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>

   <?php
    if(!empty($css))
    echo $css;
    ?>
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->   

</head>

<body>

<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->


    <!-- Mobile menu overlay mask -->
     <!-- Header================================================== -->

    <header>
        
        <div class="container">
            <div class="row">
                <!--logo-->
                
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div id="logo" title="Befree Tour">


                        <a href="{{url('/')}}"><img src="{{url('/')}}/src/citytour/img/korina-group-small.png" width="160" height="34" alt="City tours" data-retina="true"> </a>
                                       
                    </div>
                </div>
                
                
                <nav class="col-md-9 col-sm-9 col-xs-9">
                    
                    
                    <style>
                    
                    /*======= MENU MOBILE =========*/
                    
                    #mobile-menu {  
                        z-index: 99999;
                        position: fixed;
                        width: 100%;    
                        height: 100%;
                        top: 0; left: 0;
                        -webkit-transition: top 0.2s ease-out; 
                        transition: top 0.2s ease-out;
                        display: none;  background: #fff;
                        overflow-y: scroll; auto;   
                        padding: 0;
                    }
                    .continent { 
                        text-align: center;  
                        font-weight: 600;  
                        font-size: 15px;   
                        padding: 5px 0; 
                        background: #f5f5f5;
                    }
                    
                    .country {   
                        text-align: left; 
                        font-weight: 500;  
                        font-size: 14px;   
                        padding: 10px 0px 10px 10px;  
                        border-bottom: 1px solid #f5f5f5;
                    }
                    
                    .city {    
                        text-align: left;  
                        font-weight: 400;  
                        font-size: 13px;   
                        padding: 7px 0px 7px 28px; 
                        border-bottom: 1px solid #f5f5f5;
                    }
                    
                    .city:before {  
                        content: "\eaf4";
                        font-style: normal; 
                        font-weight: normal;  
                        font-family: "fontello";  
                        position: relative;  
                        right: 10px;   
                        color: #ddd;
                    }
                    
                    .city a {   
                        color: #777;
                    }
                    
                    .city a:hover { 
                        color: #e04f67;
                    }
                    
                    .search-input-wrapper {   
                        padding: 7px 10px;   
                        background-color: #fff; 
                        color: #bdbdbd;  
                        border-bottom: 1px solid #bdbdbd;
                    }
                    
                    .search-input-wrapper .search-form {   
                        position: relative;   
                        overflow: hidden;
                    }
                    
                    .search-back-space { 
                        margin: 3px 17px 3px 5px;
                        position: relative;   
                        font-weight: lighter;   
                        float: left;    
                        color:#bdbdbd;
                    }
                    
                    .search-input-wrapper i.fa-search {   
                        position: relative;   
                        margin-top: 7px;   
                        font-size: 16px;   
                        float: left;    
                        margin-left: 5px;
                    }
                    
                    .search-input-wrapper input {   
                        width: 82%;  
                        height: 32px;
                        padding-left: 10px;
                        border-radius: 10px; 
                        color: #000;   
                        text-align: left;  
                        font-size: 13px;  
                        background: #ececec;  
                        float: left;   
                        outline: none;   
                        border: 1px solid #fff;
                    }
                    
                    button.mobile-menu {
                        padding: 0;
                        border: none;
                        background: none;
                    }
                    
                    /*======= MODAL DESTINATION =========*/
                    
                    div#destination .nav-searchfield-outer {
                        background: none;
                    }

                    div#destination .nav {
                        display: table;
                        margin: 0;
                        padding: 0;
                    }

                    div#destination .nav a {
                        color: #999;
                    }

                    div#destination .nav > li {
                        position: relative;
                        float: left;
                        border-radius: 0px;
                        border-top: 0;
                        border-left: 0;
                        border-right: 0;
                        border-bottom: 1px solid #ddd; 
                        padding: 0px !important;
                        margin: 0px !important;
                        display: inline;
                        width: 100%;
                        text-align: left;
                        font-size: 14px;
                        font-weight: 600;
                        text-transform: uppercase;
                    }
                    
                    div#destination .nav > li:last-child {
                        border: none;
                    }
                    
                    div#destination .nav > li > a {
                        border-radius: 0x;
                        padding: 15px;
                    }

                    div#destination .nav > li.active > a,
                    div#destination .nav > li.active > a:hover,
                    div#destination .nav > li.active > a:focus {
                        border-radius: 0px;
                        background: #e04f67;
                        color: #f9f9f9;
                    }

                    div#destination .modal-body {
                        padding: 0px;
                    }

                    div#destination .row{
                        margin: 0;
                        padding: 0;
                    }

                    div#destination .tab-content {
                        border: none;
                        border-left: 1px solid #ddd;
                        border-radius: 0px;
                        margin: 0;
                        padding: 0;
                        min-height: 250px;
                        background: none;
                    }

                    div#destination .tab-content > .tab-pane {
                        background: none;
                        border: none;
                    }

                    div#destination table td:first-child {
                        width: 33%;
                        text-align: left;
                        padding-right: 7px;
                        padding-bottom: 10px;
                        color: #e04f67;
                        font-size: 14px;
                        font-weight: 600;
                        text-transform: uppercase;
                        vertical-align: top;
                    }

                    div#destination span {
                        border-right: 1px solid #999;
                        padding: 0px 10px;
                        margin-bottom: 10px;
                        display: inline-block;
                        text-transform: uppercase;
                    }

                    div#destination span:last-child {
                        border: none;
                    }

                    div#destination table td a {
                        color: #777;
                        font-size: 14px;
                        text-transform: uppercase;
                    }

                    div#destination table td a:hover {
                        color: #e04f67;
                    }

                    div#destination tr {
                        margin-bottom: 10px
                    }
                    
                    div#destination .close {
                        top: 0;
                        position: absolute;
                        right: -26px;
                        color: #ffffff;
                        opacity: 1;
                        font-size: 30px;
                        font-weight: 100;
                    }

                    div#destination .modal-header {
                        padding: 0;
                    }   

                    div#destination .search_bar {
                        width: 100%;
                    }

                    div#destination #twotabsearchtextbox {
                        width: 100%;
                        padding: 3px 0px 0px 50px;
                        outline: none;
                        font-weight: 500;
                        color: #555;
                    }
                    
                    div#destination .nav-submit-button {
                        left: 5px;
                        right: 0;
                    }
                    div#destination .col-md-4 {
                        padding: 5px;
                    }

                    div#destination .tab-pane h3 {
                        color: #fff;
                        margin-bottom: 6px;
                        font-size: 13px;
                        font-weight: 400;
                    }
                    
                    div#destination .img-responsive {
                    border-radius: 5px;
                    }
                    
                    .topdest-menu {
                        text-align: center;
                        position: relative;
                        display: block !important;
                        color: #fff;
                        font-size: 13px;
                        font-weight: 600;
                        text-shadow: 0px 0px 10px #111;
                        top: 60%;
                    }
                    
                    .topdest-show {
                        text-align: center;
                        display: inline-block;
                        position: relative;
                        width: 100%;
                        font-weight: 700;
                        text-transform: uppercase;
                        top: 15px;
                        color: #777;
                    }

                    .topdest-col {
                        border: 2px solid #fff;
                        border-radius: 5px;
                        padding: 5px !important;
                    }
                    
                    /* END MODAL DESTINATION*/
                                        
                    </style>


                    
                    
                    <ul id="top_tools">


{{--                         <li id="cart_list"> 
                            <div class="dropdown dropdown-cart">
                                <a href="#" class="dropdown-toggle" id="cart_link" data-toggle="dropdown">Keranjang </a>
                                                                    <ul class="dropdown-menu" id="cart_items">
                                        <li>Keranjang Belanja Anda Kosong.</li>
                                    </ul>
                                                            </div><!-- End dropdown-cart-->
                        </li> --}}
                         
                                                <!--<div class="dropdown dropdown-cart" id="dropdown-crncy">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> IDR <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu-new" id="cart_items">
                                                                 <li><a href="#" onclick="ChangeCurrency('HKD');"><span>HK$ | HKD</span></a></li>
                                                                 <li><a href="#" onclick="ChangeCurrency('IDR');"><span>Rp | IDR</span></a></li>
                                                                 <li><a href="#" onclick="ChangeCurrency('SGD');"><span>S$ | SGD</span></a></li>
                                                                 <li><a href="#" onclick="ChangeCurrency('USD');"><span>$ | USD</span></a></li>
                                                                 <li><a href="#" onclick="ChangeCurrency('KRW');"><span>₩ | KRW</span></a></li>
                                                                 <li><a href="#" onclick="ChangeCurrency('JPY');"><span>¥ | JPY</span></a></li>
                                                                 <li><a href="#" onclick="ChangeCurrency('TWD');"><span>NT$ | TWD</span></a></li>
                                                            </ul>
                        </div>-->
                        
                                                    
                             <li id="mylogin">
                                <a href="{{url('/')}}/agent/logout" id="access_link">Keluar</a>
                            </li>
                            
                                                    </div>
                    </ul>
                </nav>
            </div>
        </div><!-- container -->
    </header><!-- End Header -->
    

<div class="user_header">
    <div class="user_container" id="gradientAni">
        <div class="user_tittle">
             <h1>Halo {{$agent->fullname}}</h1>
        </div>
    </div>
</div>

<style>

            #gradientAni {
            will-change: opacity;
                transition: opacity 0.3s ease-out;
                background-color: #9fc;
                background-image: linear-gradient(to top,#C9283E 0%,#e04f67 50%, #C9283E 100%);
                -webkit-background-size: 100% 400%;
                -moz-background-size: 100% 400%;
                background-size: 100% 400%;
                -webkit-animation: grad-anim 21s infinite linear;
                -moz-animation: grad-anim 21s infinite linear;
                animation: grad-anim 21s infinite linear;
            }

            @-webkit-keyframes grad-anim{to{background-position:0 400%;}}
            @-moz-keyframes grad-anim{to{background-position:0 400%;}}
            @keyframes  grad-anim{to{background-position:0 400%;}}

                    .user_header {
                        height: 320px;
                    }
                    
                    .user_container {
                        width: 100%;
                        height: 100%;
                        display: table;
                    }
                    
                    .user_tittle {
                        position: absolute;
                        width: 100%;
                        top: 180px;
                        text-align: center;
                    }
                    
                    .user_tittle h1 {
                        font-size: 32px;
                        font-weight: 700;
                        color: #fff;
                        text-transform: uppercase;
                        text-shadow: 0px 2px 10px #000000a6;
                    }

                    div#user_dash {
                        border: 1px solid #ddd;
                        border-radius: 5px;
                        background: rgba(245, 245, 245, 1);
                    }

                    div#user_dash .nav {
                        display: table;
                        margin: 0;
                        padding: 10px;
                    }

                    div#user_dash .nav a {
                        color: #999;
                    }

                    div#user_dash .nav > li {
                        position: relative;
                        float: left;
                        border-radius: 0px;
                        border-top: 0;
                        border-left: 0;
                        border-right: 0;
                        padding: 0px !important;
                        margin: 0px !important;
                        display: inline;
                        width: 100%;
                        text-align: left;
                        font-size: 14px;
                        font-weight: 500;
                    }
                    div#user_dash .nav > li > a {
                        border-radius: 0;
                        padding: 10px 0px 10px 20px;
                    }
                    
                    div#user_dash .nav > li > a > span {
                        padding-left: 15px;
                    }

                    div#user_dash .nav > li.active > a,
                    div#user_dash .nav > li.active > a:hover,
                    div#user_dash .nav > li.active > a:focus {
                        border-radius: 0px;
                        background: none;
                        color: #e04f67;
                        font-weight: 600;
                    }

                    div#user_dash .modal-body {
                        padding: 0px;
                    }

                    div#user_dash .row{
                        margin: 0;
                        padding: 0;
                    }

                    div#user_dash .tab-content {
                        border: none;
                        border-left: 1px solid #ddd;
                        border-radius: 0px;
                        margin: 0;
                        padding: 0;
                        background: none;
                    }

                    div#user_dash .tab-content > .tab-pane {
                        background: #fff;
                        border: none;
                        min-height: 560px;
                        margin: 0;
                    }

                    div#user_dash tr {
                        margin-bottom: 10px
                    }
                    
                    div#user_dash h4 {
                        font-weight: 600;
                        color: #e04f67;
                        text-transform: capitalize;
                    }
                    
</style>



<div class="margin_30 container">
    <div id="user_dash" class="row">

        <ul class="nav tab-menu nav-pills col-md-3 nav-stacked pr15">           
            <li class="active"><a href="#section-1" class="icon-vcard" data-toggle="tab"><span>Akun Saya</span></a></li>
            <li><a href="#section-2" class="icon-calendar-outlilne" data-toggle="tab"><span>Pesanan</span></a></li>
            {{-- @foreach($agent as $x) @endforeach --}}
            @if($agent->akses == 1)
                <li><a href="#section-3" class="icon-heart-2" data-toggle="tab"><span>History Saldo</span></a></li>
                <li><a href="#section-4" class="icon-pencil-2" data-toggle="tab"><span>Topup Saldo</span></a></li>
                <li><a href="#section-5" class="icon-question" data-toggle="tab"><span>Member</span></a></li>
            @endif
            <li><a href="{{url('/')}}/agent/logout" class="icon-off-1"><span>Keluar</span></a></li>
        </ul>

        <div class="tab-content col-md-9">
            <div class="tab-pane well active in active" id="section-1">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Akun saya</h4>
                        <hr>
                    </div>

                     @foreach($data_agent as $data)

                                <div class="row">
                                    <div class="col-sm-4">Date Registered</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7">{{$data->agent_date_register}}</div>
                                    <div class="col-sm-4">Account Number</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7">{{$data->agent_account_number}}</div>
                                    <div class="col-sm-4">Agent Name</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7">{{$data->agent_name}}</div>
                                    <div class="col-sm-4">Phone number</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7">{{$data->agent_telephone}}</div>
                                    <div class="col-sm-4">Email</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7">{{$data->agent_email}}</div>
                                    <div class="col-sm-4">Address</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7">{{$data->agent_address}}</div>
                                    <div class="col-sm-4">Country</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7">{{$data->agent_country}}</div>
                                    <div class="col-sm-4">City</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7">{{$data->agent_city}}</div>
                                    <div class="col-sm-4">Last Balance</div>
                                    <div class="col-sm-1">:</div>
                                    <div class="col-sm-7"><strong>Rp. {{ number_format($data->last_balance) }}</strong></div>
                                </div>
                               
                                @endforeach
                </div>
            </div><!-- End row -->

            <div class="tab-pane well fade" id="section-2">
                <div class="row">
                        <div class="col-md-12">
                            <h4>Pesanan </h4>
                           @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif   
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach
                            <hr>
                             @if(count($bookings)>0)
                            <?php  foreach ($bookings as $booking) { ?>
                            
                    <div class="strip_booking">
                        
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <?php
                                //pisahkan tanggal
                                        $array1=explode("-",$booking->date_booking);
                                        $tahun=$array1[0];
                                        $bulan=$array1[1];
                                        $sisa1=$array1[2];
                                        $array2=explode(" ",$sisa1);
                                        $tanggal=$array2[0];
       
                                //ubah nama bulan menjadi bahasa indonesia
                                        switch($bulan)
                                        {
                                        case"01";
                                        $bulan="January";
                                        break;
                                        case"02";
                                        $bulan="February";
                                        break;
                                        case"03";
                                        $bulan="March";
                                        break;
                                        case"04";
                                        $bulan="April";
                                        break;
                                        case"05";
                                        $bulan="May";
                                        break;
                                        case"06";
                                        $bulan="June";
                                        break;
                                        case"07";
                                        $bulan="July";
                                        break;
                                        case"08";
                                        $bulan="August";
                                        break;
                                        case"09";
                                        $bulan="September";
                                        break;
                                        case"10";
                                        $bulan="October";
                                        break;
                                        case"11";
                                        $bulan="November";
                                        break;
                                        case"12";
                                        $bulan="December";
                                        break;
                                        }
                                        $nameOfDay = date('D', strtotime($booking->date_booking));
                                ?>
                                <div class="date">
                                    <span class="month">{{$bulan}}</span>
                                    <span class="day"><strong>{{$tanggal}}</strong>{{$nameOfDay}}</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <h3 class="hotel_booking">{{ $booking->hotel_name }}<span>{{$booking->adult_num}} Adults / <?php if (!empty($booking->child_num)){ echo $booking->child_num.' Anak /'; } ?> {{$booking->night}} Nights</span></h3>
                            </div>
                            <div class="col-md-2 col-sm-3">
                                <ul class="info_booking">
                                    <li><strong>Booking id</strong> {{$booking->reservation_number}}</li>
                                    <?php if ($booking->reservation_status==1) { ?>
                                    <li><strong>Booked on</strong> {{$booking->date_confirm}}</li>
                                    <?php }elseif ($booking->reservation_status==2) {?>
                                    <li><strong><small>Canceled on</small></strong> {{$booking->date_confirm}}</li>
                                    <?php }elseif ($booking->reservation_status==3) {?>
                                    <li><strong>Pending</strong></li>
                                    <?php }else{ ?>
                                    <li><strong>status null</strong></li>
                                    <?php } ?>
                                  
                                </ul>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="booking_buttons">
                                    {{-- <a href="#" onclick="return confirm('Are you sure to confirm your booking ?');" class="btn_2">Confirm Booking</a> --}}
                                    @if ($booking->reservation_status==1)
                                    <a href="{{url('api/booking/confirm/').'/'.$booking->mg_os_ref_no.'/'.$booking->mg_hbook_id.'/'.$agent->id_agent}}" onclick="return confirm('Are you sure to cancel your booking ?');" class="btn_2">Cancel Booking</a>
                                    @elseif ($booking->reservation_status==2)
                                        <div align="center">
                                            <small>
                                                Refund <br>Rp. {{number_format($booking->refund_idr)}}
                                            </small>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                    </div>
                    
                    <?php  } ?>
                     <div align="center"> {!! str_replace('/?', '?', $bookings->render()) !!} </div>
                             @endif
                        </div>
                </div>      
             <style>

.img_prod_det {
    min-height: 200px;
    background-size: cover !important;
}   

.booking_date {
    position: relative;
    float: right;
    text-transform: uppercase;
}

.booking_code {
    position: relative;
    float: left;
    color: #e04f67;
    font-weight: 600;
}

.header_booking {
    background: #f5f5f5;
    padding: 9px 10px;
    height: 35px;
}

.booking_detail {
    border: 1px solid #ddd;
    border-radius: 5px;
    margin: 5px !important;
}

.prod_det {
    text-transform: uppercase;
}



</style>

        
    

    <div class="text-center">
            </div>          </div>
            
            <!-- End section 1 -->

            <div class="tab-pane well fade" id="section-3">
                <div class="row">
                        <div class="col-md-12">
                            <h4>History Saldo</h4>
                            <hr>
                            <?php if ($session_user['akses']==1) { ?>
                        <div class="col-sm-12">
                                  <table class="table table-striped" id="topup">
                                      <thead>
                                          <th>No</th>
                                          <th>Date Transaction</th>
                                          <th>Debit</th>
                                          <th>Kredit</th>
                                          <th>Status</th>
                                          <th>Date Confirm</th>
                                          <th>Created By</th>
                                          <th>Balance Saldo</th>
                                      </thead>
                                      <tbody></tbody>
                                  </table>                     
                        </div>
                        <?php } ?>
                        </div>
                </div>  
                <div class="row">
                            
            </div><!-- End row -->
            <div class="text-center">
                                </div>          </div>
            
            <!-- End section 2 -->

            <div class="tab-pane well fade" id="section-4">
                <div class="row">
                        <div class="col-md-12">
                             <h4>Transfer Confirmation Form </h4>
                            <hr>
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach
                    <form method="POST" action="{{url('/agent/top_up')}}"  enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="id_member" value="{{ $session_user['id_member'] }}">
                        <input type="hidden" name="id_agent" value="{{ $session_user['id_agent'] }}">
                        <input type="hidden" name="username" value="{{ $session_user['username'] }}">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Pilih Bank</label>
                                <select id="bank" name="bank" class="form-control" >
                                    <option value="BCA">BCA</option>
                                    <option value="BNI">BNI</option>
                                    <option value="BRI">BRI</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Pemilik Nama Rekening</label>
                                <input class="form-control" name="pemilik" id="pemilik" type="text">
                            </div>
                        </div>
                    </div>
                    <!-- End row -->

                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Nominal yang dikirim</label>
                                <input class="form-control" name="top_up" id="top_up" type="number">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Pesan
                                </label>
                                <textarea class="form-control"  name="pesan" id="pesan"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->

                    <hr>

                    <h4>Upload bukti transfer</h4>
                    <div class="form-inline upload_1">
                        <div class="form-group">
                            <input type="file" name="file">
                        </div>
                    </div>

                        <hr>
                        <button type="submit" class="btn_1 green">Konfirmasi</button>
                    </form>
                        </div>
                </div>  
                <div class="row">


            
        <div class="text-center">

                
                </div>

        </div>          </div>
            
            <!-- End section 4 -->

            <div class="tab-pane well fade" id="section-5">
                <div class="Pertanyaan">
                        <div class="col-md-12">
                            <h4>Member</h4>
                            <hr>
                              <div align="right"><a href="{{ url('/agent/member/add') }}" class="btn btn-primary" role="button">New Member</a></div><br><p>
                            <table class="table table-striped" id="member">
                                <thead>
                                    <th>No</th>
                                    <th>Fullname</th>
                                    <th>Username</th>
                                    <th>Password</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                </div>
                <div class="row">

            
        <div class="text-center">

                
                </div>

        </div>          </div>

        </div><!-- End content -->

    </div><!-- End tabs -->

<!--    
    <br>
        <div class="panel-group">
        <div class="panel panel-default">
            <div id="collapseOne_payment" class="panel-collapse collapse in">
            <div class="panel-body sign-out">
                <h4>SETTING</h4>                
                <hr class="margin-t0">
                <a href="http://www.befreetour.com/id/customer_logout"><i class="icon-off-1"></i> <span>Keluar</span></a>
                <br>
            </div>
            </div>
        </div>
        </div>
    -->
 
 </div>


<section class="panel-footer-why">

<div class="container">

        <div class="main_title">

            <h2>Mengapa<span> BeFreeTour ?</span></h2>

            <p>

   

            </p>

        </div>

        

        <div class="row">

        

            <div class="col-md-3 col-sm-3">

                <div class="feature_why">

                    <i class="icon_set_1_icon-30"></i>

                    <h3><span>BE FREE</span></h3>

                    <p>

                        Ingin berpergian sendiri atau butuh seorang pemandu? Anda akan menikmati aktivitas yang anda pilih


                    </p>

                </div>

            </div>

            

            <div class="col-md-3 col-sm-3">

                <div class="feature_why">

                    <i class="icon_set_1_icon-33"></i>

                    <h3><span>BE CONFIDENT</span></h3>

                    <p>

                        Yakinkan keputusan pemesanan Anda dengan memeriksa ulasan, foto dan artikel dari pengguna kami.

                         

                    </p>

                </div>

            </div>

            

            <div class="col-md-3 col-sm-3">

                <div class="feature_why">

                    <i class="icon_set_1_icon-18"></i>

                    <h3><span>BE PIONEER</span></h3>

                    <p>

                        Tidak tahu akan kemana dan kapan pergi? Kami bisa mengatur segalanya untuk kenyamanan Anda.

                    </p>

                </div>

            </div>

             <div class="col-md-3 col-sm-3">

                <div class="feature_why">

                    <i class="icon_set_1_icon-63"></i>

                    <h3><span>BE FAST</span></h3>

                    <p>
                        Liburan tak terlupakan dan mengagumkan hanya dengan satu klik.


                    </p>

                </div>

            </div>

            

            

        </div><!--End row -->

        

        

    </div><!-- End container -->

</section>

<footer>



        <div class="container"> 

        <div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="false" data-auto-logout-link="true"></div>

            <div class="row">

                <div class="col-md-9 col-sm-9">                 
                    <br>
                    <ul>
                        <li><span>Befree Tour</span> <br>Befree Tour Indonesia is Owned and Operated by PT. Korina Prima Sinergi</li>           
                        <li>ID Travel License: 556/0135/B-11/VII/2016 | KR Travel License : 382-12-00133 
                        <br>Telp: 0271-7451109 | Phone: 082137370942</li>
                        <li>Head Office: Jl. Yosodipuro No. 158 Mangkubumen Surakarta 57139</li>        
                        <li>Bali Branch: Jl. Pulau Belitung, Babakan Sari VII, Denpasar, Bali</li>
                        <li> Yogya Branch: Kranggan Murtigading Sanden Bantul, Daerah Istimewa Yogyakarta </li>
                        <li>Korea Branch : 3rd Floor, 24-1, Gwangmyeong-ro, Jungwon-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</li>
                    </ul>

                    <div class="margin-50"></div>
                    
                    <ul><a href="http://www.befreetour.com/id/aboutus">Tentang Kami</a> &nbsp
                    <a href="http://www.befreetour.com/id/register">Daftar</a> &nbsp
                    <a href="http://www.befreetour.com/id/term">Sebelum Reservasi</a>&nbsp 
                    <a href="http://www.befreetour.com/id/policy">Kebijakan Privasi </a>&nbsp
                    <a href="http://www.befreetour.com/id/help/faq">FAQ</a>&nbsp
                    </ul>
                </div>
 
                <div class="col-md-3 col-sm-3">
                    <br>
                    <ul>
                      <li>Layanan Kepuasan Pelanggan</li>
                    </ul>
                    <ul class="margin_ask">
                    <a href="http://www.befreetour.com/id/ask" class="btn_ask_outline">Customer Center</a>
                    </ul>
                    <ul>

                        <li>Jam Kerja : 09:30 ~ 18:00 (GMT+7)
                        <br>Jam Istirahat : 12:00 ~ 13:00 (GMT+7)</li>
                        <li>Reservasi : help@befreetour.com</li>
                        <li>Kontak Kami : info@befreetour.com</li>
                    </ul><br>
                </div>


            </div><!-- End row -->

            

            <div class="row">



                

                <!-- <div class="col-md-3 col-sm-3">

                    <h3>Settings</h3>

               

                    <div class="styled-select">

                     <select class="form-control" name="currency" id="currency" onchange="myFunction(this.value)" >

                     <option value=""></option> <option value="1">HKD</option> <option value="2">IDR</option> <option value="3">SGD</option> <option value="4">USD</option> <option value="5">KRW</option> <option value="6">JPY</option> <option value="7">TWD</option>                        </select>
                    </div>
                </div>-->
            </div>
            <div class="row">
            <div id="social_footer">
                <div class="col-md-9 col-sm-9">
                    <ul>                    
                    <li>  Copyright 2017 © PT Korina Prima Sinergi.</li>
                    </ul>  
                </div>
                <div class="col-md-3 col-sm-3">               
                     <ul> 

                    <img src="http://www.befreetour.com/src/bismillah/img/logo-footer-id.png" class="img-responsive" width="80%" height="80%">                  
                    </ul>  
                </div>
            </div><!-- End row -->

        </div><!-- End container -->

    </footer><!-- End footer -->

        <script src="http://www.befreetour.com/src/bismillah/js/jquery-1.11.2.min.js"></script>

</body>


<div id="toTop"></div><!-- Back to top button -->






 <!-- Common scripts -->

<script src="http://www.befreetour.com/src/bismillah/js/jquery-1.11.2.min.js"></script>
<script src="http://www.befreetour.com/src/bismillah/js/common_scripts_min.js"></script>
<script src="http://www.befreetour.com/src/bismillah/js/functions.js"></script>

 <!-- Specific scripts -->

<script src="http://www.befreetour.com/src/bismillah/js/tabs.js"></script>

<?php
            if(!empty($js))
            echo $js;
        ?>
  </body>
</html>