@extends('citytour.main')
@section('content')
 <section id="hero_2">
        <div class="intro_title animated fadeInDown">
            <h1>Place your order </h1>
            <div class="bs-wizard">

                <div class="col-xs-4 bs-wizard-step complete">
                    <div class="text-center bs-wizard-stepnum">Your cart</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="cart_hotel.html" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum">Your details</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step disabled">
                    <div class="text-center bs-wizard-stepnum">Finish!</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="confirmation_hotel.html" class="bs-wizard-dot"></a>
                </div>

            </div>
            <!-- End bs-wizard -->
        </div>
        <!-- End intro-title -->
    </section>
    <!-- End Section hero_2 -->
    
    <main>
        <div id="position">
            <div class="container">
                <ul>
                   <li><a href="{{url('/')}}">Home</a>
                    </li>
                    <li><a href="{{url('/list/hotel/bangkok')}}">List Hotel</a>
                    </li>
                    <li><a href="{{url('/detail/hotel').'/'.$hotel->id.'-'.str_replace(' ', '-', $_SESSION['dataOrder'][6]).'.html'}}">{{$_SESSION['dataOrder'][6]}}</a>
                    </li>
                    <li>Payment</li>
                </ul>
            </div>
        </div>
        <!-- End position -->

        <div class="container margin_60">
            <div class="row">
                <div class="col-md-8 add_bottom_15">
                     @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach
                    <div class="form_title">
                        <h3><strong>1</strong>Your Details</h3>
                        <p>
                            Mussum ipsum cacilds, vidis litro abertis.
                        </p>
                    </div>
                    <div class="step">
                        <div class="row">

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Account Number</label>
                                    <input type="text" class="form-control" id="firstname_booking" value="{{$_SESSION['dataAgent']['agent_account_number']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Agent Name</label>
                                    <input type="text" class="form-control" id="lastname_booking" value="{{$_SESSION['dataAgent']['agent_name']}}" name="lastname_booking" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" id="email_booking" name="email_booking" value="{{$_SESSION['dataAgent']['agent_email']}}" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Saldo</label>
                                    <?php $saldo=$_SESSION['dataAgent']['last_balance'];?>
                                    <input type="email" id="email_booking_2" name="saldo_agent" value="Rp {{number_format($_SESSION['dataAgent']['last_balance'], 0,".",".")}}  "class="form-control" readonly>
                                </div>
                            </div>
                        </div>

                    </div>
                 
                    <!--End step -->

                    <div class="form_title">
                        <h3><strong>2</strong>Guest Information</h3>
                        <p>
                            Informasi Pengunjung
                        </p>
                    </div>
                    <div class="step">
                        <form method="POST" action="{{URL::to('hotel/post-pay')}}">
                            {{ csrf_field() }}
                        <input type="hidden" name="numChild" value="{{$_SESSION['dataOrder'][15]}}">
                        <input type="hidden" name="ageChild1" value="{{$_SESSION['dataOrder'][16]}}">
                        <input type="hidden" name="ageChild2" value="{{$_SESSION['dataOrder'][17]}}">
                        <input type="hidden" name="agent_account_number" value="{{$_SESSION['dataAgent']['agent_account_number']}}" >
                        <input type="hidden" name="agent_id" value="{{$_SESSION['dataAgent']['id_agent']}}" >
                        <input type="hidden" name="idr" value="{{$_SESSION['dataOrder'][5]}}">
                        <input type="hidden" name="agent_name" value="{{$_SESSION['dataAgent']['agent_name']}}" name="lastname_booking" readonly>
                        {{--{{$_SESSION['dataOrder'][4]}}--}}
                        <?php
                        for($i=1;$i<=$_SESSION['dataOrder'][4];$i++)
                            {
                                ?>
                        <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12"><label>Guest Name {{$i}}</label></div>
                            <div class="col-sm-3"> 
                                <select class="form-control" id="sel1" name="title[]">
                                    <option value="Mr.">Mr </option>
                                    <option value="Mrs.">Mrs </option>    
                                </select>
                            </div>
                            <div class="col-sm-9"> 
                                <input type="text" class="form-control" id="name_card_bookign" name="guest[]" required>
                            </div>
                        </div>
                        </div>
                        <?php
                            }
                        ?>{{--
                        <div class="form-group">
                            <label>Guest Name 2</label>
                            <input type="text" class="form-control" id="name_card_bookign" name="name_card_bookign">
                        </div>--}}



                        <hr>
                    </div>
                </div>


                <aside class="col-md-4">
                    <div class="box_style_1">
                        <h3 class="inner">- Summary -</h3>

                        <table class="table table_summary">
                            <tbody>
                            <tr>
                                <td>
                                    Reservation No
                                </td>
                                <td class="text-right">
                                    <?php $noRsv="KG-RSV-".date("d")."/".date("m")."/".date("y")."-".rand(100,900);?>
                                    <b>{{$noRsv}}</b>
                                    <input type="hidden" name="no_rsv" value="<?=$noRsv;?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Hotel
                                </td>
                                <td class="text-right">
                                    {{$_SESSION['dataOrder'][6]}}
                                </td>
                            </tr>


                                <tr>
                                    <td>
                                        Check in
                                    </td>
                                    <td class="text-right">
                                        {{date('d F Y', strtotime($_SESSION['dataOrder'][0]))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Check out
                                    </td>
                                    <td class="text-right">

                                        {{date('d F Y', strtotime($_SESSION['dataOrder'][1]))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Rooms
                                    </td>
                                    <td class="text-right">
                                        {{$_SESSION['dataOrder'][2]}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nights
                                    </td>
                                    <td class="text-right">
                                        {{$_SESSION['dataOrder'][3]}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Adults
                                    </td>
                                    <td class="text-right">
                                        {{$_SESSION['dataOrder'][4]}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Childs
                                    </td>
                                    <td class="text-right">
                                        {{$_SESSION['dataOrder'][15]}}
                                    </td>
                                </tr>

                            <tr class="total" style="color:green" >
                                <td>
                                    Saldo
                                </td>
                                <td class="text-right" >
                                    <sup><small>RP</small></sup>
                                    <?php
                                   /* $saldo =9000000;*/
                                    ?>
                                    {{number_format($saldo, 0,".",".")}}

                                </td>
                            </tr>

                                <tr class="total">
                                    <td>
                                        Total pay
                                    </td>
                                    <td class="text-right">
                                        <sup><small>RP</small></sup>
                                        {{number_format($_SESSION['dataOrder'][5], 0,".",".")}}
                                    </td>
                                </tr>

                            <tr class="total" style="color:black" >
                                <td>
                                    Sisa
                                </td>
                                <td class="text-right" >
                                    <sup><small>RP</small></sup>
                                    {{number_format($saldo-$_SESSION['dataOrder'][5], 0,".",".")}}
                                </td>
                            </tr>


                            </tbody>
                        </table>
                         {{--<a class="btn_full" href="{{URL::to('hotel/pay')}}">Book now</a>--}}

                       {{-- <a class="btn_full" href="{{URL::to('hotel/post-pay')}}">Book now</a>--}}
                        <input class="btn_full" type="submit" value="Book Now"/>
                        <a class="btn_full_outline" href="{{ url('/list/hotel/bangkok') }}"><i class="icon-right"></i> Modify your search</a>
                    </div>
                    <div class="box_style_4">
                        <i class="icon_set_1_icon-57"></i>
                        <h4>Need <span>Help?</span></h4>
                        <a href="tel://004542344599" class="phone">+45 423 445 99</a>
                        <small>Monday to Friday 9.00am - 7.30pm</small>
                    </div>
                </aside>

            </div>
            <!--End row -->
        </div>
        <!--End container -->
    </main>
    <!-- End main -->
@endsection