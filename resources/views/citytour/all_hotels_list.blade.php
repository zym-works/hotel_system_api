<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="Citytours - Premium site template for city tours agencies, transfers and tickets.">
	<meta name="author" content="Ansonika">
	<title>CITY TOURS - City tours and travel site template by Ansonika</title>

	<!-- Favicons-->
	<link rel="shortcut icon" href="{{url('/')}}/src/citytour/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="{{url('/')}}/src/citytour/img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{url('/')}}/src/citytour/img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{url('/')}}/src/citytour/img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{url('/')}}/src/citytour/img/apple-touch-icon-144x144-precomposed.png">
	
	<!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i" rel="stylesheet">

	<!-- CSS -->
	<link href="{{url('/')}}/src/citytour/css/base.css" rel="stylesheet">

	<!-- Radio and check inputs -->
	<link href="{{url('/')}}/src/citytour/css/skins/square/grey.css" rel="stylesheet">
	<link href="{{url('/')}}/src/citytour/css/ytLoad.jquery.css" rel="stylesheet" type="text/css">

	<!-- Range slider -->
	<link href="{{url('/')}}/src/citytour/css/ion.rangeSlider.css" rel="stylesheet">
	<link href="{{url('/')}}/src/citytour/css/nprogress.css" rel="stylesheet">
	<link href="{{url('/')}}/src/citytour/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="{{url('/')}}/src/citytour/css/pace-theme-minimal.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="{{url('/')}}/src/citytour/js/jquery-2.2.4.min.js"></script>
	<script src="{{url('/')}}/src/citytour/js/pace.js"></script>
    <script src="{{url('/')}}/src/citytour/js/nprogress.js"></script>
	<script type="text/javascript" src="{{url('/')}}/src/citytour/js/jquery.transit.js"></script>
	<script type="text/javascript" src="{{url('/')}}/src/citytour/js/ytLoad.jquery.js"></script>

	<!--[if lt IE 9]>
      <script src="{{url('/')}}/src/citytour/js/html5shiv.min.js"></script>
      <script src="{{url('/')}}/src/citytour/js/respond.min.js"></script>
    <![endif]-->

<style>
	#progress {
		position:fixed;
		z-index:2147483647;
		top:0;
		left:-6px;
		width:0%;
		height:2px;
		background:#b91f1f;
		-moz-border-radius:1px;
		-webkit-border-radius:1px;
		border-radius:1px;
		-moz-transition:width 500ms ease-out,opacity 400ms linear;
		-ms-transition:width 500ms ease-out,opacity 400ms linear;
		-o-transition:width 500ms ease-out,opacity 400ms linear;
		-webkit-transition:width 500ms ease-out,opacity 400ms linear;
		transition:width 500ms ease-out,opacity 400ms linear
	}
	#progress.done {
		opacity:0
	}
	#progress dd,#progress dt {
		position:absolute;
		top:0;
		height:2px;
		-moz-box-shadow:#b91f1f 1px 0 6px 1px;
		-ms-box-shadow:#b91f1f 1px 0 6px 1px;
		-webkit-box-shadow:#b91f1f 1px 0 6px 1px;
		box-shadow:#b91f1f 1px 0 6px 1px;
		-moz-border-radius:100%;
		-webkit-border-radius:100%;
		border-radius:100%
	}
	#progress dd {
		opacity:1;
		width:20px;
		right:0;
		clip:rect(-6px,22px,14px,10px)
	}
	#progress dt {
		opacity:1;
		width:180px;
		right:-80px;
		clip:rect(-6px,90px,14px,-6px)
	}
	@-moz-keyframes pulse {
		30% {
			opacity:1
		}
		60% {
			opacity:0
		}
		100% {
			opacity:1
		}
	}
	@-ms-keyframes pulse {
		30% {
			opacity:.6
		}
		60% {
			opacity:0
		}
		100% {
			opacity:.6
		}
	}
	@-o-keyframes pulse {
		30% {
			opacity:1
		}
		60% {
			opacity:0
		}
		100% {
			opacity:1
		}
	}
	@-webkit-keyframes pulse {
		30% {
			opacity:.6
		}
		60% {
			opacity:0
		}
		100% {
			opacity:.6
		}
	}
	@keyframes pulse {
		30% {
			opacity:1
		}
		60% {
			opacity:0
		}
		100% {
			opacity:1
		}
	}
	#progress.waiting dd,#progress.waiting dt {
		-moz-animation:pulse 2s ease-out 0s infinite;
		-ms-animation:pulse 2s ease-out 0s infinite;
		-o-animation:pulse 2s ease-out 0s infinite;
		-webkit-animation:pulse 2s ease-out 0s infinite;
		animation:pulse 2s ease-out 0s infinite
	}
</style>

</head>

<body>
<div id="progress" class="waiting">
	<dt></dt>
	<dd></dd>
</div>

	<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->

	<div id="preloader">
		<div class="sk-spinner sk-spinner-wave">
			<div class="sk-rect1"></div>
			<div class="sk-rect2"></div>
			<div class="sk-rect3"></div>
			<div class="sk-rect4"></div>
			<div class="sk-rect5"></div>
		</div>
	</div>
	<!-- End Preload -->

	<div class="layer"></div>
	<!-- Mobile menu overlay mask -->

	<!-- Header================================================== -->
	<header>
		<div id="top_line">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6"><i class="icon-phone"></i><strong>0045 043204434</strong>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-6">
						<ul id="top_links">
							<li>
								<div class="dropdown dropdown-access">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="access_link">
                                        <?php 

                                            if (!empty($session_user['username'])){  
                                                echo "Welcome ".$session_user['username'];
                                            }else{
                                               echo "Sign in"; 
                                            }
                                        ?> 
                                    </a>
                                    <?php
                                    if (isset($session_user['username'])){ ?>
                                     <div class="dropdown-menu">
                                        <div class="login-or">
                                            <hr class="hr-or">
                                            <span class="span-or">Hello</span><br>
                                        
                                        </div>
                                        <div class="dropdown-menu">
                                            <i class="ti-user m-r-10 text-custom"></i>helo
                                        </div>
                                        <div class="row">
                                        <div align="left" class="col-md-6">
                                            <a href="{{url('/')}}/agent" class="btn btn-primary" role="button"><i class="ti-power-off m-r-10 text-default"></i> Dashboard</a>
                                        </div>
                                        <div align="right" class="col-md-6">
                                            <a href="{{url('/')}}/agent/logout" class="btn btn-danger" role="button"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a>
                                        </div>
                                    </div>
                                    </div>
                                    <?php
                                        } else {
                                    ?>
                                    <div class="dropdown-menu">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_facebook">
                                                    <i class="icon-facebook"></i>Facebook </a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_paypal">
                                                    <i class="icon-paypal"></i>Paypal </a>
                                            </div>
                                        </div>
                                        <div class="login-or">
                                            <hr class="hr-or">
                                            <span class="span-or">or</span>
                                        </div>
                                        <form action="{{route('agent.signin')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="inputUsernameEmail" id="inputUsernameEmail" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Password">
                                        </div>
                                        <a id="forgot_pw" href="#">Forgot password?</a>
                                        <input type="submit" name="Sign_in" value="Sign in" id="Sign_in" class="button_drop">
                                        <a href="{{url('/register')}}"><input type="button" name="Sign_up" value="Sign up" id="Sign_up" class="button_drop"></a>
                                        </form>
                                    </div>
                                    <?php } ?>

                                </div><!-- End Dropdown access -->
								<!-- End Dropdown access -->
							</li>
						</ul>
					</div>
				</div>
				<!-- End row -->
			</div>
			<!-- End container-->
		</div>
		<!-- End top line-->

		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-3">
					<div id="logo">
						<a href="{{url('/')}}"><img src="{{url('/')}}/src/citytour/img/korina-group-small.png" width="160" height="34" alt="City tours" data-retina="true" class="logo_normal">
						</a>
						<a href="{{url('/')}}"><img src="{{url('/')}}/src/citytour/img/korina-group-small.png" width="160" height="34" alt="City tours" data-retina="true" class="logo_sticky">
						</a>
					</div>
				</div>
				<nav class="col-md-9 col-sm-9 col-xs-9">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="{{url('/')}}/src/citytour/img/korina-group-small.png" width="160" height="34" alt="City tours" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                        <ul>
                            <li class="submenu">
                                <a href="{{url('/')}}" class="show-submenu"> <span class="icon-home-outline">Home</span> </a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('/')}}" class="show-submenu"> <span class="icon-commerical-building">Hotel</span> </a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('/')}}" class="show-submenu"> <span class="icon-plane-outline">Pesawat</span> </a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('/')}}" class="show-submenu"> <span class="icon-belowground-rail">Kereta Api</span> </a>
                            </li> 

                        </ul>
                    </div><!-- End main-menu -->

                </nav>
			</div>
		</div>
		<!-- container -->
	</header>
	<!-- End Header -->

	<script type="text/javascript">
		var url = window.location.href;
		var params = url.split('?');
		// alert(params);
		if (params[1] != null) {

		    var result = params[1].split("_");

		    // alert(result);
		    // return;
		    //define
		    var mg_code = result[3];
		    var checkIn = result[1];
		    var checkOut = result[2];
		    var dewasa = result[0];
		    var kamar = result[4];
		    var Sum_child = result[5];
		    var AgeJoin = result[6];
		    // alert(checkIn);
		    // alert(AgeJoin);
		    // return;
		    var JoinAge = AgeJoin.replace(/~/g,'_');

		    var urlGambar="{{URL::to('/')}}";
		    // alert('blm ajax');
		    // alert(JoinAge);
		    // {dewasa}/{cekin}/{cekout}/{mg_code}/{kamar}/{sumChild}/{ageJoin}
		   $.ajax({
		   		// alert();
				type: "GET",
				dataType: "JSON",
				url:"{{URL::to('api/ServicesPHP/APIS_SearchHotelByDestination')}}/"+dewasa+"/"+checkIn+"/"+checkOut+"/"+mg_code+"/"+kamar+"/"+Sum_child+"/"+AgeJoin,
				success: function (response){
					// alert('sini');
					if (!$.trim(response)){
                swal ( "Maaf tidak ada kamar tersedia" ,  "Coba Cari Di tanggal Lain" )
    /*
                alert("Maaf Tidak Tersedia Kamar di tanggal tersebut: ");
                                    */                                     }
            else{
					$(".ToRemove").remove();
                    var len = response.length;
                    // alert(len);
                    for (var i = 0; i < len; i++) {

                        var hotel_adress = response[i].hotel_address;
                        var hotelName =response[i].hotel_name;
					  	var hotelSlug =response[i].slug;
						var hotelRating =response[i].rating;
						//var hotelPicName=response[i].name;
                        var priceStartFrom=response[i].priceStartFrom;
                        var hargaCoret=response[i].hargaCoret;
                        var hotelId =response[i].id;
                        var counter =response[i].counter;
                        var book =response[i].book;

                        var ratingTag="";
                        // alert('hotelName');

                        for(var x = 0; x<hotelRating;x++){
                          ratingTag += '<i class="icon-star voted"></i>';
                      }

                      // alert(ratingTag);
                      //   return;

                        var hotelList='<div class="ToRemove">\t<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s"><div class="row">\t<div class="col-lg-4 col-md-4 col-sm-4"><div class="ribbon_3 popular"><span>Popular</span></div><div class="wishlist">\t<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a></div><div class="img_list"><a href="#"><img src="'+urlGambar+'/storage/IMG-HOTEL/'+hotelName+' hotel/'+hotelName+'_280x200.jpg" alt="Image"><div class="short_info"></div>\t</a></div>\t</div>\t<div class="clearfix visible-xs-block"></div>\t<div class="col-lg-5 col-md-5 col-sm-5"><div class="tour_list_desc">\t<div class="row"><div class="col-sm-6 icon-cart">Dipesan '+book+' kali</div><div class="col-sm-6 icon-eye-6"> Dilihat '+counter+' kali</div></div>\t<div class="rating">'+ratingTag+'\t</div>\t<h3><strong>' + hotelName + '</strong> Hotel</h3>\t<p>Lorem ipsum dolor sit '+name+', quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum...</p>\t<ul class="add_info"><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Free Wifi"><i class="icon_set_1_icon-86"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Plasma TV with cable channels"><i class="icon_set_2_icon-116"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Swimming pool"><i class="icon_set_2_icon-110"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Fitness Center"><i class="icon_set_2_icon-117"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Restaurant"><i class="icon_set_1_icon-58"></i></a></li>\t</ul></div>\t</div>\t<div class="col-lg-3 col-md-3 col-sm-3" ><div class="price_list">\t<div><sup>Rp</sup>'+priceStartFrom+'<span class="normal_price_list">Rp '+hargaCoret+'</span><small>*From/Per night</small><p><a href="'+urlGambar+'/detail/hotel/'+hotelId+'-'+hotelSlug+'.html" class="btn_1">Details</a></p>\t</div></div>\t</div></div>\t</div>\t<!--End strip --></div>';
                         $("#produkTampung").append(hotelList);
                    	}
                	}
				},
				 error: function() {
				        alert('error');
				      }
			});

									
		}
	</script>

	<section class="parallax-window" data-parallax="scroll" data-image-src="{{url('/')}}/storage/IMG DESTINATION/BANGKOK.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Found {{$data_total_hotel}} Hotels in Bangkok </h1>
				<p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>
			</div>
		</div>
	</section>
	<!-- End section -->

	<main>
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="{{url('/')}}">Home</a>
                    </li>
                    <li>List Hotel
                    </li>
				</ul>
			</div>
		</div>
		<!-- Position -->

		<div class="collapse" id="collapseMap">
			<div id="map" class="map">
			</div>
		</div>
		<!-- End Map -->

		<div class="container margin_60">

			<div class="row">
				<aside class="col-lg-3 col-md-3">
					<p>
						<a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View on map</a>
					</p>

					<div id="filters_col">
						<a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt"><i class="icon_set_1_icon-65"></i>Filters <i class="icon-plus-1 pull-right"></i></a>
						<div class="collapse" id="collapseFilters">


							<script>
                                $(document).ready(function() {
                                    function loadBar()
									{

                                        $({property: 0}).animate({property: 105}, {
                                            duration: 500,
                                            step: function() {
                                                //$("#progress").removeClass("done");
                                                var _percent = Math.round(this.property);
                                                $('#progress').css('width',  _percent+"%");
                                                if(_percent == 105) {
                                                    $("#progress").addClass("done");
                                                }
                                            },
                                        });
									}

									$('#ByPrice').on('change', function() {
										loadBar();
										val = this.value;
										var urlGambar="{{URL::to('/')}}";

										// alert(val);
										// return;
										$.ajax({
											type: "GET",
											dataType: "JSON",
											url:"{{URL::to('ajax/byPrice/')}}/"+val,
											success: function (response){
												$(".ToRemove").remove();
                                                var len = response.length;
                                                // alert(len);
                                                for (var i = 0; i < len; i++) {
                                                    var hotel_adress = response[i].hotel_address;
                                                    var hotelName =response[i].hotel_name;
												  	var hotelSlug =response[i].slug;
													var hotelRating =response[i].rating;
													//var hotelPicName=response[i].name;
                                                    var priceStartFrom=response[i].priceStartFrom;
                                                    var hargaCoret=response[i].hargaCoret;
                                                    var hotelId =response[i].id;
                                                    var counter =response[i].counter;
                                                    var book =response[i].book;
                                                    var ratingTag="";
                                                    // alert('hotelName');
                                                    for(var x = 0; x<hotelRating;x++){
                                                      ratingTag += '<i class="icon-star voted"></i>';
                                                  }

                                                    var hotelList='<div class="ToRemove">\t<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s"><div class="row">\t<div class="col-lg-4 col-md-4 col-sm-4"><div class="ribbon_3 popular"><span>Popular</span></div><div class="wishlist">\t<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a></div><div class="img_list"><a href="#"><img src="'+urlGambar+'/storage/IMG-HOTEL/'+hotelName+' hotel/'+hotelName+'_280x200.jpg" alt="Image"><div class="short_info"></div>\t</a></div>\t</div>\t<div class="clearfix visible-xs-block"></div>\t<div class="col-lg-5 col-md-5 col-sm-5"><div class="tour_list_desc">\t<div class="row"><div class="col-sm-6 icon-cart">Dipesan '+book+' kali</div><div class="col-sm-6 icon-eye-6"> Dilihat '+counter+' kali</div></div>\t<div class="rating">'+ratingTag+'\t</div>\t<h3><strong>' + hotelName + '</strong> Hotel</h3>\t<p>Lorem ipsum dolor sit '+name+', quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum...</p>\t<ul class="add_info"><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Free Wifi"><i class="icon_set_1_icon-86"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Plasma TV with cable channels"><i class="icon_set_2_icon-116"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Swimming pool"><i class="icon_set_2_icon-110"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Fitness Center"><i class="icon_set_2_icon-117"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Restaurant"><i class="icon_set_1_icon-58"></i></a></li>\t</ul></div>\t</div>\t<div class="col-lg-3 col-md-3 col-sm-3" ><div class="price_list">\t<div><sup>Rp</sup>'+priceStartFrom+'<span class="normal_price_list">Rp '+hargaCoret+'</span><small>*From/Per night</small><p><a href="'+urlGambar+'/detail/hotel/'+hotelId+'-'+hotelSlug+'.html" class="btn_1">Details</a></p>\t</div></div>\t</div></div>\t</div>\t<!--End strip --></div>';
                                                     $("#produkTampung").append(hotelList);
                                                }
											}
										});
									});

									$('#ByRate').on('change', function() {
										
										loadBar();

										val = this.value;
										var urlGambar="{{URL::to('/')}}";
										// alert(val);
										// return;
										$.ajax({
											type: "GET",
											dataType: "JSON",
											url:"{{URL::to('ajax/byRate/')}}/"+val,
											success: function (response){
												// alert('success'+val);
												$(".ToRemove").remove();
                                                var len = response.length;
                                                // alert(len);
                                                 for (var i = 0; i < len; i++) {

                                                    var hotel_adress = response[i].hotel_address;
                                                    var hotelName =response[i].hotel_name;
												  	var hotelSlug =response[i].slug;
													var hotelRating =response[i].rating;
													//var hotelPicName=response[i].name;
                                                    var priceStartFrom=response[i].priceStartFrom;
                                                    var hargaCoret=response[i].hargaCoret;
                                                    var hotelId =response[i].id;
                                                    var counter =response[i].counter;
                                                    var book =response[i].book;

                                                    var ratingTag="";

/*                                                    for(var x=1;x<=5;x++){
														alert("x=".x)
                                                    }*/
                                                  for(var x = 0; x<hotelRating;x++){
                                                      ratingTag += '<i class="icon-star voted"></i>';
                                                  }
													// alert(hotelId);
													var hotelList='<div class="ToRemove">\t<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s"><div class="row">\t<div class="col-lg-4 col-md-4 col-sm-4"><div class="ribbon_3 popular"><span>Popular</span></div><div class="wishlist">\t<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a></div><div class="img_list"><a href="#"><img src="'+urlGambar+'/storage/IMG-HOTEL/'+hotelName+' hotel/'+hotelName+'_280x200.jpg" alt="Image"><div class="short_info"></div>\t</a></div>\t</div>\t<div class="clearfix visible-xs-block"></div>\t<div class="col-lg-5 col-md-5 col-sm-5"><div class="tour_list_desc">\t<div class="row"><div class="col-sm-6 icon-cart">Dipesan '+book+' kali</div><div class="col-sm-6 icon-eye-6"> Dilihat '+counter+' kali</div></div>\t<div class="rating">'+ratingTag+'\t</div>\t<h3><strong>' + hotelName + '</strong> Hotel</h3>\t<p>Lorem ipsum dolor sit '+name+', quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum...</p>\t<ul class="add_info"><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Free Wifi"><i class="icon_set_1_icon-86"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Plasma TV with cable channels"><i class="icon_set_2_icon-116"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Swimming pool"><i class="icon_set_2_icon-110"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Fitness Center"><i class="icon_set_2_icon-117"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Restaurant"><i class="icon_set_1_icon-58"></i></a></li>\t</ul></div>\t</div>\t<div class="col-lg-3 col-md-3 col-sm-3" ><div class="price_list">\t<div><sup>Rp</sup>'+priceStartFrom+'<span class="normal_price_list">Rp '+hargaCoret+'</span><small>*From/Per night</small><p><a href="'+urlGambar+'/detail/hotel/'+hotelId+'-'+hotelSlug+'.html" class="btn_1">Details</a></p>\t</div></div>\t</div></div>\t</div>\t<!--End strip --></div>';
                                                     $("#produkTampung").append(hotelList);
												}
											}
										});
									 //  alert( this.value );
									});
					
									$(".byFacility").click(function(){
										loadBar();
										var pilih = [];
                                        var urlGambar="{{URL::to('/')}}";

                                        var q;
                                        var param ;
                                        $.each($("input[name='facility']:checked"), function(){
                                            pilih.push($(this).val());
                                        });
                                        param=pilih.join("_");
                                        // alert(param);
                                        // return;
                                        $.ajax({
                                        	type: "GET",
                                        	dataType: "JSON",
                                        	url: "{{URL::to('ajax/byFacility/')}}/"+param,
                                        	success: function (response){
                                        		$(".ToRemove").remove();
                                                var len = response.length;
                                                // alert(len);
                                                for (var i = 0; i < len; i++) {

                                                    var hotel_adress = response[i].hotel_address;
                                                    var hotelName =response[i].hotel_name;
												  	var hotelSlug =response[i].slug;
													var hotelRating =response[i].rating;
													//var hotelPicName=response[i].name;
                                                    var priceStartFrom=response[i].priceStartFrom;
                                                    var hargaCoret=response[i].hargaCoret;
                                                    var hotelId =response[i].id;
                                                    var counter =response[i].counter;
                                                    var book =response[i].book;

                                                    var ratingTag="";

/*                                                    for(var x=1;x<=5;x++){
														alert("x=".x)
                                                    }*/
                                                  for(var x = 0; x<hotelRating;x++){
                                                      ratingTag += '<i class="icon-star voted"></i>';
                                                  }
													// alert(hotelId);
													var hotelList='<div class="ToRemove">\t<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s"><div class="row">\t<div class="col-lg-4 col-md-4 col-sm-4"><div class="ribbon_3 popular"><span>Popular</span></div><div class="wishlist">\t<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a></div><div class="img_list"><a href="#"><img src="'+urlGambar+'/storage/IMG-HOTEL/'+hotelName+' hotel/'+hotelName+'_280x200.jpg" alt="Image"><div class="short_info"></div>\t</a></div>\t</div>\t<div class="clearfix visible-xs-block"></div>\t<div class="col-lg-5 col-md-5 col-sm-5"><div class="tour_list_desc">\t<div class="row"><div class="col-sm-6 icon-cart">Dipesan '+book+' kali</div><div class="col-sm-6 icon-eye-6"> Dilihat '+counter+' kali</div></div>\t<div class="rating">'+ratingTag+'\t</div>\t<h3><strong>' + hotelName + '</strong> Hotel</h3>\t<p>Lorem ipsum dolor sit '+name+', quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum...</p>\t<ul class="add_info"><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Free Wifi"><i class="icon_set_1_icon-86"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Plasma TV with cable channels"><i class="icon_set_2_icon-116"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Swimming pool"><i class="icon_set_2_icon-110"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Fitness Center"><i class="icon_set_2_icon-117"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Restaurant"><i class="icon_set_1_icon-58"></i></a></li>\t</ul></div>\t</div>\t<div class="col-lg-3 col-md-3 col-sm-3" ><div class="price_list">\t<div><sup>Rp</sup>'+priceStartFrom+'<span class="normal_price_list">Rp '+hargaCoret+'</span><small>*From/Per night</small><p><a href="'+urlGambar+'/detail/hotel/'+hotelId+'-'+hotelSlug+'.html" class="btn_1">Details</a></p>\t</div></div>\t</div></div>\t</div>\t<!--End strip --></div>';
                                                     $("#produkTampung").append(hotelList);
												}
                                        	}
                                        });
									});
										
										
									//var jqxhr = {abort: function () {}};
									var xhr = null;
									function shadowPage()
									{
										
                                          $("#loadTampung").show();
                                          $('#loadTampung').fadeIn(1000);
                                          $('#loadTampung').fadeOut(1000);
                                          //$('#loadTampung').fadeIn();
									}

                                    $(".cekbok").click(function(){

                                    	
                                                     	 
        								//alert(xhr);


                                        //loadBar();
                                       $(".ToRemove").remove();
                                       
                                        if( xhr != null ) {
                                        	
                							xhr.abort();
                							$('#loadTampung').hide();
                                        	//$("#produkTampung").hide();
             								xhr = null;

        								}
                            //$(".toRemove").remove();
                                        var pilih = [];
                                        var urlGambar="{{URL::to('/')}}";
                                        //alert(urlGambar);

                                    //<img src="'+urlGambar+'storage/IMG HOTEL/'+name+'/'+name+'.jpg" alt="Image">
                                        var q;
                                        var param ;
                                        $.each($("input[name='rating']:checked"), function(){
                                            pilih.push($(this).val());
                                        });
                                        param=pilih.join("_");
                                       /* if(pilih = [])
                                        {
                                        	alert("kosong");
                                        }*/
                                     //   alert("Anda memilih range bintang: " +param );
                                	    xhr=$.ajax({
                                            type: "GET",
                                            dataType: 'JSON',
                                            url: "{{URL::to('ajax/sort/')}}/"+param,
                                              beforeSend: function () {                                                
                                                     	 // $('#loadTampung').fadeIn(1000);
                                                          /*$("#loadTampung").show();
                                                           $('#loadTampung').fadeIn(1000);
                                                     	  $('#loadTampung').fadeOut(1000);
                                                     	  $('#loadTampung').fadeIn();*/

        													 
        													shadowPage();
        														
        													
                                                     	  
                                                          
                                                        },
                                            success: function (response) {
                                            	$('#loadTampung').hide();

                                                var len = response.length;
                                                //alert(len);

                                              for (var i = 0; i < len; i++) {
                                                    var hotel_adress = response[i].hotel_address;
                                                    var hotelName =response[i].hotel_name;
												  	var hotelSlug =response[i].slug;
													var hotelRating =response[i].rating;
													//var hotelPicName=response[i].name;
                                                    var priceStartFrom=response[i].priceStartFrom;
                                                    var hargaCoret=response[i].hargaCoret;
                                                    var hotelId =response[i].id;
                                                    var counter =response[i].counter;
                                                    var book =response[i].book;

													var ratingTag="";

/*                                                    for(var x=1;x<=5;x++){
														alert("x=".x)
                                                    }*/
                                                  for(var x = 0; x<hotelRating;x++){
                                                      ratingTag += '<i class="icon-star voted"></i>';
                                                  }
                                                    //alert(appendString);

													//alert(hotelRating);
                                                   /* var hotelName =response[i].
                                                    var hotelName =response[i].
                                                    var hotelName =response[i].
                                                      var hotelName =response[i].
                                                      var hotelName =response[i].
                                                      var hotelName =response[i].
                                                      var hotelName =response[i].
                                                      var hotelName =response[i].*/
                                                    //alert(pesan);
												  //url(\'/\')}}/storage/IMG HOTEL/$hotel->name}}/$hotel->name}}.jpg


                                                  //'+urlGambar+'/storage/IMG HOTEL/'+name+'/'+name+'
                                                  var data = 'Room Name : ' + hotel_adress + ' Harga :' + hotelName + 'bed type'+ hotelSlug +'id room'+ hotelRating +'<br/><hr/>';
                                                  //var roomAvail='<div class="toRemove"><div class="row">\t<div class="col-md-12" style=""><div class="row"><div class="col-md-7">\t<h2>ROOM '+room_name+' </h2>\t<h4>'+ bed_type +'</h4>\t<h5>'+ bf_type +'</h5></div>\t<div class="col-md-5"><div id="price_single_main" class="hotel">\t<span style="font-size:41px"><sup>* '+periodStay+' Malam '+tipe_kamar+' Kamar Rp</sup>' + rupiah +  ' </span></div> <form method="POST" action="{{URL::to('hotel/booking')}}">{{ csrf_field() }}<input type="hidden" name="dataBooking" value="[1]'+check_in+'[1][2]'+check_out+'[2][3]'+tipe_kamar+' '+room_name+'[3][4]'+periodStay+'[4][5]'+adults+'[5][6]'+convert_price+'[6][7]'+hotelName+'[7]"/> \t<input class="btn_full" type="submit" value="Book Now"/></form>\t</div></div></div></div>\t<p>Lorem ipsum dolor sit amet, at omnes deseruisse pri. Quo aeterno legimus insolens ad. Sit cu detraxit constituam, an mel iudico constituto efficiendi.\t</p>\t<div class="row"><div class="col-md-6 col-sm-6">\t<ul class="list_icons"><li><i class="icon_set_1_icon-86"></i> Free wifi</li><li><i class="icon_set_2_icon-116"></i> Plasma Tv</li><li><i class="icon_set_2_icon-106"></i> Safety box</li>\t</ul></div><div class="col-md-6 col-sm-6">\t<ul class="list_ok"><li>Lorem ipsum dolor sit amet</li><li>No scripta electram necessitatibus sit</li><li>Tipe kamarnya adalah'+tipe_kamar+'</li>\t</ul></div>\t</div><hr/></div>';
												  //var hotelList='<div class="ToRemove">\t<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s"><div class="row">\t<div class="col-lg-4 col-md-4 col-sm-4"><div class="ribbon_3 popular"><span>Popular</span></div><div class="wishlist">\t<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a></div><div class="img_list">{{--\t<a href="single_hotel.html"><img src="{{url(\'/\')}}/src/citytour/img/hotel_1.jpg" alt="Image">--}}<a href="single_hotel.html"><img src="'+urlGambar+'/storage/IMG HOTEL/'+hotelPicName+'/'+hotelPicName+'.jpg" alt="Image"><div class="short_info"></div>\t</a></div>\t</div>\t<div class="clearfix visible-xs-block"></div>\t<div class="col-lg-5 col-md-5 col-sm-5"><div class="tour_list_desc">\t<div class="score">Superb<span>9.0</span>\t</div>\t<div class="rating">'+ratingTag+'\t</div>\t<h3><strong>' + hotelName + '</strong> Hotel</h3>\t<p>Lorem ipsum dolor sit '+name+', quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum...</p>\t<ul class="add_info"><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Free Wifi"><i class="icon_set_1_icon-86"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Plasma TV with cable channels"><i class="icon_set_2_icon-116"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Swimming pool"><i class="icon_set_2_icon-110"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Fitness Center"><i class="icon_set_2_icon-117"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Restaurant"><i class="icon_set_1_icon-58"></i></a></li>\t</ul></div>\t</div>\t<div class="col-lg-3 col-md-3 col-sm-3" ><div class="price_list">\t<div><sup>Rp</sup>'+priceStartFrom+'<span class="normal_price_list">Rp '+hargaCoret+'</span><small>*From/Per night</small><p><a href="'+urlGambar+'/detail/hotel/'+hotelId+'-'+hotelSlug+'.html" class="btn_1">Details</a></p>\t</div></div>\t</div></div>\t</div>\t<!--End strip --></div>';

                                                  var hotelList='<div class="ToRemove">\t<div class="strip_all_tour_list" data-wow-delay="0.1s"><div class="row">\t<div class="col-lg-4 col-md-4 col-sm-4"><div class="ribbon_3 popular"><span>Popular</span></div><div class="wishlist">\t<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a></div><div class="img_list"><a href="#"><img src="'+urlGambar+'/storage/IMG-HOTEL/'+hotelName+' hotel/'+hotelName+'_280x200.jpg" alt="Image"><div class="short_info"></div>\t</a></div>\t</div>\t<div class="clearfix visible-xs-block"></div>\t<div class="col-lg-5 col-md-5 col-sm-5"><div class="tour_list_desc">\t<div class="row"><div class="col-sm-6 icon-cart">Dipesan '+book+' kali</div><div class="col-sm-6 icon-eye-6"> Dilihat '+counter+' kali</div></div>\t<div class="rating">'+ratingTag+'\t</div>\t<h3><strong>' + hotelName + '</strong> Hotel</h3>\t<p>Lorem ipsum dolor sit '+name+', quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum...</p>\t<ul class="add_info"><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Free Wifi"><i class="icon_set_1_icon-86"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Plasma TV with cable channels"><i class="icon_set_2_icon-116"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Swimming pool"><i class="icon_set_2_icon-110"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Fitness Center"><i class="icon_set_2_icon-117"></i></a></li><li>\t<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Restaurant"><i class="icon_set_1_icon-58"></i></a></li>\t</ul></div>\t</div>\t<div class="col-lg-3 col-md-3 col-sm-3" ><div class="price_list">\t<div><sup>Rp</sup>'+priceStartFrom+'<span class="normal_price_list">Rp '+hargaCoret+'</span><small>*From/Per night</small><p><a href="'+urlGambar+'/detail/hotel/'+hotelId+'-'+hotelSlug+'.html" class="btn_1">Details</a></p>\t</div></div>\t</div></div>\t</div>\t<!--End strip --></div>';
                                                     $("#produkTampung").append(hotelList);
                                                }
                                                // alert("it works");

                                            }
                                        });



                                    });
                                });


							</script>

					{{--		<div class="filter_type">
								<h6>Price</h6>
								<input type="text" id="range" name="range" class="cekbox" value="">
							</div>--}}

                           {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}


                            <div class="filter_type">
								<h6  >Star Categorys</h6>
								<ul>
									<li>

										<label>

											<input type="checkbox" class="cekbok" name="rating" value="5" ><span class="rating">
						<i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i>
						</span>({{$hotel_star_5}})</label>
									</li>
									<li>
										<label>
											<input type="checkbox" class="cekbok" name="rating" value="4" ><span class="rating">
						<i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81"></i>
						</span>({{$hotel_star_4}})</label>
									</li>
									<li>
										<label>
											<input type="checkbox" class="cekbok" name="rating" value="3"><span class="rating">
						<i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81"></i><i class="icon_set_1_icon-81"></i>
						</span>({{$hotel_star_3}})</label>
									</li>
									<li>
										<label>
											<input type="checkbox" class="cekbok" name="rating" value="2"><span class="rating">
						<i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81"></i><i class="icon_set_1_icon-81"></i><i class="icon_set_1_icon-81"></i>
						</span>({{$hotel_star_2}})</label>
									</li>
									<li>
										<label>
											<input type="checkbox" class="cekbok" name="rating" value="1"><span class="rating">
						<i class="icon_set_1_icon-81 voted"></i><i class="icon_set_1_icon-81"></i><i class="icon_set_1_icon-81"></i><i class="icon_set_1_icon-81"></i><i class="icon_set_1_icon-81"></i>
						</span>({{$hotel_star_1}})</label>
									</li>
								</ul>
							</div>
							<div class="filter_type">
								<h6>Facility</h6>
								<ul>
                                    <?php

                                    $result=array();
                                    foreach($data_list_facility as $row)
                                    {
                                        ?>
										<li>
											<label>
												<input type="checkbox" class="byFacility"  name="facility" value="{{$row->id}}"> {{$row->facility_name}} &nbsp;({{ substr_count($total_facility,$row->facility_name)}})</label>
										</li>{{--
                                        echo $row->facility_name;
                                        echo $jumlahFasilitas=substr_count($data,$row->facility_name);--}}
										<?php

                                    }

                                    ?>{{--
									@foreach ($data_list_facility as $facility)
									<li>
										<label>
											<input type="checkbox">{{$facility->facility_name}}</label>
									</li>
									@endforeach
--}}								</ul>
							</div>

						</div>
						<!--End collapse -->
					</div>
					<!--End filters col-->
					<div class="box_style_2">
						<i class="icon_set_1_icon-57"></i>
						<h4>Need <span>Help?</span></h4>
						<a href="tel://004542344599" class="phone">+45 423 445 99</a>
						<small>Monday to Friday 9.00am - 7.30pm</small>
					</div>
				</aside>
				<!--End aside -->

				<div class="col-lg-9 col-md-9">


					<div id="tools">
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-6">
								<div class="styled-select-filters">
									<select name="sort_price" id="ByPrice" >
										<option value=""  selected>Sort by price</option>
										<option value="lower"  >Lowest price</option>
										<option value="higher">Highest price</option>
									</select>
								</div>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-6">
								<div class="styled-select-filters">
									<form>
									<select name="sort_rating" id="ByRate">
										<option value="" selected>Sort by ranking</option>
										<option value="asc">Lowest ranking</option>
										<option value="desc">Highest ranking</option>
									</select>
									</form>
								</div>
							</div>

							<div class="col-md-6 col-sm-6 hidden-xs text-right">
								<a href="#" class="bt_filters"><i class="icon-th"></i></a> <a href="#" class="bt_filters"><i class=" icon-list"></i></a>
							</div>
						</div>
					</div>


					<!--/tools -->

{{-- <div id="txtHint"><b>Person info will be listed here.</b></div> --}}
					<!--  list hotel area  -->
					<div id="loadTampung" style="display:none">
						<img src="{{URL::to('/')}}/storage/IMG-HOTEL/loader.jpg" alt="Image">
						<img src="{{URL::to('/')}}/storage/IMG-HOTEL/loader.jpg" alt="Image">
						<img src="{{URL::to('/')}}/storage/IMG-HOTEL/loader.jpg" alt="Image">
						<img src="{{URL::to('/')}}/storage/IMG-HOTEL/loader.jpg" alt="Image">
						 
					</div>

					<div id="produkTampung">

					</div>



					<div class="ToRemove">
					<?php $idr = app('App\Http\Controllers\For_testing')->convertPrice(); ?>
					<?php $no = 0;  ?>
					@foreach ($data_list_hotel as $hotel)

					<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="ribbon_3 popular"><span>Popular</span>
								</div>
								<div class="wishlist">
									<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
								</div>
								<div class="img_list">
{{--									<a href="single_hotel.html"><img src="{{url('/')}}/src/citytour/img/hotel_1.jpg" alt="Image">--}}
                                    <a href="#"><img src="{{url('/')}}/storage/img-hotel/{{$hotel->hotel_name}} hotel/{{$hotel->hotel_name}}_280x200.jpg" alt="Image">

										<div class="short_info"></div>
									</a>
								</div>
							</div>
							<div class="clearfix visible-xs-block"></div>
							<div class="col-lg-5 col-md-5 col-sm-5">
								<div class="tour_list_desc">
									<div class="row">
										<div class="col-sm-6 icon-cart">Dipesan {{$sumBook[$no]}} kali </div>
										<div class="col-sm-6 icon-eye-6">Dilihat {{$counter[$no]->view_count}} kali</div>
									</div>
									

									<div class="rating">
                                        <?php for($i=1;$i<=$hotel->rating;$i++)
                                        {?>
											<i class="icon-star voted"></i>
                                        <?php
                                        }
                                        ?>


									</div>
									<h3><strong>{{$hotel->hotel_name}}</strong> Hotel</h3>
									<p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum...</p>
									<ul class="add_info">
										<li>
											<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Free Wifi"><i class="icon_set_1_icon-86"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Plasma TV with cable channels"><i class="icon_set_2_icon-116"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Swimming pool"><i class="icon_set_2_icon-110"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Fitness Center"><i class="icon_set_2_icon-117"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);" class="tooltip-1" data-placement="top" title="Restaurant"><i class="icon_set_1_icon-58"></i></a>
										</li>

									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3" >
                                <?php
                               
                                $getprice=new \App\Helpers\PriceHelp();
                                $cheapestPrice=$getprice->cheapestPrice($hotel->id);
                                // echo "ini son".$cheapestPrice;
                                
                                $price = (double)filter_var($idr, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                                $ini = $price*$cheapestPrice;
     
                                
                                // echo "<br>iniyy ".$ini;
      
                                $markup = $ini*(100+15)/100;
                                $coret = $ini*(100+30)/100;
                                // echo 'markup '.number_format($markup);
                                // echo 'real '.$cheapestPrice;
                                ?>
								<div class="price_list">
									<div><sup>Rp</sup>{{ number_format($markup) }} <span class="normal_price_list">Rp {{ number_format($coret) }}</span><small>*From/Per night</small>
										<p><a href="<?php echo url(''); ?>/detail/hotel/{{$hotel->id}}-{{$hotel->slug}}.html" class="btn_1">Details</a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--End strip -->
					<?php  $no++; ?>
					@endforeach
					</div>












					<!--End strip -->







					<!--  /list hotel area  -->



					<hr>

					{{--<div class="text-center">
						<ul class="pagination">
							<li><a href="#">Prev</a>
							</li>
							<li class="active"><a href="#">1</a>
							</li>
							<li><a href="#">2</a>
							</li>
							<li><a href="#">3</a>
							</li>
							<li><a href="#">4</a>
							</li>
							<li><a href="#">5</a>
							</li>
							<li><a href="#">Next</a>
							</li>
						</ul>
					</div>--}}
					<!-- end pagination-->

				</div>
				<!-- End col lg-9 -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->
	</main>
	<!-- End main -->

	<footer class="revealed">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-3">
                    <h3>Need help?</h3>
                    <a href="tel://004542344599" id="phone">+45 423 445 99</a>
                    <a href="mailto:help@citytours.com" id="email_footer">help@citytours.com</a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <h3>About</h3>



					<ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Login</a></li>
                        <li><a href="#">Register</a></li>
                         <li><a href="#">Terms and condition</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-3">
                    <h3>Discover</h3>
                    <ul>
                        <li><a href="#">Community blog</a></li>
                        <li><a href="#">Tour guide</a></li>
                        <li><a href="#">Wishlist</a></li>
                         <li><a href="#">Gallery</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-3">
                    <h3>Settings</h3>
                    <div class="styled-select">
                        <select class="form-control" name="lang" id="lang">
                            <option value="English" selected>English</option>
                            <option value="French">French</option>
                            <option value="Spanish">Spanish</option>
                            <option value="Russian">Russian</option>
                        </select>
                    </div>
                    <div class="styled-select">
                        <select class="form-control" name="currency" id="currency">
                            <option value="USD" selected>USD</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                            <option value="RUB">RUB</option>
                        </select>
                    </div>
                </div>
            </div><!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <ul>
                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-google"></i></a></li>
                            <li><a href="#"><i class="icon-instagram"></i></a></li>
                            <li><a href="#"><i class="icon-pinterest"></i></a></li>
                            <li><a href="#"><i class="icon-vimeo"></i></a></li>
                            <li><a href="#"><i class="icon-youtube-play"></i></a></li>
                            <li><a href="#"><i class="icon-linkedin"></i></a></li>
                        </ul>
                        <p>© Citytours 2015</p>
                    </div>
                </div>
            </div><!-- End row -->
        </div><!-- End container -->
    </footer><!-- End footer -->

	<div id="toTop"></div><!-- Back to top button -->

	<!-- Search Menu -->
	<div class="search-overlay-menu">
		<span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
		<form role="search" id="searchform" method="get">
			<input value="" name="q" type="search" placeholder="Search..." />

			<button type="submit"><i class="icon_set_1_icon-78"></i>
			</button>
		</form>
	</div><!-- End Search Menu -->

	<!-- Common scripts -->

	<script src="{{url('/')}}/src/citytour/js/jquery-2.2.4.min.js"></script>
	<script src="{{url('/')}}/src/citytour/js/common_scripts_min.js"></script>
	<script src="{{url('/')}}/src/citytour/js/functions.js"></script>

	<!-- Map -->
	<script src="http://maps.googleapis.com/maps/api/js"></script>
	<script src="{{url('/')}}/src/citytour/js/map_hotels.js"></script>
	<script src="{{url('/')}}/src/citytour/js/infobox.js"></script>

</body>

</html>