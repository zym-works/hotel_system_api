@extends('citytour.main')
@section('content')
<main>
    <section id="hero" class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div id="login">
                        <div class="text-center"><img src="{{url('/')}}/src/citytour/img/logo_sticky.png" alt="Image" data-retina="true" ></div>
                        <hr>
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                            @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif 
                        @endforeach
                        <form method="post" action="{{url('agent/member/create')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id_agent" value="{{ $session_user['id_agent'] }}">
                            <div class="form-group {{ $errors->has('fullname') ? ' has-error' : '' }}">
                                <label>Full Name</label>
                                <input type="text" class=" form-control" name="fullname"  placeholder="Full Name">
                                <div class="col-md-8">
                                        {!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                <label>Username</label>
                                <input type="text" class=" form-control" name="username" placeholder="Username">
                                <div class="col-md-8">
                                        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <input type="password" class=" form-control" name="password" id="password1" placeholder="Password">
                                <div class="col-md-8">
                                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Confirm password</label>
                                <input type="password" class=" form-control" id="password2" placeholder="Confirm password">
                            </div>
                            <div id="pass-info" class="clearfix"></div>
                            <button class="btn_full">Create an account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End main -->
@endsection