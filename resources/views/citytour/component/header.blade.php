 <header>
        <div id="top_line">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6"><i class="icon-phone"></i><strong>0045 043204434</strong></div>
                    
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <ul id="top_links">
                            <li>
                                <div class="dropdown dropdown-access" align="right">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="access_link">
                                        <?php 

                                            if (!empty($session_user['username'])){  
                                                echo "Welcome ".$session_user['username'];
                                            }else{
                                               echo "Sign in"; 
                                            }
                                        ?> 
                                    </a>
                                    <?php
                                    if (isset($session_user['username'])){ ?>
                                     <div class="dropdown-menu">
                                        <div class="login-or">
                                            <hr class="hr-or">
                                            <span class="span-or">Hello</span><br>
                                        
                                        </div>
                                        <div class="dropdown-menu">
                                            <i class="ti-user m-r-10 text-custom"></i>helo
                                        </div>
                                        <div class="row">
                                        <div align="left" class="col-md-6">
                                            <a href="{{url('/')}}/agent" class="btn btn-primary" role="button"><i class="ti-power-off m-r-10 text-default"></i> Dashboard</a>
                                        </div>
                                        <div align="right" class="col-md-6">
                                            <a href="{{url('/')}}/agent/logout" class="btn btn-danger" role="button"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a>
                                        </div>
                                    </div>
                                    </div>
                                    <?php
                                        } else {
                                    ?>
                                    <div class="dropdown-menu">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_facebook">
                                                    <i class="icon-facebook"></i>Facebook </a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_paypal">
                                                    <i class="icon-paypal"></i>Paypal </a>
                                            </div>
                                        </div>
                                        <div class="login-or">
                                            <hr class="hr-or">
                                            <span class="span-or">or</span>
                                        </div>
                                        <form action="{{route('agent.signin')}}" method="post" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="inputUsernameEmail" id="inputUsernameEmail" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Password">
                                        </div>
                                        <a id="forgot_pw" href="#">Forgot password?</a>
                                        <input type="submit" name="Sign_in" value="Sign in" id="Sign_in" class="button_drop">
                                        <a href="{{url('/register')}}"><input type="button" name="Sign_up" value="Sign up" id="Sign_up" class="button_drop"></a>
                                        </form>
                                    </div>
                                    <?php } ?>

                                </div><!-- End Dropdown access -->
                            </li>
                           
                        </ul>
                    </div>
                </div><!-- End row -->
            </div><!-- End container-->
        </div><!-- End top line-->
        

        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div id="logo_home">
                    	<h1><a href="{{url('/')}}" title="City tours travel template ">City Tours travel template</a></h1>
                    </div>
                </div>
                <nav class="col-md-9 col-sm-9 col-xs-9">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="{{url('/')}}/src/citytour/img/korina-group-small.png" width="160" height="34" alt="City tours" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                        <ul>
                            <li class="submenu">
                                <a href="{{url('/')}}" class="show-submenu"> <span class="icon-home-outline">Home</span> </a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('/')}}" class="show-submenu"> <span class="icon-commerical-building">Hotel</span> </a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('/')}}" class="show-submenu"> <span class="icon-plane-outline">Pesawat</span> </a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('/')}}" class="show-submenu"> <span class="icon-belowground-rail">Kereta Api</span> </a>
                            </li>
                        </ul>
                    </div><!-- End main-menu -->
                    
                </nav>
            </div>
        </div><!-- container -->
    </header><!-- End Header -->
