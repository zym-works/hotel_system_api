@extends('citytour.main')
@section('content')
 <section id="hero_2">
        <div class="intro_title animated fadeInDown">
            <h1>Place your order </h1>
            <div class="bs-wizard">

                <div class="col-xs-4 bs-wizard-step complete">
                    <div class="text-center bs-wizard-stepnum">Your cart</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="cart_hotel.html" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step complete">
                    <div class="text-center bs-wizard-stepnum">Your details</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum">Finish!</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="confirmation_hotel.html" class="bs-wizard-dot"></a>
                </div>

            </div>
            <!-- End bs-wizard -->
        </div>
        <!-- End intro-title -->
    </section>
    <!-- End Section hero_2 -->
    
    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="{{url('/')}}">Home</a>
                    </li>
                    <li><a href="{{url('/list/hotel/bangkok')}}">List Hotel</a>
                    </li>
                    <li><a href="{{url('/detail/hotel').'/'.$summary->id.'-'.str_replace(' ', '-', $_SESSION['dataOrder'][6]).'.html'}}">{{$_SESSION['dataOrder'][6]}}</a>
                    </li>
                    <li>Payment Finish</li>
                </ul>
            </div>
        </div>
        <!-- End position -->

        <div class="container margin_60">
            <div class="row">
                <div class="col-md-8 add_bottom_15">
                    <div class="form_title">
                        <h3><strong>Info</strong>Your Booking succesed !!</h3>
                        <p>
                            Mussum ipsum cacilds, vidis litro abertis.
                         
                        </p>
                    </div>
                    <div class="step">

                    </div>
                 
                </div>


                <aside class="col-md-4">
                    <div class="box_style_1">
                        <h3 class="inner">- Summary -</h3>
                        
                        <table class="table table_summary">
                            <tbody>
                            <tr>
                                <td>
                                    Reservation No
                                </td>
                                <td class="text-right">
                                    <b>{{ $summary->reservation_number }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Hotel
                                </td>
                                <td class="text-right">
                                   {{ $summary->hotel_name }}
                                </td>
                            </tr>


                                <tr>
                                    <td>
                                        Check in
                                    </td>
                                    <td class="text-right">
                                      {{ $summary->check_in }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Check out
                                    </td>
                                    <td class="text-right">
                                        {{ $summary->check_out }}
                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Rooms
                                    </td>
                                    <td class="text-right">
                                        {{ $summary->room_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nights
                                    </td>
                                    <td class="text-right">
                                        {{ $summary->night }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Adults
                                    </td>
                                    <td class="text-right">
                                        {{ $summary->adult_num }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Childs
                                    </td>
                                    <td class="text-right">
                                        {{ $summary->child_num }}
                                    </td>
                                </tr>

                            <tr class="total" style="color:green" >
                                <td>
                                    Saldo 
                                </td>
                                <td class="text-right" >
                                    <sup><small>RP</small></sup>
                                    {{ number_format($balance->last_balance) }}
                                </td>
                            </tr>

                                <tr class="total">
                                    <td>
                                        Total pay
                                    </td>
                                    <td class="text-right">
                                        <sup><small>RP</small></sup>
                                    {{ number_format($summary->total_price_idr) }}
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        
                         {{--<a class="btn_full" href="{{URL::to('hotel/pay')}}">Book now</a>--}}

                       {{-- <a class="btn_full" href="{{URL::to('hotel/post-pay')}}">Book now</a>--}}
                        {{-- <input class="btn_full" type="submit" value="Book Now"/> --}}
                        <a href="{{url('/agent')}}" class="btn_full">Agent Area</a>
                    </div>
                    <div class="box_style_4">
                        <i class="icon_set_1_icon-57"></i>
                        <h4>Need <span>Help?</span></h4>
                        <a href="tel://004542344599" class="phone">+45 423 445 99</a>
                        <small>Monday to Friday 9.00am - 7.30pm</small>
                    </div>
                </aside>

            </div>
            <!--End row -->
        </div>
        <!--End container -->
    </main>
    <!-- End main -->
@endsection