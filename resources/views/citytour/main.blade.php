<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Citytours - Premium site template for city tours agencies, transfers and tickets.">
    <meta name="author" content="Ansonika">
    <title>CITY TOURS - City tours and travel site template by Ansonika</title>
    
    <!-- Favicons-->
    <link rel="shortcut icon" href="{{url('/')}}/src/citytour/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{url('/')}}/src/citytour/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{url('/')}}/src/citytour/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{url('/')}}/src/citytour/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{url('/')}}/src/citytour/img/apple-touch-icon-144x144-precomposed.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<!-- Google web fonts -->
     <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i" rel="stylesheet">
    <!-- CSS -->

    <!-- SPECIFIC CSS -->
    <link href="{{url('/')}}/src/citytour/css/admin.css" rel="stylesheet">
    <link href="{{url('/')}}/src/citytour/css/jquery.switch.css" rel="stylesheet">
    <!-- CSS -->
    <link href="{{url('/')}}/src/citytour/css/base.css" rel="stylesheet">
    
    <!-- SPECIFIC CSS -->
    <link href="{{url('/')}}/src/citytour/css/skins/square/grey.css" rel="stylesheet">
  {{--  <link href="{{url('/')}}/src/citytour/css/date_time_picker.css" rel="stylesheet">--}}
    <link href="{{url('/')}}/src/citytour/css/hotel-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
    <?php
    if(!empty($css))
    echo $css;
    ?>
        
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
        
</head>
<body>
    <div id="progress" class="waiting">
    <dt></dt>
    <dd></dd>
</div>
<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->

    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

     <!-- Header================================================== -->
   @include('citytour.component.header')
{{--     @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        </p>
        @endif 
    @endforeach --}}
	@yield('content')
	
    @include('citytour.component.footer')

	<div id="toTop"></div><!-- Back to top button -->
	
	<!-- Search Menu -->
	<div class="search-overlay-menu">
		<span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
		<form role="search" id="searchform" method="get">
			<input value="" name="q" type="search" placeholder="Search..." />
			<button type="submit"><i class="icon_set_1_icon-78"></i>
			</button>
		</form>
	</div><!-- End Search Menu -->

 <!-- Common scripts -->
<script src="{{url('/')}}/src/citytour/js/jquery-2.2.4.min.js"></script>
<script src="{{url('/')}}/src/citytour/js/common_scripts_min.js"></script>
<script src="{{url('/')}}/src/citytour/js/functions.js"></script>

 <!-- Specific scripts -->
<script src="{{url('/')}}/src/citytour/js/icheck.js"></script>


  <script src="{{url('/')}}/src/citytour/js/jquery.ddslick.js"></script>
   <script>
   $("select.ddslick").each(function(){
            $(this).ddslick({
                showSelectedHTML: true 
            });
        });
        </script>
    <!-- Specific scripts -->
<script src="{{url('/')}}/src/citytour/js/tabs.js"></script>
<script>
    new CBPFWTabs(document.getElementById('tabs'));
</script>
<script>
    $('.wishlist_close_admin').on('click', function (c) {
        $(this).parent().parent().parent().fadeOut('slow', function (c) {});
    });
</script>

        <?php
            if(!empty($js))
            echo $js;
        ?>
        
  </body>
</html>