<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class ReservationDetailModel extends Model
{
    protected $table = 'sales_detail';
	public $timestamps = false;
	
}
