<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class RoomBedModel extends Model
{
    protected $table = 'room_bed';
	public $timestamps = false;
	
}
