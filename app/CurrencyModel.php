<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CurrencyModel extends Model
{
    protected $table = 'currency';
	public $timestamps = false;
	
	public static function getCur(){
		$menu = DB::table('currency')
				->select('currency.*')
                ->get();
		return $menu;
	}
	
	public static function getCurrencyName($id){
		$menu = DB::table('currency')
                ->where('id', '=', $id)
				->select('currency.*')
                ->first();
		return $menu;
	}
	
}
