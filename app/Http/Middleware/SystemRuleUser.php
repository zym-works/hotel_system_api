<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Support\Facades\Route;
class SystemRuleUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$login_user=session('login_user');
        if(!empty($login_user)){     
            return $next($request);
        }else{
            return redirect('404');
        }

    }
}

        