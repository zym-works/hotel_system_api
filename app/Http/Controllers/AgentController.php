<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use DB;
use Session;
use App\AgentModel;
use App\AgentDetailModel;
use App\AgentBalanceModel;
use App\Http\Requests;
use Illuminate\Support\Str;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
	}
	
	
    public function index()
    {
        session_start();
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>		
			<script>
			$(function() {
					var table = $(\"#agent\").DataTable({
						processing: true,
						serverSide: true,
						idSrc: \"id_agent\",
						ajax: \"".$url."/agent/data\",
						columns: [
							{ data: 'agent_name'},
							{ data: 'username'},
							{ data: 'agent_email'},
							{ data: 'agent_address'},
							{ data: 'agent_telephone'},
							{ data: 'agent_city'},
							{ data: 'agent_country'},
							{ data: 'last_balance'},
							{ data: 'action', 'searchable': false, 'orderable':false }
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="Agent";
		return view('backend.agent.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
        session_start();
	   if($request->ajax()){
		$url=url('/');
		//$agent = AgentModel::select(['*'])->get();
		$agent = DB::table('agent')
					->join('agent_detail', 'agent_detail.agent_id', '=', 'agent.id_agent')
					->select('agent.*','agent_detail.*')
					->get();
         // $member=DB::table('agent_member')
         //        ->select('*')
         //        ->where('id_agent',$id)
         //        ->count();
		return Datatables::of($agent)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($agent) { return
				'<a href="'.url('/').'/backend/agent/edit/'.$agent->id_agent.'" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/backend/agent/delete/'.$agent->id_agent.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				 <a href="'.url('/').'/backend/agent/detail/'.$agent->id_agent.'" class="btn btn-sm btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
				';})
				->editColumn('last_balance', function ($agent) {
                    return 'Rp.'.number_format($agent->last_balance, 0);
                     })
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        session_start();
        $url=url('/');
		$js=" ";
		$css=" ";

		$page['title']="Agent Add";
		return view('backend.agent.formcreate')->with('js',$js)->with('css',$css)->with('page',$page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        session_start();
    	$this->validate($request, [
            'agent_name'           => 'required',
            'username'           => 'required|unique:agent,username',
            'email'              => 'required|email|unique:agent_detail,agent_email',
            'telephone'           => 'required',
            'address'           => 'required',
            'country'           => 'required',
            'city'           => 'required',
            'password'           => 'required'
        ]);


        $cruds = new AgentModel();
        $agent_detail = new AgentDetailModel();

		$cruds->agent_name = $request->agent_name;
		$cruds->username = $request->username;
		$cruds->password = md5($request->password);
		$cruds->last_balance = $request->last_balance;
		if($cruds->save()){
			$last_id_agent = $cruds->id_agent;
           //create agent account number
            $list = AgentDetailModel::select('agent_account_number')
                    ->orderBy('id','desc')
                    ->first();

            $string = "KG-AG";
            $month = date('m');
            $year = date('y');
            $temp = (int) substr($list->agent_account_number,10,4);
            $temp = $temp+1; 
            $number = sprintf("%04s", $temp);
            $account_number = $string.$month.$year.'-'.$number;
            //echo $account_number;

          $agent_detail->agent_account_number   = $account_number;
          $agent_detail->agent_email            = $request->email;
          $agent_detail->agent_address          = $request->address;
          $agent_detail->agent_telephone        = $request->telephone;
          $agent_detail->agent_id               = $last_id_agent;
          $agent_detail->agent_date_register    = date('Y-m-d H:i:s');
          $agent_detail->agent_country          = $request->country;
          $agent_detail->agent_city             = $request->city;
          $agent_detail->save();
			return redirect()->route('agent.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('agent.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        session_start();
        $url=url('/');
		$js=" ";
		$css=" ";
		$page['title']="Agent Edit";
		// $option=self::$option;
		// $benua = ContinentModel::pluck('continent_name_en','id');
		//$cruds = AgentModel::find($id);
		$cruds = DB::table('agent')
					->join('agent_detail', 'agent_detail.agent_id', '=', 'agent.id_agent')
					->select('agent.*','agent_detail.*')
					->where('agent.id_agent', '=', $id)
					->get();
		return view('backend.agent.formupdate')->with('js',$js)->with('css',$css)->with('page',$page)->with('agent', $cruds);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        session_start();
    	// $this->validate($request, [
	    //     'old_password'          => 'required',
	    //     'password'              => 'required|min:6',
	    //     'password_confirmation' => 'required|confirmed'
   		// ]);
		
        $id=$request->id;
        $cruds = AgentModel::find($id);
        $agent_detail = AgentDetailModel::where('agent_id',$id)->first();
        
		$cruds->agent_name = $request->agent_name;
		$cruds->username = $request->username;
		$cruds->password = md5($request->password);
		$cruds->last_balance = $request->last_balance;

		$agent_detail->agent_email	= $request->email;
		$agent_detail->agent_address	= $request->address;
		$agent_detail->agent_telephone= $request->telephone;
		$agent_detail->agent_city	= $request->city;
		$agent_detail->agent_country	= $request->country;
		if($cruds->save() && $agent_detail->save()){
			//echo $request->email.$request->address.$request->telephone.$request->city.$request->country;
			 return redirect()->route('agent.edit',['id'=>$id])->with('alert-success', 'Data Berhasil Diubah.');
		 }
		 else{
			 return redirect()->route('agent.edit',['id'=>$id])->with('alert-danger', 'Data Tidak Berhasil Diubah.');
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        session_start();
        // $cruds = areaModel::findOrFail($id);
		$cruds = AgentModel::find($id);
		$agent_detail = AgentDetailModel::where('agent_id',$id)->first();
		if($cruds->delete() && $agent_detail->delete()){
			  return redirect()->route('agent.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('agent.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }

    public function detail($id, Request $request){
        session_start();
    	$url=url('/');
    	$js = '<script type="text/javascript">
				$(function() {
				      $(\'img\').on(\'click\', function() {
				      $(\'.enlargeImageModalSource\').attr(\'src\', $(this).attr(\'src\'));
				      $(\'#enlargeImageModal\').modal(\'show\');
				    });
				});
				</script>
				<script src="'.$url.'/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
            <script src="'.$url.'/src/backend/assets/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="'.$url.'/src/backend/assets/plugins/datatables/dataTables.bootstrap.js"></script>
            <script src="'.$url.'/src/backend/assets/plugins/datatables/dataTables.buttons.min.js"></script>
            <script src="'.$url.'/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js"></script>   
            <script src="'.$url.'/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>  
            <script src="'.$url.'/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js"></script> 
            <script src="'.$url.'/src/backend/assets/plugins/datatables/dataTables.responsive.min.js"></script>   
            <script src="'.$url.'/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js"></script>    
            <script src="'.$url.'/src/backend/assets/plugins/datatables/dataTables.scroller.min.js"></script> 
            <script src="'.$url.'/src/backend/assets/plugins/datatables/dataTables.colVis.js"></script>   
            <script src="'.$url.'/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js"></script> 
            <script src="'.$url.'/src/backend/assets/pages/datatables.init.js"></script>      
            <script>
            $(function() {
                    var table = $("#topdown").DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: "'.$url.'/backend/agent/detail/show_topdown/'.$id.'",
                        columns: [
                            { data: \'DT_Row_Index\'},
                            { data: \'date_transaction\'},
                            { data: \'top_down\'},
                            { data: \'status\'},
                            { data: \'balance_saldo\'},
                            { data: \'created_by\'}
                        ],
                    });
                });
                
        </script>
         <script>
            $(function() {
                    var table = $("#topup").DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: "'.$url.'/backend/agent/detail/show_topup/'.$id.'",
                        columns: [
                            { data: \'DT_Row_Index\'},
                            { data: \'date_transaction\'},
                            { data: \'top_up\'},
                            { data: \'picture\'},
                            { data: \'note\'},
                            { data: \'status\'},
                            { data: \'action\'}
                        ],
                    });
                });
                
        </script>
        <script>
            $(function() {
                    var table = $("#member").DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: "'.$url.'/backend/agent/detail/member/'.$id.'",
                        columns: [
                            { data: \'DT_Row_Index\'},
                            { data: \'agent_account_number\'},
                            { data: \'username\'},
                            { data: \'fullname\'}
                        ],
                    });
                });
                
        </script>
        <script>
            $(function() {
                    var table = $("#refund").DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: "'.$url.'/backend/agent/detail/refund/'.$id.'",
                        columns: [
                            { data: \'DT_Row_Index\'},
                            { data: \'agent_name\'},
                            { data: \'top_up\'},
                            { data: \'created_by\'},
                            { data: \'date_confirm\'},
                            { data: \'status\' }
                        ],
                    });
                });
                
        </script>';
		$css = '<style>
				img {
				    cursor: zoom-in;
				}
				.button.disabled {
					opacity: 0.65;
					cursor: not-allowed;
				}
				</style>
				<link href="'.$url.'/src/backend/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">
            <link href="'.$url.'/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet">
            <link href="'.$url.'/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet">
            <link href="'.$url.'/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet">
            <link href="'.$url.'/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet">
            <link href="'.$url.'/src/backend/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet">
            <link href="'.$url.'/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet">
            <link href="'.$url.'/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet">';
		$page['title']="Agent Detail";
        $details = AgentBalanceModel::where('id_agent',$id)->where('top_down',null)->paginate(5);
        $topdown = AgentBalanceModel::where('id_agent',$id)->where('top_up',null)->paginate(5);
        $agent = DB::table('agent')
					->join('agent_detail', 'agent_detail.agent_id', '=', 'agent.id_agent')
					->select('agent.*','agent_detail.*')
					->where('agent_detail.agent_id','=',$id)
					// ->where('top_down',null)
					->get();
				// dd($details);
    	return view('backend.agent.detail',compact('details','topdown'))
    			// ->with('details',$details)
		    	->with('agent',$agent)
		    	->with('i', ($request->input('pages', 1) - 1) * 5)
		    	->with('js',$js)
		    	->with('css',$css)
		    	->with('page',$page);
		    	
	}
    public function show_refund($id){
        session_start();
        $refund=DB::table('agent_balance')
                ->join('agent','agent.id_agent','agent_balance.id_agent')
                ->where('agent_balance.status',3)
                ->select('*')
                ->get();
        // dd($refund);
         return Datatables::of($refund)
                ->addColumn('status',function ($refund) { return
                    $refund->status == 1 ? '<label class="label label-success"> Added </label>' : ($refund->status == 2 ? '<label class="label label-warning">processing</label>' : ($refund->status == 3 ? '<label class="label label-info">Refund Succes</label>' : ($refund->status_topdown == 1 ? '<label class="label label-primary"> Booked </label>' : ($refund->status_topdown == 2 ? '<label class="label label-warning">Calcel Booking</label>' : '<label class="label label-danger">wrong'))));
                })
                ->editColumn('top_up', function ($refund) {
                    return  $refund->top_up > 0 ? 'Rp.'.number_format($refund->top_up, 0)  : '-';
                     })
                ->addIndexColumn()
                ->make(true);
    }
	public function show_topdown($id){
        session_start();
		$agent=DB::table('agent_balance')
				->select('*')
				->where('id_agent',$id)
				->where('top_up',null)
				->get();

		 return Datatables::of($agent)
                ->addColumn('status',function ($agent) { return
                    $agent->status == 1 ? '<label class="label label-success"> Added </label>' : ($agent->status == 2 ? '<label class="label label-warning">processing</label>' : ($agent->status == 3 ? '<label class="label label-danger">Rejected</label>' : ($agent->status_topdown == 1 ? '<label class="label label-primary"> Booked </label>' : ($agent->status_topdown == 2 ? '<label class="label label-warning">Calcel Booking</label>' : '<label class="label label-danger">wrong'))));
                })
                ->editColumn('balance_saldo', function ($agent) {
                    return 'Rp.'.number_format($agent->balance_saldo, 0);
                     })
                ->editColumn('top_down', function ($agent) {
                    return  $agent->top_down > 0 ? 'Rp.'.number_format($agent->top_down, 0)  : '-';
                     })
                ->addIndexColumn()
                ->make(true);
	}

	public function show_topup($id){
        session_start();
		$agent=DB::table('agent_balance')
				->select('*')
				->where('id_agent',$id)
                ->where('status','<',3 )
                ->orwhere('status','=',4)
				->where('top_down',null)
				->get();
       
        // dd($agent);

		 return Datatables::of($agent)
                ->addColumn('status',function ($agent) { return
                    $agent->status == 1 ? '<label class="label label-success"> Added </label>' : ($agent->status == 2 ? '<label class="label label-warning">processing</label>' : ($agent->status == 4 ? '<label class="label label-danger">Aborted</label>' : ($agent->status_topdown == 1 ? '<label class="label label-primary"> Booked </label>' : ($agent->status_topdown == 2 ? '<label class="label label-warning">Calcel Booking</label>' : '<label class="label label-danger">wrong'))));
                })
                ->editColumn('balance_saldo', function ($agent) {
                    return 'Rp.'.number_format($agent->balance_saldo, 0);
                     })
                ->editColumn('top_up', function ($agent) {
                    return  $agent->top_up > 0 ? 'Rp.'.number_format($agent->top_up, 0)  : '-';
                     })
                ->editColumn('picture', function ($agent) {
                    return '<a target="_blank" href="'.url('/img/topup/'.$agent->id_agent.'_/'.$agent->picture).'"><img src="'.url('/img/topup/'.$agent->id_agent.'_/'.$agent->picture).'" class="img-responsive" style="width:50%" ></a>';
                     })
                ->editColumn('action', function ($agent) {
                    return  $agent->status == 1 ? '<a href="#" class="button disabled btn-sm btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>' : ($agent->status == 2 ? '<a href="'.url('/backend/agent/topup/'.$agent->id.'/'.$agent->id_agent).'" class="btn-sm btn-success" onclick="return confirm(\'Are you sure your balance will be added ?\');"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a> <a href="'.url('/backend/agent/topup/cancel/'.$agent->id.'/'.$agent->id_agent).'" class="btn-sm btn-danger" onclick="return confirm(\'Are you sure your balance will be aborted ?\');"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>' : ($agent->status == 4 ? '<a href="#" class="button disabled btn-sm btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>' : 'else'));
                     })
                ->addIndexColumn()
                ->make(true);
	}

    public function show_member($id){
        session_start();
        $member=DB::table('agent_member')
                ->select('*')
                ->where('id_agent',$id)
                ->where('akses',0)
                ->get();

         return Datatables::of($member)
                ->addIndexColumn()
                ->make(true);
    }

	public function topup($history,$agent){
        session_start();
		$add_balance = AgentBalanceModel::find($history);
		$last_balance = AgentModel::find($agent);
		$sessionlogin = session('login');
		$date_confirm = date('Y-m-d H:i:s');
		// echo $add_balance->top_up; //tambah saldo baru
		// echo $last_balance->last_balance;
		// dd($last_balance);

		$new_last_balance = $last_balance->last_balance+$add_balance->top_up;
		// dd($new_last_balance);

		$last_balance->last_balance = $new_last_balance;

		$add_balance->balance_saldo = $new_last_balance;
		$add_balance->status = 1;
		$add_balance->created_by = $sessionlogin['username'];
		$add_balance->date_confirm = $date_confirm;

		if($last_balance->save() && $add_balance->save()){
			return redirect('backend/agent/detail/'.$agent)
					->with('alert-success', 'Data Berhasil Disimpan.');
		}
	}

    public function topup_cancel($history,$agent){
        session_start();
        $add_balance = AgentBalanceModel::find($history);
        $last_balance = AgentModel::find($agent);
        $sessionlogin = session('login');
        $date_confirm = date('Y-m-d H:i:s');
       
        $add_balance->status = 4;
        $add_balance->balance_saldo = $last_balance->last_balance;
        $add_balance->created_by = $sessionlogin['username'];
        $add_balance->date_confirm = $date_confirm;

        if($add_balance->save()){
            return redirect('backend/agent/detail/'.$agent)
                    ->with('alert-success', 'Data Berhasil Disimpan.');
        }
    }

    public function show_test(){
        session_start();
    	$agent = DB::table('agent')
					->join('agent_detail', 'agent_detail.agent_id', '=', 'agent.id_agent')
					->select('agent.*','agent_detail.*')
					->where('agent.id_agent', '=', '48')
					->get();
		dd($agent);
    }

}
