<?php

namespace App\Http\Controllers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Session;
use App\UserModel;
use App\Http\Requests;
use Carbon\Carbon;


class HomeController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
	function __construct() {
		// $this->middleware('SystemRuleGuide');

	}
	
    public function index(Request $request) {
    	session_start();
		$url=url('/');
		$js="<script src=\"".$url."/src/backend/plugins/jquery/dist/jquery.min.js\"></script>";
		$css="<link href=\"".$url."/src/backend/css/bootstrap.min.css\" rel=\"stylesheet\">";
		$page['title']="Dashboard | Korina Hotel Admin";
		// $daily = DB::table('booking_hotel')
		// 		->select('')
		// 		->where()
		// 		->
		// $sql = " DATEDIFF(items.date_start, '".date('Y-m-d')."' ) = ".$SomeDayDiff;
		// return Item::whereRaw( $sql )->get();
		$fromDate = new Carbon('last monday'); //monday setiap hari senin terakhir - last week 2 minggu
		$toDate = new Carbon('now'); 

		// echo "<br>from ".$fromDate;
		// echo "<br>to ".$toDate;
		// die;

		$monthly = DB::table('booking_hotel')
					->select('*')
					->whereMonth('date_booking', '=', date('m'))
					->count();
		// dd($monthly);

		$weekly = DB::table('booking_hotel')
					->select('*')
					->whereBetween('date_booking', [$fromDate->toDateTimeString(), $toDate->toDateTimeString()])
					// ->whereBetween('votes', [1, 100])->get();
					->count();

		// dd($weekly);

		$daily = DB::table('booking_hotel')
					->select('*')
					->whereDay('date_booking', '=', date('d')) 
					->count();
		$notif = DB::table('agent_balance')
					->join('agent','agent.id_agent','agent_balance.id_agent')
					->select('*')
					->where('date_confirm',null)
					->get();
		$listToday = DB::table('booking_hotel')
					->join('agent_member','agent_member.agent_account_number','booking_hotel.agent_account_number')
					// ->join('agent_detail', 'agent_detail.agent_account_number', '=', 'booking_hotel.agent_account_number')
					// ->join('agent', 'agent.id_agent', '=', 'agent_detail.agent_id')
					// ->select('agent_detail.*','agent.*','booking_hotel.*')
					->whereDay('booking_hotel.date_booking', date('d'))
					// ->where('date_booking', '=', Carbon::today())
					// ->whereDay('date_booking', '=', date('m')) 
					// ->orderBy('booking_hotel.id', 'asc')
					->get();

		$incomeDaily = DB::table('booking_hotel')
					->select( DB::raw('SUM(total_price_idr)-SUM(refund_idr) as total'))
					->whereDay('date_booking', '=', date('d'))
					->first(); 
		$refundDaily = DB::table('booking_hotel')
					->select( DB::raw('SUM(refund_idr) as total'))
					->whereDay('date_booking', '=', date('d'))
					->first();

		$incomeWeekly = DB::table('booking_hotel')
					->select(DB::raw('SUM(total_price_idr)-SUM(refund_idr) as total'))
					->whereBetween('date_booking', [$fromDate->toDateTimeString(), $toDate->toDateTimeString()])
					->first();
		$refundWeekly = DB::table('booking_hotel')
					->select(DB::raw('SUM(refund_idr) as total'))
					->whereBetween('date_booking', [$fromDate->toDateTimeString(), $toDate->toDateTimeString()])
					->first();

		$incomeMonthly = DB::table('booking_hotel')
					->select(DB::raw('SUM(total_price_idr)-SUM(refund_idr) as total'))
					->whereMonth('date_booking', '=', date('m'))
					->first();
		$refundMonthly = DB::table('booking_hotel')
					->select(DB::raw('SUM(refund_idr) as total'))
					->whereMonth('date_booking', '=', date('m'))
					->first();


		// dd($incomeWeekly);
		// dd($listToday);
		// $i=1;
		// foreach($notif as $x){
		// 	$_SESSION['Notif']['agent_name']=$x->agent_name;
		// 	$_SESSION['Notif']['last_balance']=$x->last_balance;
		// }
		// // dd($i);
			$_SESSION['Notif']=$notif;
		// dd(count($_SESSION['Notif']));
		// dd($_SESSION['Notif'][0]->id);
		// dd($notif);
		return view('backend.index')
				->with('js',$js)
				->with('css',$css)
				->with('page',$page)
				->with('daily',$daily)
				->with('weekly',$weekly)
				->with('monthly',$monthly)
				->with('listToday',$listToday)
				->with('incomeDaily',$incomeDaily)
				->with('refundDaily',$refundDaily)
				->with('incomeWeekly',$incomeWeekly)
				->with('refundWeekly',$refundWeekly)
				->with('incomeMonthly',$incomeMonthly)
				->with('refundMonthly',$refundMonthly);
    }

    public function login()
    {
		$sessionlogin=session('login');
		if(!isset($sessionlogin)){
			return view('backend.login');
		}else{
			return redirect()->route('home');
		}
    }

    public function login_submit(Request $request){
			$cruds = new UserModel();
			$username=$request->username;
			$password=md5(md5($request->password));
			$log_in = DB::table('system_user')
            ->select('system_user.*')
			->where('username','=',$username)
			->where('password','=',$password)
			->first();
			if(!empty($log_in)){
				$data=array(		
					'username' =>$log_in->username,
					'user_group_id' =>$log_in->user_group_id,
					'user_name' =>$log_in->user_name
				);
				session(['login'=>$data]);
				return redirect()->route('home');
			}else{
				$gagal='<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<strong>Invalid username / password</strong>
				</div>';
				session(['gagal'=>$gagal]);
				 return redirect()->route('korinaadmin');
			}
	}

}