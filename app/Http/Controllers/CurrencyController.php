<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use App\CurrencyModel;
use App\Http\Requests;
use App\Http\Requests\Currency\CurrencyRequest;
use Session;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 function __construct()
		{
			$this->middleware('SystemRule');
			session_start();
		 
		}
	 
    public function index()
    {
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>	
			<script>
				$(function() {
					var table = $(\"#currency\").DataTable({
						processing: true,
						serverSide: true,
						ajax: \"".$url."/currency/data\",
						columns: [
						  { data: 'currency_code' },
						  { data: 'currency_name' },
						  { data: 'currency_margin' },
						  { data: 'action', 'searchable': false, 'orderable':false }
					   ]
					});
				});
			</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="Currency";
		return view('backend.currency.list')->with('js',$js)->with('css',$css)->with('page',$page);
	
    }
	
	public function data_show(Request $request){
	   if($request->ajax()){
		$url=url('/');
		$currency = CurrencyModel::select('*')
					->get();
		return Datatables::of($currency)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($currency) { return
				'<a href="'.url('/').'/backend/currency/edit/'.$currency->id.'" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/backend/currency/delete/'.$currency->id.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				';})
				->removeColumn('id')
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url=url('/');
		$js="";
		$css="";
		$page['title']="Currency Add";
		return view('backend.currency.formcreate')->with('js',$js)->with('css',$css)->with('page',$page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$session=session('login');
		// print_r($session); exit;
         $cruds = new CurrencyModel();
		 $cruds->currency_name = $request->currency_name;
		 $cruds->currency_margin = $request->currency_margin;
		 $cruds->currency_code = $request->currency_code;
		 $cruds->created_at = date('Y-m-d H:i:s');
		 $cruds->created_by = $session['username'];
		 $cruds->last_update = date('Y-m-d H:i:s');
		 // print_r($cruds); exit;
		 if($cruds->save()){
			  return redirect()->route('currency.create')->with('alert-success', 'Data Berhasil Disimpan.');
		 }
		 else{
			 return redirect()->route('currency.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		 }
		 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $url=url('/');
		$js="";
		$css="";
		$page['title']="Currency Edit";
		$cruds = CurrencyModel::find($id);
		return view('backend.currency.formupdate')->with('js',$js)->with('css',$css)->with('page',$page)->with('currency', $cruds);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$session=session('login');
		$id=$request->id;
        $cruds = CurrencyModel::find($id);
		$cruds->currency_name = $request->currency_name;
		$cruds->currency_margin = $request->currency_margin;
		$cruds->currency_code = $request->currency_code;
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			  return redirect()->route('currency.edit',['id'=>$id])->with('alert-success', 'Data Berhasil Diubah.');
		 }
		 else{
			 return redirect()->route('currency.edit',['id'=>$id])->with('alert-danger', 'Data Tidak Berhasil Diubah.');
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$cruds = CurrencyModel::find($id);
		// $cruds->data_state = '1';
        // $cruds = CurrencyModel::findOrFail($id);
		if($cruds->delete()){
			  return redirect()->route('currency.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('currency.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }
}
