<?php
/**
 * Created by PhpStorm.
 * User: Korina Developer
 * Date: 12/5/2017
 * Time: 9:37 AM
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Image;
use Datatables;
use Session;
use App\AgentModel;
use App\AgentDetailModel;
use App\AgentBalanceModel;
use App\AgentMemberModel;
use App\HotelModel;
use App\Http\Requests;
use Illuminate\Support\Str;
use Input;
use Response;


class FrontendController extends Controller
{

    // function __construct() {
    //     $this->middleware('SystemRuleUser');

    // }
    function index(){
        $sessionlogin=session('login_user');
        $hotel = DB::table('hotel')
                    ->join('hotel_detail','hotel_detail.id_hotel','hotel.id')
                    ->join('master_country','master_country.id','hotel_detail.country_code')
                    ->join('master_city','master_city.id','hotel_detail.city_code')
                    ->select('*')
                    ->orderBy('view_count','DESC')
                    ->get();
        // dd($hotel);
        return view('citytour.home')
            ->with('session_user',$sessionlogin)
            ->with('hotel',$hotel);
    }

    function sortMixByStarFacilityPrice2($param)
    {
        if(strpos($param, 'rat') === false){
            // echo "ada";
            //echo "pertama pasti mihi facility";
            goto sort_facility;
            //goto sort_facility;
        }
        else
        {
            echo "pasti milih rating";
            goto sort_rating;
        }
        die;
        sort_rating:
        $jumlahParameter=substr_count($param,"_");
        echo $jumlahParameter;
        die();
        $patternAwal="(.*)";
        $patternDinamis="_(.*)";
        for($i=0,$hitungJumahPattern=1,$patternGabung="";$i<$jumlahParameter;$i++)
        {
            $patternGabung.=$patternDinamis;
            $hitungJumahPattern++;
        }
        $patternFinal=$patternAwal."".$patternGabung;
        preg_match("/".$patternFinal."/",$param,$hasil);
        for($j=1,$tampungParam="";$j<=$hitungJumahPattern;$j++)
        {
            $tampungParam.="= ".$hasil[$j]." OR rating ";
        }
        $whereCond="rating ".preg_replace("~ OR rating (?!.* OR rating )~","",$tampungParam);
      /*  echo $whereCond;
        die;*/
        $data_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            ->join('master_picture','master_picture.id_hotel','=','hotel.id')
            ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address','master_picture.name')
            ->whereRaw($whereCond)
            // ->where('master_picture.size_type','=',1)
            ->OrderBy('id')
            ->get();
        $result=array();
        foreach($data_hotel as $baris)
        {
            $data = array(
                'id'    =>    $baris->id,
                'hotel_name'    =>    $baris->hotel_name,
                'priceStartFrom' => number_format((($this->cheapestPrice($baris->id)*(100+15))/100)*13573.50, 0,".","."),
                'hargaCoret' => number_format((($this->cheapestPrice($baris->id)*(100+30))/100)*13573.50, 0,".","."),
                'hotel_address' =>$baris->hotel_address,
                'slug'=>$baris->slug,
                'name'=>$baris->name,
                'rating'=>$baris->rating
            );
            array_push($result,$data);
        }
        echo json_encode($result);



        /*$data_hotel_x = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            ->join('master_picture','master_picture.id_hotel','=','hotel.id')
            ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address','master_facility.facility_name')
            ->join('hotel_facility','hotel_facility.id_hotel','=','hotel.id')
            ->join('master_facility','master_facility.id','=','hotel_facility.id_facility')
            //->where('hotel.id','=',$baris->id)
            ->whereRaw($whereCond)
            ->where('master_picture.size_type','=',1)
            ->where('master_facility.facility_name','=','Swimming')
            ->OrderBy('id')
            ->distinct()
            ->get();
        print_r($data_hotel_x);
        die;*/


        sort_facility:
        $jumlahParameter=substr_count($param,"_");

        $param=str_replace("fac", "", $param);
        /*echo $jumlahParameter;
        die();*/
        $patternAwal="(.*)";
        $patternDinamis="_(.*)";
        for($i=0,$hitungJumahPattern=1,$patternGabung="";$i<$jumlahParameter;$i++)
        {
            $patternGabung.=$patternDinamis;
            $hitungJumahPattern++;
        }
        $patternFinal=$patternAwal."".$patternGabung;
        preg_match("/".$patternFinal."/",$param,$hasil);
       /* echo $hasil[1];
        die;*/
        for($j=1,$tampungParam="";$j<=$hitungJumahPattern;$j++)
        {
            $tampungParam.="= '".$hasil[$j]."' OR master_facility.facility_name ";
        }
/*        echo $tampungParam;
        die;*/
        $whereCond="master_facility.facility_name ".preg_replace("~ OR master_facility.facility_name (?!.* OR master_facility.facility_name )~","",$tampungParam);
        $result=array();
        //Swimming
        /*echo $whereCond;
        die;*/

            $data_hotel_x = DB::table('hotel')
                ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
                ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address','master_facility.facility_name')
                ->join('hotel_facility','hotel_facility.id_hotel','=','hotel.id')
                ->join('master_facility','master_facility.id','=','hotel_facility.id_facility')
                //->whereRaw("master_facility.facility_name ='Swimming'")
                ->whereRaw($whereCond)
                //->where('hotel.id','=',$baris->id)
                //->where('master_facility.facility_name','=','Swimming')
                ->OrderBy('id')
                ->distinct()
                ->get();
/*            foreach($data_hotel_x as $baris)
            {
              echo $baris->id."<br/>";

              if($baris->id);
            }*/
        foreach($data_hotel_x  as $baris){
            $hasil[$baris->id] = null;
        }
        echo implode('<br/>', array_keys($hasil));
            //print_r($data_hotel_x);

            /*echo $baris->id."<br/>";*/



        die;


        /*->join('hotel_facility','hotel_facility.id_hotel','=','hotel.id')
        ->join('master_facility','master_facility.id','=','hotel_facility.id_facility')
        */
        rating_star:
        $result=array();
        foreach($data_hotel as $baris)
        {
            $data = array(
                'id'    =>    $baris->id,
                'hotel_name'    =>    $baris->hotel_name,
                'priceStartFrom' => number_format((($this->cheapestPrice($baris->id)*(100+15))/100)*13573.50, 0,".","."),
                'hargaCoret' => number_format((($this->cheapestPrice($baris->id)*(100+30))/100)*13573.50, 0,".","."),
                'hotel_address' =>$baris->hotel_address,
                'slug'=>$baris->slug,
                'name'=>$baris->name,
                'rating'=>$baris->rating
            );
            array_push($result,$data);
        }
        echo json_encode($result);
    }
    function sortMixByStarFacilityPrice($param)
    {
         $jumlahParameter=substr_count($param,"_");
         // dd($jumlahParameter);
        $patternAwal="(.*)";
        $patternDinamis="_(.*)";
        for($i=0,$hitungJumahPattern=1,$patternGabung="";$i<$jumlahParameter;$i++)
        {
            $patternGabung.=$patternDinamis;
            $hitungJumahPattern++;
        }
        $patternFinal=$patternAwal."".$patternGabung;
        preg_match("/".$patternFinal."/",$param,$hasil);
        for($j=1,$tampungParam="";$j<=$hitungJumahPattern;$j++)
        {
            $tampungParam.="= ".$hasil[$j]." OR rating ";
        }
        $whereCond="rating ".preg_replace("~ OR rating (?!.* OR rating )~","",$tampungParam);
        $data_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            //->join('master_picture','master_picture.id_hotel','=','hotel.id')
            ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address')
            ->whereRaw($whereCond)
            //->where('master_picture.size_type','=',1)
            ->OrderBy('id')
            ->get();

         $idr = app('App\Http\Controllers\For_testing')->convertPrice();
             $price = (double)filter_var($idr, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

                 foreach($data_hotel as $baris){
            $book[] = DB::table('booking_hotel')
                    ->select('*')
                    ->where('mg_id_hotel',$baris->id_from_mg)
                    ->count();
        }

        $result=array();
        $i=0;
        foreach($data_hotel as $baris)
        {
            $data = array(
                'id'    =>    $baris->id,
                'hotel_name'    =>    $baris->hotel_name,
                'priceStartFrom' => number_format((($this->cheapestPrice($baris->id)*(100+15))/100)*$price, 0,".","."),
                'hargaCoret' => number_format((($this->cheapestPrice($baris->id)*(100+30))/100)*$price, 0,".","."),
                'hotel_address' =>$baris->hotel_address,
                'slug'=>$baris->slug,
                'rating'=>$baris->rating,
                'counter'=>$baris->view_count,
                'book'=>$book[$i]
                );
            $i++;
            array_push($result,$data);
        }
        echo json_encode($result);
    }

    public function orderByFacilty($param){
        $jumlahParameter=substr_count($param,"_");
         // dd($jumlahParameter);
        $patternAwal="(.*)";
        $patternDinamis="_(.*)";
        for($i=0,$hitungJumahPattern=1,$patternGabung="";$i<$jumlahParameter;$i++)
        {
            $patternGabung.=$patternDinamis;
            $hitungJumahPattern++;
        }
        $patternFinal=$patternAwal."".$patternGabung;
        preg_match("/".$patternFinal."/",$param,$hasil);
        for($j=1,$tampungParam="";$j<=$hitungJumahPattern;$j++)
        {
            $tampungParam.="= ".$hasil[$j]." OR id_facility ";
        }
        $whereCond="id_facility ".preg_replace("~ OR id_facility (?!.* OR id_facility )~","",$tampungParam);
        // dd($whereCond);
        $data_hotel = DB::table('hotel_facility')
            ->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
            ->join('master_facility','master_facility.id','hotel_facility.id_facility')
            ->join('hotel_detail','hotel_detail.id_hotel','hotel.id')
            //->join('master_picture','master_picture.id_hotel','=','hotel.id')
            // ->select('hotel_facility.*','master_facility.*','hotel.*','hotel_detail.hotel_address')
            ->whereRaw($whereCond)
            //->where('master_picture.size_type','=',1)
            ->distinct()
            ->OrderBy('hotel.id')
            ->get(['hotel.id','hotel.id_from_mg','hotel.view_count','hotel.hotel_name','hotel_detail.hotel_address','hotel.slug','hotel.rating']);
        // dd($data_hotel);
             $idr = app('App\Http\Controllers\For_testing')->convertPrice();
             $price = (double)filter_var($idr, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            

            foreach($data_hotel as $baris){
            $book[] = DB::table('booking_hotel')
                    ->select('*')
                    ->where('mg_id_hotel',$baris->id_from_mg)
                    ->count();
        }
        $result=array();
        $i=0;
        foreach($data_hotel as $baris)
        {
            $data = array(
                'id'    =>    $baris->id,
                'hotel_name'    =>    $baris->hotel_name,
                'priceStartFrom' => number_format((($this->cheapestPrice($baris->id)*(100+15))/100)*$price, 0,".","."),
                'hargaCoret' => number_format((($this->cheapestPrice($baris->id)*(100+30))/100)*$price, 0,".","."),
                'hotel_address' =>$baris->hotel_address,
                'slug'=>$baris->slug,
                'rating'=>$baris->rating,
                'counter'=>$baris->view_count,
                'book'=>$book[$i]
                );
            $i++;
            array_push($result,$data);
        }
        // json_encode($result);
        // die;
        // $tmp = array();
        // foreach ($result as $v){
        //      $id = $v['hotel_name'] . "|" . $v['priceStartFrom'];
        //      isset($tmp[$id]) or $tmp[$id] = $v;
        // }
        // print_r($tmp);die;
        // $tmp = array_diff($tmp, array('Amari Boulevard|811.695'));
        //  dd($tmp);
        // $x=$result[''];

    //     $ids = array_column($result, 'id');
    //     // if ($ids=$ids){
    //     $ids = array_unique($ids);
    //     $result = array_filter($result, function ($key, $value) use ($ids) {
    //         return in_array($value, array_keys($ids));
    //     }, ARRAY_FILTER_USE_BOTH);
    // // }
    //     print_r($result);
    //     die;
        // dd($result);
        // print_r(array_values($tmp));
        // die;
        // dd($tmp);
         // $x = ;
       // $val=array_unique (array_column($result, 'id'), SORT_REGULAR);

       // dd($val);
       // $array = array_unique($result, SORT_STRING);
       
       // dd($val);
        echo json_encode($result);
    }

    public function orderByRate($val){
         $data_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            //->join('master_picture','master_picture.id_hotel','=','hotel.id')
            ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address')
            // ->where('rating',$val)
            //->where('master_picture.size_type','=',1)
            ->OrderBy('rating',$val)
            ->get();

             $idr = app('App\Http\Controllers\For_testing')->convertPrice();
             $price = (double)filter_var($idr, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

             $book = array();

        foreach($data_hotel as $baris){
            $book[] = DB::table('booking_hotel')
                    ->select('*')
                    ->where('mg_id_hotel',$baris->id_from_mg)
                    ->count();
        }

        $result=array();
        $i=0;
        foreach($data_hotel as $baris)
        {
            $data = array(
                'id'    =>    $baris->id,
                'hotel_name'    =>    $baris->hotel_name,
                'priceStartFrom' => number_format((($this->cheapestPrice($baris->id)*(100+15))/100)*$price, 0,".","."),
                'hargaCoret' => number_format((($this->cheapestPrice($baris->id)*(100+30))/100)*$price, 0,".","."),
                'hotel_address' =>$baris->hotel_address,
                'slug'=>$baris->slug,
                'rating'=>$baris->rating,
                'counter'=>$baris->view_count,
                'book'=>$book[$i]
                );
            $i++;
            array_push($result,$data);
        }
        echo json_encode($result);
    } 
    public function orderByPrice($val){
         $data_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            //->join('master_picture','master_picture.id_hotel','=','hotel.id')
            ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address')
            ->get();

         $idr = app('App\Http\Controllers\For_testing')->convertPrice();
             $price = (double)filter_var($idr, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        $book = array();
        foreach($data_hotel as $baris){
            $book[] = DB::table('booking_hotel')
                    ->select('*')
                    ->where('mg_id_hotel',$baris->id_from_mg)
                    ->count();
        }

        $result=array();
        $i=0;
        foreach($data_hotel as $baris)
        {
            $data = array(
                'id'    =>    $baris->id,
                'hotel_name'    =>    $baris->hotel_name,
                'priceStartFrom' => number_format((($this->cheapestPrice($baris->id)*(100+15))/100)*$price, 0,".","."),
                'hargaCoret' => number_format((($this->cheapestPrice($baris->id)*(100+30))/100)*$price, 0,".","."),
                'hotel_address' =>$baris->hotel_address,
                'slug'=>$baris->slug,
                'rating'=>$baris->rating,
                'counter'=>$baris->view_count,
                'book'=>$book[$i]
                );
            $i++;
            array_push($result,$data);
        }
      
      // $val=="lower" ? array_multisort (array_column($result, 'priceStartFrom'), SORT_ASC, $result) : array_multisort (array_column($result, 'priceStartFrom'), SORT_DESC,  $result) ;

        // dd($result);
        // print_r($result);
        // die;
        // $x = array_column($result, 'priceStartFrom');
        // $aa = strval($x[0]);
        // // $xx = (string)$x[0];
        // // echo $xx;
        // // die;
        // $a = str_replace('.','',$aa);
        // echo $a;


        // die;
        if ($val=="lower"){
            usort($result, function($a,$b){
                 // $convert = explode($decimal,$number);
                 // str_replace("world","Peter","Hello world!");
                    return str_replace('.','',strval($a['priceStartFrom'])) > str_replace('.','',strval($b['priceStartFrom'])) ? 1 : -1;
                
            });
        }
        if ($val=="higher"){
            usort($result, function($a,$b){
                 // $convert = explode($decimal,$number);
                 // str_replace("world","Peter","Hello world!");
                    return str_replace('.','',strval($a['priceStartFrom'])) < str_replace('.','',strval($b['priceStartFrom'])) ? 1 : -1;
                
            });
        }

        // foreach ($result as $key => $value) {
        //     $result["priceStartFrom"] = intval($value);
        // }

        // sort($result, SORT_NUMERIC);
        // dd($result);

          // foreach ($result as $key => $row)
          //   {
          //       $priceStartFrom[$key]  = $row['priceStartFrom'];
          //   }    

          //   array_multisort($priceStartFrom, SORT_DESC,  $result);
            // dd($result);
        echo json_encode($result);
    }
    public static function cheapestPrice($idHotel){
        $harga_termurah = DB::table('master_room')
            ->select('master_room.net_price')
            ->where('master_room.id_hotel','=',$idHotel)
            ->where('master_room.id_bed_room','=',1)
            ->first();
        //*13519
        return $harga_termurah->net_price;
    }
    function delete_semua()
    {
        DB::table('master_room')->delete();
    }

    function detail_hotel(Request $request)
    {   $url = url('/');
        $css =' <!-- CSS -->
    <link href="'.$url.'/src/citytour/css/slider-pro.min.css" rel="stylesheet">

    <link href="'.$url.'/src/citytour/css/owl.carousel.css" rel="stylesheet">
    <link href="'.$url.'/src/citytour/css/owl.theme.css" rel="stylesheet">
    <link href="'.$url.'/src/citytour/css/nprogress.css" rel="stylesheet">
    <link href="'.$url.'/src/citytour/css/hotel-datepicker.css" rel="stylesheet">
    <style>
        #tombol_book{
            margin-top:22px;
        }
        #mapIni {
          height: 100%;
         }
         html, body {
  height: 100%;
  margin: 0;
  padding: 0;
}
    </style>';
        $js='<!-- Specific scripts -->
    <script src="'.$url.'/src/citytour/js/icheck.js"></script>
    <!-- Date and time pickers -->
    <script src="'.$url.'/src/citytour/js/jquery.sliderPro.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $(\'#Img_carousel\').sliderPro({
                width: 960,
                height: 500,
                fade: true,
                arrows: true,
                buttons: false,
                fullScreen: false,
                smallSize: 500,
                startSlide: 0,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: true,
                autoplay: false
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(".carousel").owlCarousel({
                items: 4,
                itemsDesktop: [1199, 3],
                itemsDesktopSmall: [979, 3]
            });
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOjoASTeCYgA3poXFzOpd_ienvMFLiUSc&callback=myMap"></script>';
        $sessionlogin=session('login_user');
        $id_hotel=$request->hotel_id;
        $data_detail_facility = DB::table('hotel_facility')
            ->join('master_facility', 'master_facility.id', '=', 'hotel_facility.id_facility')
            ->select('master_facility.facility_name')
            ->where('id_hotel','=',$id_hotel)
            ->get();

        $data_detail_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            ->select('hotel_detail.*','hotel.*')
            ->where('id_hotel','=',$id_hotel)
            ->get();

            // dd($data_detail_hotel);

        $data_detail_room_single = DB::table('hotel')
            ->join('master_room','master_room.id_hotel', '=', 'hotel.id')
            ->join('master_food', 'master_food.id', '=', 'master_room.bf_type')
            ->join('room_bed', 'room_bed.id', '=', 'master_room.id_bed_room')
            ->select('hotel.hotel_name','master_room.room_name','master_room.net_price','master_food.type_name','room_bed.room_type')
            ->where('hotel.id',$id_hotel)
            ->where('room_bed.room_type','Single')
            ->get();

        $data_detail_room_twin = DB::table('hotel')
            ->join('master_room','master_room.id_hotel', '=', 'hotel.id')
            ->join('master_food', 'master_food.id', '=', 'master_room.bf_type')
            ->join('room_bed', 'room_bed.id', '=', 'master_room.id_bed_room')
            ->select('hotel.hotel_name','master_room.room_name','master_room.net_price','master_food.type_name','room_bed.room_type')
            ->where('hotel.id',$id_hotel)
            ->where('room_bed.room_type','Twin')
            ->get();

        $master_picture = DB::table('master_picture')
            ->select('*')
            ->where('id_hotel',$id_hotel)
            ->where('type_picture',5)
            ->get();

        //  $blogKey = 'blog_' . $id_hotel;
        //  // dd($blogKey);

        // // Check if blog session key exists
        // // If not, update view_count and create session key
        // if (!Session::has($blogKey)) {
        //     dd('sini');
            //increment view conter
             $counter = DB::table('hotel')->where('id', $id_hotel)->increment('view_count');
             // dd($a);
           
        //     Session::put($blogKey, 1);
        // }

        return view('citytour.detail',compact('master_picture','data_detail_facility','data_detail_hotel','data_detail_room_single','data_detail_room_twin'))->with('session_user',$sessionlogin)->with('css',$css)->with('js',$js);
    }

    function detail_hotel_home(Request $request)
    {   $url = url('/');
        $css =' <!-- CSS -->
    <link href="'.$url.'/src/citytour/css/slider-pro.min.css" rel="stylesheet">

    <link href="'.$url.'/src/citytour/css/owl.carousel.css" rel="stylesheet">
    <link href="'.$url.'/src/citytour/css/owl.theme.css" rel="stylesheet">
    <link href="'.$url.'/src/citytour/css/nprogress.css" rel="stylesheet">
    <link href="'.$url.'/src/citytour/css/hotel-datepicker.css" rel="stylesheet">
    <style>
        #tombol_book{
            margin-top:22px;
        }
    </style>';
        $js='<!-- Specific scripts -->
    <script src="'.$url.'/src/citytour/js/icheck.js"></script>
    <!-- Date and time pickers -->
    <script src="'.$url.'/src/citytour/js/jquery.sliderPro.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $(\'#Img_carousel\').sliderPro({
                width: 960,
                height: 500,
                fade: true,
                arrows: true,
                buttons: false,
                fullScreen: false,
                smallSize: 500,
                startSlide: 0,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: true,
                autoplay: false
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(".carousel").owlCarousel({
                items: 4,
                itemsDesktop: [1199, 3],
                itemsDesktopSmall: [979, 3]
            });
        });
    </script>';
        $sessionlogin=session('login_user');
        $id_hotel=$request->hotel_id;
        $data_detail_facility = DB::table('hotel_facility')
            ->join('master_facility', 'master_facility.id', '=', 'hotel_facility.id_facility')
            ->select('master_facility.facility_name')
            ->where('id_hotel','=',$id_hotel)
            ->get();

        $data_detail_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            ->select('hotel_detail.*','hotel.*')
            ->where('id_hotel','=',$id_hotel)
            ->get();

        $data_detail_room_single = DB::table('hotel')
            ->join('master_room','master_room.id_hotel', '=', 'hotel.id')
            ->join('master_food', 'master_food.id', '=', 'master_room.bf_type')
            ->join('room_bed', 'room_bed.id', '=', 'master_room.id_bed_room')
            ->select('hotel.hotel_name','master_room.room_name','master_room.net_price','master_food.type_name','room_bed.room_type')
            ->where('hotel.id',$id_hotel)
            ->where('room_bed.room_type','Single')
            ->get();

        $data_detail_room_twin = DB::table('hotel')
            ->join('master_room','master_room.id_hotel', '=', 'hotel.id')
            ->join('master_food', 'master_food.id', '=', 'master_room.bf_type')
            ->join('room_bed', 'room_bed.id', '=', 'master_room.id_bed_room')
            ->select('hotel.hotel_name','master_room.room_name','master_room.net_price','master_food.type_name','room_bed.room_type')
            ->where('hotel.id',$id_hotel)
            ->where('room_bed.room_type','Twin')
            ->get();
        return view('citytour.detail',compact('data_detail_facility','data_detail_hotel','data_detail_room_single','data_detail_room_twin'))->with('session_user',$sessionlogin)->with('css',$css)->with('js',$js);
    }

    public function list_hotel_avail($dewasa,$cekin,$cekout,$mg_code,$kamar,$sumChild,$ageJoin){
        // dd($dewasa.$cekin.$cekout.$mg_code.$kamar.$sumChild.$ageJoin);
        $avail = app('App\Http\Controllers\APIHotel_korina')->APIS_SearchHotelByDestination($dewasa,$cekin,$cekout,$mg_code,$kamar,$sumChild,$ageJoin);
        // echo 'ini'.$avail;
        // $list = array();
        // $list = [];
        for($a=0;$a<count($avail);$a++){
            $x = DB::table('hotel')
                    ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
                    ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address')
                    ->where('hotel.id_from_mg',$avail[$a])
                    ->OrderBy('hotel_name','ASC')
                    ->get();
            // $list = array_merge($x);
        }
        dd($x);
        
    }

    function list_hotel()
    {   
        $sessionlogin=session('login_user');
       $data_list_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            // ->join('master_picture','master_picture.id_hotel','hotel.id')
            ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address')
            // ->where('master_picture.size_type','=',1)

            ->OrderBy('hotel_name','ASC')
            ->get();
        // dd($data_list_hotel);

        $data_total_facility =DB::table('master_facility')
            ->join('hotel_facility','hotel_facility.id_facility','=','master_facility.id')
            ->select('master_facility.facility_name','master_facility.id','hotel_facility.id_hotel')
            ->get();

        $total_facility=print_r($data_total_facility,true);

        $data_list_facility =DB::table('master_facility')
            ->select('master_facility.facility_name','master_facility.id')
            ->get();

            // dd($data_list_hotel);
        $book = array();
        foreach($data_list_hotel as $x){
            // dd($x->id_from_mg);
            $book[] = DB::table('booking_hotel')
                    ->select('*')
                    ->where('mg_id_hotel',$x->id_from_mg)
                    ->count();
        }

        $counter = array();
        foreach($data_list_hotel as $x){
            // dd($x->id_from_mg);
            $counter[] = DB::table('hotel')
                    ->select('view_count')
                    ->where('id_from_mg',$x->id_from_mg)
                    ->first();
        }

        // echo $counter[0]->view_count;
        // die;
        // dd($counter);
        // dd($data_list_hotel->mg_id_hotel);
       
                // dd($book);

        /*print_r($data_hotel_facility);
        die;*/
        $data_total_hotel = DB::table('hotel')
          /*  ->where('city_code','=','2')*/
            ->count();
        $hotel_star_1= DB::table('hotel')
            ->where('rating','=','1')
            ->count();
        $hotel_star_2= DB::table('hotel')
            ->where('rating','=','2')
            ->count();
        $hotel_star_3= DB::table('hotel')
            ->where('rating','=','3')
            ->count();
        $hotel_star_4= DB::table('hotel')
            ->where('rating','=','4')
            ->count();
        $hotel_star_5= DB::table('hotel')
            ->where('rating','=','5')
            ->count();
        //return view('govihar.products',compact('data_total_hotel','data_hotel_facility','data_list_hotel'));
        //return view('frontend.list-hotel',compact('data_total_hotel','data_hotel_facility','data_list_hotel'));
        return view('citytour.all_hotels_list',compact('data_total_hotel','data_list_facility','data_list_hotel','hotel_star_1','hotel_star_2','hotel_star_3','hotel_star_4','hotel_star_5','total_facility'))->with('session_user',$sessionlogin)->with('sumBook',$book)->with('counter',$counter);
    }

        function list_hotel_order_by($orderBy)
    {   
        $sessionlogin=session('login_user');
       $data_list_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            // ->join('master_picture','master_picture.id_hotel','hotel.id')
            ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address')
            // ->where('master_picture.size_type','=',1)

            ->OrderBy('rating',$orderBy)
            ->get();
        // dd($data_list_hotel);

        $data_total_facility =DB::table('master_facility')
            ->join('hotel_facility','hotel_facility.id_facility','=','master_facility.id')
            ->select('master_facility.facility_name','hotel_facility.id_hotel')
            ->get();

        $total_facility=print_r($data_total_facility,true);

        $data_list_facility =DB::table('master_facility')
            ->select('master_facility.facility_name')
            ->get();

        /*print_r($data_hotel_facility);
        die;*/
        $data_total_hotel = DB::table('hotel')
          /*  ->where('city_code','=','2')*/
            ->count();
        $hotel_star_1= DB::table('hotel')
            ->where('rating','=','1')
            ->count();
        $hotel_star_2= DB::table('hotel')
            ->where('rating','=','2')
            ->count();
        $hotel_star_3= DB::table('hotel')
            ->where('rating','=','3')
            ->count();
        $hotel_star_4= DB::table('hotel')
            ->where('rating','=','4')
            ->count();
        $hotel_star_5= DB::table('hotel')
            ->where('rating','=','5')
            ->count();
        //return view('govihar.products',compact('data_total_hotel','data_hotel_facility','data_list_hotel'));
        //return view('frontend.list-hotel',compact('data_total_hotel','data_hotel_facility','data_list_hotel'));
        return view('citytour.all_hotels_list',compact('data_total_hotel','data_list_facility','data_list_hotel','hotel_star_1','hotel_star_2','hotel_star_3','hotel_star_4','hotel_star_5','total_facility'))->with('session_user',$sessionlogin);
    }
    function price_starting_from(){
        $harga_termurah = DB::table('master_room')
            ->select('master_room.net_price')
            ->where('master_room.id_hotel','=',40)
            ->where('master_room.id_bed_room','=',1)
            ->first();
        DB::table('master_room')
            ->insert(['id_hotel' =>12, 'room_name' => "tes sekali lagi", 'bf_type' => 1, 'net_price' => 12,'GrossPrice'=>12 ,'CommPrice'=>12 ,'code_room'=>12  ,'child_min_age' =>0,'child_max_age' =>0,'created_at' =>'2017-12-04 14:43:31', 'last_update' => '2017-12-04 14:43:31','created_by'=>'bot','id_bed_room'=>3]);
        return $harga_termurah->net_price*13519;
    }
    function detail_room_hotel($id_hotel,$room_type)
    {
        $data_detail_room = DB::table('hotel')
            ->join('master_room','master_room.id_hotel', '=', 'hotel.id')
            ->join('master_food', 'master_food.id', '=', 'master_room.bf_type')
            ->join('room_bed', 'room_bed.id', '=', 'master_room.id_bed_room')
            ->select('hotel.hotel_name','master_room.room_name','master_food.type_name','room_bed.room_type')
            ->where('hotel.id_from_mg',$id_hotel)
            ->where('room_bed.room_type',$room_type)
            ->get();
        return $data_detail_room;

    }
    function agent_signin(Request $request){
            $cruds = new AgentModel();
            $username=$request->inputUsernameEmail;
            $password=md5($request->inputPassword);
            //echo $username.$password;
            $log_in = DB::table('agent_member')
            ->select('agent_member.*')
            ->where('username','=',$username)
            ->where('password','=',$password)
            ->first();
            //print_r($log_in);
            if(!empty($log_in)){
                $data=array(        
                    'username' =>$log_in->username,
                    'id_agent' =>$log_in->id_agent,
                    'id_member' => $log_in->id,
                    'akses' => $log_in->akses
                );
                session(['login_user'=>$data]);
            
                return redirect()->route('agent.home');
            }else{
                $gagal='<div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Invalid username / password</strong>
                </div>';
                session(['gagal'=>$gagal]);
                return redirect('/');
                //return 'gagal';
            }
    }
    function agent_home(Request $request){
        // return 'work';
        session_start();
        $sessionlogin=session('login_user');
        $username=$sessionlogin["username"];


        $agent = DB::table('agent_member')
            ->select('agent_account_number','id_agent','akses','username','fullname')
            ->where('username',$username)
            ->first();

        $balance = DB::table('agent_balance')
            ->select('*')
            ->where('created_by','system')
            ->where('id_agent',$agent->id_agent)
            ->get();

            // dd($balance);
        // dd($agent);
            // foreach ($agent_account as $x){ echo $x->agent_account_number; }
            // dd($agent_account);
            // echo $agent->agent_account_number;
            // die;
            // print_r($agent_account);
            
        // dd($agent_account);
        // if($username)
        $bookings = DB::table('booking_hotel')
            ->join('hotel','hotel.id_from_mg','=','booking_hotel.mg_id_hotel')
            ->select('booking_hotel.*','hotel.hotel_name')
            ->where('agent_account_number','=',$agent->agent_account_number)
            ->orderBy('booking_hotel.date_booking', 'desc')
            ->paginate(5);

            // dd($bookings);

        $data_agent=DB::table('agent')
            ->join('agent_detail','agent_detail.agent_id', '=', 'agent.id_agent')
            ->select('agent.*','agent_detail.*')
            ->where('agent.id_agent','=',$agent->id_agent)
            ->get();
        // dd($data_agent);

        if(empty($sessionlogin)){
           return view('citytour.home');
            //return 'login dulu';
        }
        $url=url('/');
        $js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
            <script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
            <script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
            <script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
            <script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>   
            <script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>  
            <script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script> 
            <script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>   
            <script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>    
            <script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script> 
            <script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>   
            <script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script> 
            <script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>      
            <script>
            $(function() {
                    var table = $(\"#topup\").DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: \"".$url."/agent/topup/data\",
                        columns: [
                            { data: 'DT_Row_Index'},
                            { data: 'date_transaction'},
                            { data: 'top_down'},
                            { data: 'top_up'},
                            { data: 'status'},
                            { data: 'date_confirm'},
                            { data: 'created_by'},
                            { data: 'balance_saldo'}
                        ],
                    });
                });
                
        </script>
        <script>
            $(function() {
                    var table = $(\"#member\").DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: \"".$url."/agent/member/data\",
                        columns: [
                            {data: 'DT_Row_Index', orderable: false, searchable: false},
                            { data: 'fullname'},
                            { data: 'username'},
                            { data: 'password'}
                        ],
                    });
                });
                
        </script>";
        $css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
            <link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
            <link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
            <link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
            <link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
            <link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
            <link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
            <link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";

       return view('citytour.agent_area',compact('data_agent','bookings','agent','balance'))
            ->with('session_user',$sessionlogin)
            ->with('js',$js)
            ->with('css',$css);
       // return 'oke';
           }

    public function data_topup_show(Request $request){
       
        // echo $id;
        // die;
       // if($request->ajax()){
         $sessionlogin=session('login_user');
        $id = $sessionlogin['id_agent'];
        // dd($id);
        //$agent = AgentModel::select(['*'])->get();
        // DB::statement(DB::raw('set @nomor=0 '));
        $agent = DB::table('agent_balance')
                    ->join('agent', 'agent.id_agent', '=', 'agent_balance.id_agent')
                    // ->join('agent_member','agent_member.id','=','agent_balance.id_member')
                    ->select('agent.*','agent_balance.*')
                    ->where('agent_balance.id_agent',$id)
                    ->orderBy('agent_balance.date_transaction', 'DESC')
                    // ->where('agent_balance.top_down',null)
                    // DB::raw('@nomor := @nomor + 1 as no')
                    ->get();
        // dd($agent);
        return Datatables::of($agent)
                ->addColumn('status',function ($agent) { return
                    $agent->status == 1 ? '<label class="label label-success"> Added </label>' : ($agent->status == 2 ? '<label class="label label-warning">processing</label>' : ($agent->status == 3 ? '<label class="label label-info">Refund</label>' : ($agent->status == 4 ? '<label class="label label-danger">Aborted</label>' : ($agent->status_topdown == 1 ? '<label class="label label-primary"> Booked </label>' : ($agent->status_topdown == 2 ? '<label class="label label-warning">Calcel Booking</label>' : '<label class="label label-danger">wrong')))));
                })
                ->editColumn('balance_saldo', function ($agent) {
                    return 'Rp.'.number_format($agent->balance_saldo, 0);
                     })
                ->editColumn('top_up', function ($agent) {
                    return  $agent->top_up > 0 ? 'Rp.'.number_format($agent->top_up, 0) : '-';
                     })
                ->editColumn('top_down', function ($agent) {
                    return  $agent->top_down > 0 ? 'Rp.'.number_format($agent->top_down, 0)  : '-';
                     })
                // ->orderColumn('date_transaction', '-date_transaction $1')
                ->addIndexColumn()
                ->make(true);
        // } else {
        //     exit("Not an AJAX request -_-");
        // }
    }
     public function data_topdown_show(Request $request){
       
        // echo $id;
        // die;
       // if($request->ajax()){
         $sessionlogin=session('login_user');
        $id_agent = $sessionlogin['id_agent'];
        $id_member = $sessionlogin['id_member'];
        //$agent = AgentModel::select(['*'])->get();
        // DB::statement(DB::raw('set @nomor=0 '));
        // $id_kor = DB::table('agent_detail')
        //         ->select('agent_account_number')
        //         ->where('agent_id',$id)
        //         ->first();
        // // dd($id_kor);
        // $ini = $id_kor->agent_account_number;

        // $agent = DB::table('agent_balance')
        //             ->join('agent_detail', 'agent_detail.agent_id', '=', 'agent_balance.id_agent')
        //             ->join('booking_hotel','booking_hotel.agent_account_number', '=', 'agent_detail.agent_account_number')
        //             ->select('agent_balance.*','agent_detail.*','booking_hotel.*')
        //             ->where('agent_balance.id_agent',$id)
        //             ->where('agent_balance.top_up',null)
        //             ->where('booking_hotel.agent_account_number',$ini)
        //             ->get();
        $agent = DB::table('agent_balance')
                ->join('agent_member','agent_member.id','=','agent_balance.id_agent')
                ->select('agent_member.*','agent_balance.*')
                ->where('agent_balance.id_member','=',$id_member)
                ->where('agent_balance.top_up',null)
                ->get();
        // dd($agent);
        return Datatables::of($agent)
                ->editColumn('balance_saldo', function ($agent) {
                    return 'Rp.'.number_format($agent->balance_saldo, 0);
                     })
                ->editColumn('top_down', function ($agent) {
                    return 'Rp.'.number_format($agent->top_down, 0);
                     })
                ->addIndexColumn()
                ->make(true);
        // } else {
        //     exit("Not an AJAX request -_-");
        // }
    }
    public function data_member_show(){
        $sessionlogin=session('login_user');
        $id = $sessionlogin['id_agent'];
        //$agent = AgentModel::select(['*'])->get();

        $member = DB::table('agent_member')
                    ->select('id','fullname','username','password')
                    ->where('id_agent',$id)
                    ->where('akses',0)
                    ->get();
        // dd($member);
        
        // dd($no);
        return Datatables::of($member)
                ->addIndexColumn()
                // ->editColumn('no', function ($member) {
                //             return array('1','2');
                //              })
                ->make(true);
    }
    function register(Request $request){
        $this->validate($request, [
            'fullname'           => 'required',
            'username'           => 'required|unique:agent_member,username',
            'email'              => 'required|unique:agent_detail,agent_email',
            'telephone'           => 'required',
            'address'           => 'required',
            'country'           => 'required',
            'city'           => 'required',
            'password'           => 'required'
        ]);

        $agent = new AgentModel();
        $agent_detail = new AgentDetailModel();
        $agent_member = new AgentMemberModel();

        $salt = str_random(10);
        $token = bcrypt($request->username.$salt);
        // dd($token);
         $list = AgentMemberModel::select('agent_account_number')
                    ->orderBy('id','desc')
                    ->get();
                $id=count($list);
            $string = "KG-AG";
            $month = date('m');
            $year = date('y');
            $temp = (int) $id;
            $temp = $temp+1; 
            $number = sprintf("%04s", $temp);
            $account_number = $string.$month.$year.'-'.$number;

            // dd($account_number);
        $agent->agent_name = $request->fullname;
        $agent->username = $request->username;
        $agent_member->username = $request->username;
        $agent_member->password = md5($request->password);
        $agent_member->fullname = $request->fullname;
        $agent_member->agent_account_number = $account_number;
        $agent_member->akses = 1;
        $agent->salt = $salt;
        $agent->token = $token;
        
        if($agent->save() ){
          $last_id_agent = $agent->id_agent;
           //create agent account number
            $list = AgentDetailModel::select('agent_account_number')
                    ->orderBy('id','desc')
                    ->get();
                    $id=count($list);

            $string = "KG-AG";
            $month = date('m');
            $year = date('y');
            $temp = (int) $id;
            $temp = $temp+1; 
            $number = sprintf("%04s", $temp);
            $account_number = $string.$month.$year.'-'.$number;
            //echo $account_number;
            $agent_member->id_agent = $last_id_agent;
            $agent_member->save();

          $agent_detail->agent_account_number   = $account_number;
          $agent_detail->agent_email            = $request->email;
          $agent_detail->agent_address          = $request->address;
          $agent_detail->agent_telephone        = $request->telephone;
          $agent_detail->agent_id               = $last_id_agent;
          $agent_detail->agent_date_register    = date('Y-m-d H:i:s');
          $agent_detail->agent_country          = $request->country;
          $agent_detail->agent_city             = $request->city;
          if($agent_detail->save()){
            return redirect('/register')->with('alert-success', 'Data Berhasil Disimpan.');
          }else{
            return redirect('/register')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
          }
        }

        // $xx = $agent->id_agent;
        // echo $xx;
        // $listAgent = AgentModel::select('id_agent')
        //         ->orderBy('id_agent', 'desc')
        //         ->get();
        // //echo $listAgent->agent_id;
        // foreach ($listAgent as $list){
        //     echo $list->id_agent."<br>";
        // }
        // echo "hello";
        //$pos=Post::all();
        // $agent->username = $request->email;
        // $agent->password = $request->telephone;

        // $agent-> = $request->address;
        // $agent->id_agent = $request->country;
        // $agent->id_agent = $request->city;
        
    }

    public function top_up(Request $request){
        $cruds = new AgentBalanceModel();

        $id_agent = $request->id_agent;
        $id_member = $request->id_member;
        $bank = $request->bank;
        $name = $request->pemilik;
        $pesan = $request->pesan;

        $note = $bank.','.$name.','.$pesan;
        $top_up = $request->top_up;
        $image = $request->file('file');
        
         $foldername = $id_agent.'_';
        // echo $id_agent.$top_up.$image.$note.$request->username;
        // echo "<br>".$note;
        // echo "<br>".$id_agent;
        // echo "<br>".$bank;
        // echo "<br>".$request->username;
         // echo $image;

        //save destination picture file
        $filename = $image->getClientOriginalName();
        $namatok = pathinfo($filename, PATHINFO_FILENAME);
        $extension = $image->getClientOriginalExtension();
        $picture = $namatok.'_'.sha1($filename . time()) . '.' . $extension;
        $destinationPath = public_path('img/topup/'.$foldername);
        $image->move($destinationPath, $picture);

         //$image->move(public_path().'images/topup/'.$foldername.'/', $request->file('file')->getClientOriginalName());

        $cruds->id_agent    = $id_agent;
        $cruds->id_member   = $id_member;
        $cruds->top_up      = $top_up;
        //$cruds->top_down    = 
        $cruds->date_transaction = date('Y-m-d H:i:s');
        $cruds->note        = $note;
        $cruds->picture     = $picture;
        $cruds->status      = '2';
        $cruds->save(); 
        return redirect('/agent')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    public function create_member(Request $request){
         $this->validate($request, [
            'fullname'           => 'required',
            'username'           => 'required|unique:agent_member,username',
            'password'           => 'required'
        ]);
         $crud = new AgentMemberModel();
          $list = AgentMemberModel::select('agent_account_number')
                    ->orderBy('id','desc')
                    ->first();

            $string = "KG-AG";
            $month = date('m');
            $year = date('y');
            $temp = (int) substr($list->agent_account_number,10,4);
            $temp = $temp+1; 
            $number = sprintf("%04s", $temp);
            $account_number = $string.$month.$year.'-'.$number;

            // dd($account_number);

         $crud->fullname = $request->fullname;
         $crud->username = $request->username;
         $crud->id_agent = $request->id_agent;
         $crud->password = md5($request->password);
         $crud->agent_account_number = $account_number;
         $crud->akses = 0;

         if($crud->save()){
            return redirect('/agent#')->with('alert-success', 'Data Berhasil Disimpan.');
         } 
    }

    public function hotelFinish($id_res){
        session_start();
        $sessionlogin=session('login_user');
        $id = $sessionlogin['id_agent'];

        $summary = DB::table('booking_hotel')
                ->join('hotel','hotel.id_from_mg','booking_hotel.mg_id_hotel')
                ->select('booking_hotel.*','hotel.*')
                ->where('booking_hotel.mg_os_ref_no',$id_res)
                ->first();
                // dd($summary);
        $balance = DB::table('agent')
                ->select('last_balance')
                ->where('id_agent',$id)
                ->first();
                // dd($balance);
        return view('citytour.finish_payment_hotel')->with('summary',$summary)->with('balance',$balance)->with('session_user',$sessionlogin);
    }

   public function autocomplete(Request $request)
    {
        $data = HotelModel::select("hotel_name")->where("hotel_name","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
    }

}
