<?php

namespace App\Http\Controllers;
// use DOMDocument;


date_default_timezone_set("Asia/Jakarta");
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class APIHotel_Korina extends Controller
{
    function CURL_jsonDataRequest(){
        $data = array("HotelCode" => "KORTHBK-0002", "DestinationCode" => "","Limit"=>"");
        $data_string = json_encode($data);
        $ch = curl_init('http://localhost/korinahotel/public/api/ServicesPHP/APIS_getDetailHotelFinal');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization:Bearer $2y$10$yNeC8wZW1yQWaE0vIfq9kuy7xCo99suhQ7gjsYI0L9vutb6MDM4jy',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);
        echo $result;
    }
    function CURL_jsonDataRequest2()
    {
        $data = array("HotelCode" => "KORTHBK-0001", "DestinationCode" => "","Limit"=>"");
        $data_string = json_encode($data);
        $ch = curl_init('http://localhost/korinahotel/public/api/ServicesPHP/APIS_getDetailHotelFinal');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: application/json',
                'Authorization:Bearer $2y$10$yNeC8wZW1yQWaE0vIfq9kuy7xCo99suhQ7gjsYI0L9vutb6MDM4jy',
                'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
    }

    function CURL_jsonDataRequest3()
    {
        $data = array("HotelCode" => "WSMA0511000127", "CheckIn" => "2018-01-19","CheckOut"=>"2018-01-20","Adult"=>"2","Room"=>"1");
        $data_string = json_encode($data);
        // echo $data_string;die;
        $ch = curl_init('http://192.168.100.15/korinahotel/public/api/testing/search_agent');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: application/json',
                'Content-Type:application/json',
                'Authorization:Bearer $2y$10$yNeC8wZW1yQWaE0vIfq9kuy7xCo99suhQ7gjsYI0L9vutb6MDM4jy',
                'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        return $result;

    }

    function APIS_getDetailHotelFinal(Request $request)
    {
        $errorMessage=array
        (
            array("Method not allowed. Only the POST method we accept",405),
            array("Authorization not match",401),
            array("Wrong hotel code parameter",400),
            array("Problem get all hotel detail",400),
            array("Wrong destination code parameter",400),
            array("Limit length range 1-100",411)
        );
        $useParam;
        $method = $request->method();
        if($method!="POST")
        {
            $useParam=0;
            goto jsonErrorMessage;
            die;
        }
        $hotelCode=$request->HotelCode;
        $DestinationCode=$request->DestinationCode;
        $limit=$request->Limit;
        $header = $request->header('Authorization');
        $token = str_replace("Bearer ","",$header);
        $agent = DB::table('agent')
            ->select('token')
            ->where('token',$token)
            ->first();
        if ($agent==null){
            $useParam=1;
            goto jsonErrorMessage;
            die;
        }
        if(!empty($hotelCode))
        {
            $whereCondition="id_to_agent = '".$hotelCode."'";
            $useParam=2;
        }
        else
        {
            if(empty($DestinationCode)) {
                $whereCondition = "TRUE";
                $useParam=3;
            }
            else{
                $whereCondition = "master_city.code_city_to_agent ='".$DestinationCode."'";
                $useParam=4;
            }
        }
        if($limit==null){
            // limit default 15 and maximum 100
            $limit=15;
        }
        elseif($limit>15)
        {
            $useParam=5;
            goto jsonErrorMessage;
            die;
        }
        elseif($limit<0)
        {
            $useParam=5;
            goto jsonErrorMessage;
            die;
        }
        $page="1";
        $json_merge="";
        $data_hotel = DB::table('hotel')
            ->select('hotel.*','master_city.city_name','master_country.country_name','master_region.region_name', 'hotel_detail.hotel_room', 'hotel_detail.hotel_address', 'hotel_detail.hotel_location', 'hotel_detail.hotel_faximile', 'hotel_detail.hotel_email', 'hotel_detail.hotel_website')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            ->join('master_city','master_city.id','=','hotel_detail.city_code')
            ->join('master_country','master_country.id','=','hotel_detail.country_code')
            ->join('master_region','master_region.id','=','hotel_detail.region_code')
            ->whereRaw($whereCondition)
            ->orderBy('hotel.id')
            ->limit($limit)
            ->get();
        $totalHotel = count($data_hotel);
        if($totalHotel==0)
        {
            goto jsonErrorMessage;
            die;
        }
        $isPage=0;
        $loop=0;
        foreach($data_hotel as $list_dataHotel)
        {
            $facilities_merge="";
            $data_hotelFacilities = DB::table('master_facility')
                ->join('hotel_facility', 'hotel_facility.id_facility', '=', 'master_facility.id')
                ->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
                ->select('facility_name')
                ->where('hotel.id', '=', $list_dataHotel->id)
                ->get();
            $total_hotelFacilities=count($data_hotelFacilities);
            if(empty($total_hotelFacilities)){
                echo "";
            }
            else{
                foreach($data_hotelFacilities as $list_dataHotelFacilities){
                    $facilities_merge.='{
				"ListFacility": {
					"Name": "'.$list_dataHotelFacilities->facility_name.'"
				}
			},';
                }
            }
            $facilities_merge = rtrim($facilities_merge, ", ");
            $json_merge .= ' {
		"HotelCode": "'.$list_dataHotel->id_to_agent.'",
		"HotelName": "'.$list_dataHotel->hotel_name.'",
		"Rating": "'.$list_dataHotel->rating.'",
		"TotalRooms": "'.$list_dataHotel->hotel_room.'",
		"Address": "'.$list_dataHotel->hotel_address.'",
		"Location": "'.$list_dataHotel->hotel_location.'",
		"City": "'.$list_dataHotel->city_name.'",
		"Country": "'.$list_dataHotel->country_name.'",
		"Region": "'.$list_dataHotel->region_name.'",
		"Email": "'.$list_dataHotel->hotel_email.'",
		"Faximile": "'.$list_dataHotel->hotel_faximile.'",
		"Website": "'.$list_dataHotel->hotel_website.'",
		"Latitude": "'.$list_dataHotel->latitude.'",
		"Longitude": "'.$list_dataHotel->longitude.'",
		"Currency":"IDR",
		"StartingPrice":"'.$this->get_CheapestPrice($list_dataHotel->id).'",
		 "Facility": ['.$facilities_merge.']
			},'
            ;
        }
        $json_merge = rtrim($json_merge, ", ");
        return '{"ResponseHotelList":['.$json_merge.']}';
        jsonErrorMessage:
        return response()->json(["Message"=>$errorMessage[$useParam][0]],$errorMessage[$useParam][1]);
/*        return '{"ResponseHotelList":{"Message":"'.$errorMessage[$useParam].'"},404}';*/
    }
    function APIS_getDetailHotelFinalBackup()
    {
        $hotelCode="";
        if(!empty($hotelCode))
        {
            $whereCondition="id_to_agent = '".$hotelCode."'";
        }
        else
        {
            $whereCondition="TRUE";
        }
        $limit="15";
        $page="1";
        $json_merge="";

        $data_hotel = DB::table('hotel')
            ->select('hotel.*', 'hotel_detail.hotel_room', 'hotel_detail.hotel_address', 'hotel_detail.hotel_location', 'hotel_detail.hotel_faximile', 'hotel_detail.hotel_email', 'hotel_detail.hotel_website')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            ->whereRaw($whereCondition)
            ->orderBy('hotel.id')
            ->limit($limit)
            ->get();

        /*        echo "jumlah :".count($data_hotel);

                die;*/
        $totalHotel = count($data_hotel);

        $isPage=0;
        $loop=0;
        foreach($data_hotel as $list_dataHotel)
        {
            //$loop++;
            /*  for($i=$loop++;;$i++)
              {
                  $isPage;
              }*/
            $facilities_merge="";
            $data_hotelFacilities = DB::table('master_facility')
                ->join('hotel_facility', 'hotel_facility.id_facility', '=', 'master_facility.id')
                ->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
                ->select('facility_name')
                ->where('hotel.id', '=', $list_dataHotel->id)
                ->get();
            $total_hotelFacilities=count($data_hotelFacilities);
            if(empty($total_hotelFacilities)){
                echo "";
            }
            else{
                foreach($data_hotelFacilities as $list_dataHotelFacilities){
                    $facilities_merge.='{
				"ListFacility": {
					"Name": "'.$list_dataHotelFacilities->facility_name.'"
				}
			},';
                }
            }
            $facilities_merge = rtrim($facilities_merge, ", ");
            $json_merge .= ' {
		"HotelCode": "'.$list_dataHotel->id_to_agent.'",
		"HotelName": "'.$list_dataHotel->hotel_name.'",
		"Rating": "'.$list_dataHotel->rating.'",
		"TotalRooms": "'.$list_dataHotel->hotel_room.'",
		"Address": "'.$list_dataHotel->hotel_address.'",
		"Location": "'.$list_dataHotel->hotel_location.'",
		"Email": "'.$list_dataHotel->hotel_email.'",
		"Faximile": "'.$list_dataHotel->hotel_faximile.'",
		"Website": "'.$list_dataHotel->hotel_website.'",
		"Latitude": "'.$list_dataHotel->latitude.'",
		"Longitude": "'.$list_dataHotel->longitude.'",
		"Currency":"IDR",
		"StartingPrice":"'.$this->get_CheapestPrice($list_dataHotel->id).'",
		 "Facility": ['.$facilities_merge.']
			},'
            ;
        }
        $json_merge = rtrim($json_merge, ", ");
        return '{"ResponseHotelList":['.$json_merge.']}';

    }




    function APIS_getDetailHotel2()
    {
       /* $fasilitashotel = DB::table('hotel_facility')
            ->join('master_facility', 'master_facility.id', '=', 'hotel_facility.id_facility')
            ->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
            ->select('hotel_facility.*','hotel_facility.*','master_facility.*')
            ->where('hotel.id', '=', $id)
            ->get();*/
       // echo "it works";
        $json_merge="";
        $data_hotel = DB::table('hotel')
            ->select('hotel.*', 'hotel_detail.hotel_room', 'hotel_detail.hotel_address', 'hotel_detail.hotel_location', 'hotel_detail.hotel_faximile', 'hotel_detail.hotel_email', 'hotel_detail.hotel_website')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            ->whereRaw("TRUE")
            ->orderBy('hotel.id')
            ->get();
       // print_r($data_hotel);
  /*      foreach($data_hotel as $list_dataHotel) {
            $fasilitashotel = DB::table('hotel_facility')
                ->join('master_facility', 'master_facility.id', '=', 'hotel_facility.id_facility')
                ->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
                ->select('master_facility.facility_name')
                ->where('hotel.id', '=', $list_dataHotel->id)
                ->get();

            echo $fasilitashotel->facility_name;
        }

        die;*/
        foreach($data_hotel as $list_dataHotel)
        {
          //  echo $list_dataHotel->id."<br/>";
            $facilities_merge="";
            $data_hotel_facilities = DB::table('master_facility')
                ->join('hotel_facility', 'hotel_facility.id_facility', '=', 'master_facility.id')
                ->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
                ->select('facility_name')
                ->where('hotel.id', '=', $list_dataHotel->id)
                ->get();
            $jum=count($data_hotel_facilities);

            if(empty($jum)){

                //echo "[kosong]";
                echo "";
/*                echo "idnya ".$list_dataHotel->id;
                die("sampai sini");*/
            }
            else{

            foreach($data_hotel_facilities as $list_dataHotelFacilities){
  /*              echo $list_dataHotel->id."<br/>";*/
/*
                echo $list_dataHotelFacilities->facility_name."<br/>".count($data_hotel_facilities);*/

/*
                echo $list_dataHotelFacilities->facility_name."<br/>";*/
                $facilities_merge.='{
				"ListFacility": {
					"Name": "'.$list_dataHotelFacilities->facility_name.'"
				}
			},';
            }
              //  echo $facilities_merge."<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";



            }
            $facilities_merge = rtrim($facilities_merge, ", ");

            $json_merge .= ' {
		"HotelCode": "'.$list_dataHotel->id_to_agent.'",
		"HotelName": "'.$list_dataHotel->hotel_name.'",
		"Rating": "'.$list_dataHotel->rating.'",
		"TotalRooms": "'.$list_dataHotel->hotel_room.'",
		"Address": "'.$list_dataHotel->hotel_address.'",
		"Location": "'.$list_dataHotel->hotel_location.'",
		"Email": "'.$list_dataHotel->hotel_email.'",
		"Faximile": "'.$list_dataHotel->hotel_faximile.'",
		"Website": "'.$list_dataHotel->hotel_website.'",
		"Latitude": "'.$list_dataHotel->latitude.'",
		"Longitude": "'.$list_dataHotel->longitude.'",
		"Currency":"IDR",
		"StartingPrice":"'.$this->get_CheapestPrice($list_dataHotel->id).'",
		 "Facility": ['.$facilities_merge.']
			},'
            ;



        }
        $json_merge = rtrim($json_merge, ", ");
        echo "[".$json_merge."]";



    }
    function APIS_getDetailHotel(Request $request)
    {

        $hotelCode = $request->HotelCode;

        $limit = 100;
        $page = 1;
        $header = $request->header('Authorization');
        $token = str_replace("Bearer ","",$header);
        $agent = DB::table('agent')
            ->select('token')
            ->where('token',$token)
            ->first();
        if ($agent==null){
            return response()->json(['msg'=>'Authorization not match']);
        }
        if(!empty($hotelCode))
        {
             $whereCondition="id_to_agent = '".$hotelCode."'";
        }
        else
        {
            $whereCondition="TRUE";
        }

        $data_hotel = DB::table('hotel')
            ->select('hotel.*', 'hotel_detail.hotel_room', 'hotel_detail.hotel_address', 'hotel_detail.hotel_location', 'hotel_detail.hotel_faximile', 'hotel_detail.hotel_email', 'hotel_detail.hotel_website')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            ->whereRaw($whereCondition)

            ->get();
        $json = "";
        $facilities_merge="";
        $json_merge="";
        foreach ($data_hotel as $list_data_hotel) {

            $data_hotel_facilities = DB::table('master_facility')
                ->join('hotel_facility', 'hotel_facility.id_facility', '=', 'master_facility.id')
                ->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
                ->select('facility_name')
                ->where('hotel.id', '=', $list_data_hotel->id)
                ->get();
            $jum=count($data_hotel_facilities);
            $id_hotel = $list_data_hotel->id;

            if(empty($jum)){

                echo "";
            }
            else{

                foreach($data_hotel_facilities as $list_dataHotelFacilities){
                    $facilities_merge.='{
				"ListFacility": {
					"Name": "'.$list_dataHotelFacilities->facility_name.'"
				}
			},';
                }




            }
            $facilities_merge = rtrim($facilities_merge, ", ");

            $json_merge .= ' {
		"HotelCode": "'.$list_data_hotel->id_to_agent.'",
		"HotelName": "'.$list_data_hotel->hotel_name.'",
		"Rating": "'.$list_data_hotel->rating.'",
		"TotalRooms": "'.$list_data_hotel->hotel_room.'",
		"Address": "'.$list_data_hotel->hotel_address.'",
		"Location": "'.$list_data_hotel->hotel_location.'",
		"Email": "'.$list_data_hotel->hotel_email.'",
		"Faximile": "'.$list_data_hotel->hotel_faximile.'",
		"Website": "'.$list_data_hotel->hotel_website.'",
		"Latitude": "'.$list_data_hotel->latitude.'",
		"Longitude": "'.$list_data_hotel->longitude.'",
		"Currency":"IDR",
		"StartingPrice":"'.$this->get_CheapestPrice($list_data_hotel->id).'",
		 "Facility": ['.$facilities_merge.']
			},'
            ;



        }
        $json_merge = rtrim($json_merge, ", ");
        echo "[".$json_merge."]";
        die;

        foreach ($data_hotel as $list_data_hotel) {
            $id_hotel = $list_data_hotel->id;
            $data_hotel_facilities = DB::table('master_facility')
                ->select('facility_name')
                ->join('hotel_facility', 'hotel_facility.id_facility', '=', 'master_facility.id')
                ->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
               // ->where('id_to_agent', '=', $list_data_hotel->id_to_agent)
                ->where('hotel.id', '=', $id_hotel)
                ->get();
           // echo "it works";
            //echo count($data_hotel_facilities);


            foreach ($data_hotel_facilities as $list_data_hotel_facilities) {
               // die($list_data_hotel_facilities->facility_name."<br/>");
                $facilities_merge.='{
				"ListFacility": {
					"Name": "'.$list_data_hotel_facilities->facility_name.'"
				}
			},';
                /*die($facilities_merge);*/
                }


            $facilities_merge = rtrim($facilities_merge, ", ");
            //die($facilities_merge);
                $json_merge .= ' {
		"HotelCode": "'.$list_data_hotel->id_to_agent.'",
		"HotelName": "'.$list_data_hotel->hotel_name.'",
		"Rating": "'.$list_data_hotel->rating.'",
		"TotalRooms": "'.$list_data_hotel->hotel_room.'",
		"Address": "'.$list_data_hotel->hotel_address.'",
		"Location": "'.$list_data_hotel->hotel_location.'",
		"Email": "'.$list_data_hotel->hotel_email.'",
		"Faximile": "'.$list_data_hotel->hotel_faximile.'",
		"Website": "'.$list_data_hotel->hotel_website.'",
		"Latitude": "'.$list_data_hotel->latitude.'",
		"Longitude": "'.$list_data_hotel->longitude.'",
		"Currency":"IDR",
		"StartingPrice":"'.$this->get_CheapestPrice($id_hotel).'",
		 "Facility": ['.$facilities_merge.']
			},'
		 ;
            if(!empty($hotelCode))
            {
                goto single;
            }
            else
            {
                goto all;
            }
            single:
            $json_final = rtrim($json_merge, ", ");
            return '{"APIS_getDetailHotel_Response":'.$json_final."}";
            die;
            all:

        }
        $json_final = rtrim($json_merge, ", ");
        return '{"APIS_getDetailHotel_Response":'.$json_final."'}'";
    }
    public static function get_CheapestPrice($idHotel){
        $harga_termurah = DB::table('master_room')
            ->select('master_room.net_price')
            ->where('master_room.id_hotel','=',$idHotel)
            ->where('master_room.id_bed_room','=',1)
            ->first();
        //*13519
        return $harga_termurah->net_price*13428.37;
    }
    function APIS_SearchHotelByDestination($dewasa,$cekin,$cekout,$mg_code,$kamar,$sumChild,$ageJoin)
    {
        //$mg_code='WSASTHBKK000087';
        $kodeNegara='WSASTH';
        $kodeKota=$mg_code;
        $checkIn=$cekin;
        $checkOut=$cekout;
        $jumlahDewasa=$dewasa;
        $jumlah_kamar=$kamar;
        $jumlahAnak=$sumChild;
        $umurAnak=$ageJoin;

        // $ini = $jumlahAnak."->".$umurAnak;
    // dd($umurAnak);
        $UmurArray = explode("~",$umurAnak);
        // dd($toArray);


        $hasil_tipe_kamar=ceil($jumlahDewasa/$jumlah_kamar);
        // dd($hasil_tipe_kamar);
        /*$if($jumlahDewasa/$jumlah_kamar=1)
        {

        }*/
        $tipeKamar="";
        /*        echo $id_hotel." ".$kodeNegara." ".$kodeKota." ".$checkIn." ".$checkOut." ".$jumlahDewasa."<br/>";
                return;*/
        /*echo "masuk get getAllRoomToDB dulu baru balek<br/><br/><br/><br/>";*/
        $timeNOW = \Carbon\Carbon::now();
        $country_code=DB::table('master_country')->select('id')->where('mg_country_code', '=', $kodeNegara)->first();
        $country_code=$country_code->id;
        $city_code=DB::table('master_city')->select('id')->where('mg_city_code', '=', $kodeKota)->first();
        $city_code=$city_code->id;

        $date1=date_create($checkIn);
        $date2=date_create($checkOut);
        $diff=date_diff($date1,$date2);
        $periodeStay=print_r($diff,true);
        preg_match('/\[da.*?>\s(.*?)\s/',$periodeStay,$hasil);
        //echo $hasil[1];

         $ini = $jumlah_kamar * 2;
        $roomKosong = ($ini - $jumlahAnak) / 2;

        $loop = $jumlah_kamar - $roomKosong;
        
        // $xx = $jumlahAnak/2;
        for($b=0,$JoinBro="";$b<$jumlahAnak;$b++){ 
            $JoinBro .= '<ChildAge>'.$UmurArray[$b].'</ChildAge>';
        }
        
        $ulang = $jumlahAnak/2;

        // $x1= substr($JoinBro, 0,44);
        // $x2= substr($JoinBro, 45,88);
        // dd($roomKosong);
        // echo $x1;
        // die;
        // dd($x2);
        // dd(substr($JoinBro,44,88));   
        // for($a)

        if ($jumlahAnak == 1){
           for($b=0;$b<$jumlahAnak;$b++){ 
                $childJoin[$b]='<ChildAges><ChildAge>'.$umurAnak[$b].'</ChildAge></ChildAges>';
           }
        }

        // dd($JoinBro);

        if ($jumlahAnak > 1){ 
            for($b=0,$aa=1;$b<$jumlahAnak,$aa<$jumlahAnak;$b++,$aa++){ 
              // if($b % 2 == 0 and $aa % 2 == 0){
              //   echo $b."genap".$aa;
              // }else{
              //   echo $b."ganjil".$aa;
              // }
                if ($aa==0 or $b==1){
                continue;
            } 
                if($UmurArray[$b] <= 9 and $UmurArray[$aa] <= 9){
                    // echo "<br>satu".$UmurArray[$b];
                    // echo "<br>satu".$UmurArray[$aa].$ulang;
                    $xx=0;
                    $yy=44;
                    for ($c=0;$c<$ulang;$c++){

                            $x[] = substr($JoinBro,$xx,$yy);
                             $xx=$xx+44;
                        $yy=$yy+44;

                        // $xx=$xx+44;
                        // $yy=$yy+44;
                    }

                    for($a=0;$a<$loop;$a++){ 
                        $childJoin[$a]='<ChildAges>'.$x[$a].'</ChildAges>';
                    }

                }elseif( ($UmurArray[$b] >= 10 and $UmurArray[$aa] <= 9) or ($UmurArray[$b] <= 9 and $UmurArray[$aa] >= 10) ){
                    // echo "<br>dua".$UmurArray[$b];
                    // echo "<br>dua".$UmurArray[$aa].$ulang;
                    $xx=0;
                    $yy=45;
                    for ($c=0;$c<$ulang;$c++){

                            $x[] = substr($JoinBro,$xx,$yy);

                            $xx=$xx+45;
                        $yy=$yy+45;

                        // $xx=$xx+45;
                        // $yy=$yy+45;
                    }

                    for($a=0;$a<$loop;$a++){ 
                        $childJoin[$a]='<ChildAges>'.$x[$a].'</ChildAges>';
                    }
                }elseif( ($UmurArray[$b] >= 10 and $UmurArray[$aa] >= 10) or ($UmurArray[$b] >= 10 and $UmurArray[$aa] >= 10) ){
                    // echo "<br>tiga".$UmurArray[$b];
                    // echo "<br>tiga".$UmurArray[$aa].$ulang;
                    $xx=0;
                    $yy=46;
                    for ($c=0;$c<$ulang;$c++){

                            $x[] = substr($JoinBro,$xx,$yy);
                            $xx=$xx+46;
                        $yy=$yy+46;

                        // $xx=$xx+46;
                        // $yy=$yy+46;
                    }

                    for($a=0;$a<$loop;$a++){ 
                        $childJoin[$a]='<ChildAges>'.$x[$a].'</ChildAges>';
                    }
                }
            }      
            // if ($aa==2 or $b==1){
            //     continue;
            // }   
        }


            for($a=0,$roomBook="";$a<$loop;$a++){ 
                for($i=0;$i<1;$i++){
                    $roomBook.='<RoomInfo>
                                    <AdultNum RoomType="" RQBedChild="N">'.$hasil_tipe_kamar.'</AdultNum>
                                    '.$childJoin[$a].'
                                </RoomInfo>';
                    if ($roomKosong>0){
                        for($i=0;$i<intval($roomKosong);$i++){
                            $roomBook.='<RoomInfo>
                                    <AdultNum RoomType="" RQBedChild="N">'.$hasil_tipe_kamar.'</AdultNum>
                                </RoomInfo>';
                        }
                    }
                }  
            } 


//room kosong berapa ??
            // dd($roomKosong);
            // for($a=0,$roomBook="";$a<$roomKosong;$a++){ 


            //         $roomBook.='<RoomInfo>
            //                         <AdultNum RoomType="" RQBedChild="N">'.$hasil_tipe_kamar.'</AdultNum>
            //                         '.$childJoin[$a].'
            //                     </RoomInfo>';
                

            // } 
            // $yuhu = intval($roomKosong);
            // dd($yuhu);

        // dd($roomBook);

        // if ($jumlahAnak > 0){
        //     for ($a=0, $JoinBro=""; $a<$jumlahAnak; $a++){
        //             if($a==2){
        //                 break;
        //             }
        //                  $JoinBro .= '<ChildAge>'.$UmurArray[$a].'</ChildAge>';
        //         }
        //         $childJoin='<ChildAges>'.$JoinBro.'</ChildAges>';
        //         // dd($jumlahDewasa);
        //     //<AdultNum RoomType="' . $tipeKamar . '" RQBedChild="N">' . $jumlahDewasa . '</AdultNum>
        //     for($i=0,$roomBook="";$i<1;$i++){


        //             $roomBook.='<RoomInfo>
        //                             <AdultNum RoomType="" RQBedChild="N">'.$hasil_tipe_kamar.'</AdultNum>
        //                             '.$childJoin.'
        //                         </RoomInfo>';
                
        //     }

            
        
        // }
        //tanpa anak
        if ($jumlahAnak == 0){
            for($i=0,$roomBook="";$i<$jumlah_kamar;$i++){
                    $roomBook.='<RoomInfo>
                                    <AdultNum RoomType="" RQBedChild="N">'.$hasil_tipe_kamar.'</AdultNum>
                                </RoomInfo>';
            }
        }
        //$roomBook2="";
        //$roomBook2=$roomBook;

        // dd($roomBook);
        /*<RoomInfo>
<AdultNum RoomType="Twin" RQBedChild="N">2</AdultNum>
</RoomInfo>
<RoomInfo>
<AdultNum RoomType="Twin" RQBedChild="N">2</AdultNum>
</RoomInfo>*/
        /*<RoomInfo>
<AdultNum RoomType="' . $tipeKamar . '" RQBedChild="N">2</AdultNum>
</RoomInfo>*/

        $datah='<RoomInfo><AdultNum RoomType="Twin" RQBedChild="N">2</AdultNum></RoomInfo>';


        $input_xml = '<?xml version="1.0" encoding="utf-8" ?>
<Service_SearchHotel>
<AgentLogin>
<AgentId>BMG1</AgentId>
<LoginName>BMG1XML</LoginName>
<Password>BMG12017</Password>
</AgentLogin>
<SearchHotel_Request>
<PaxPassport>WSASKR</PaxPassport>
<DestCountry>' . $kodeNegara . '</DestCountry>
<DestCity>' . $kodeKota . '</DestCity>
<ServiceCode/>
<Period checkIn="' . $checkIn . '" checkOut="' . $checkOut . '" />
'.$roomBook.'
<flagAvail/>
</SearchHotel_Request>
</Service_SearchHotel>';
// dd($input_xml);
        $url = "http://xml.travflex.com/11WS_SP2_1/ServicePHP/SearchHotel.php";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "requestXML=" . $input_xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);
      //   dd($data);
      // Header('Content-type: text/xml');
      //  print($data);
      //  die;

//         $dom = new DOMDocument;
// $dom->load($data);
// if ($dom->validate()) {
//     echo "This document is valid!\n";
// }else{
//     echo "no no ";
// }
// die;

         // $xmlcontents = XMLReader::open('filename.xml');

// $xmlDoc = new DOMDocument();
// $xmlDoc->load($data);

// print $xmlDoc->saveXML();
// die;

        $xml = simplexml_load_string($data);
        // dd($xml);
        $errorMessage = $xml->SearchHotel_Response->Error->ErrorDescription;
        
        if (!empty($errorMessage) OR empty($xml)){
            return redirect('/')->with('msg','Data Not Found, Please In Another Date');
        }
       /* echo json_encode($xml);*/
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
        $total_hotel_available= $xml->SearchHotel_Response->Hotel;
        // dd(count($total_hotel_available));
        $id_hotel = "";
        for($a=0;$a<count($total_hotel_available);$a++){
            $mg_id_hotel=$xml->SearchHotel_Response->Hotel[$a]["HotelId"];
            // dd($mg_id_hotel);
            $id_hotel .= $mg_id_hotel.',';
        }
        $toArray = explode(",",$id_hotel);

        // return $toArray;
        // die;
         $sessionlogin=session('login_user');

         $ini = count($toArray)-2;
         $whereCondition = '';
         for ($i=0; $i < count($toArray)-1 ; $i++) { 
             // echo $i;
            if($ini > $i){
             $whereCondition .= " id_from_mg"." = '".$toArray[$i]."' OR ";
         }
             if ($ini == $i){
                $whereCondition .= " id_from_mg"." = '".$toArray[$i]."'";
             }
            // $whereCondition[]= "id_from_mg => ".$toArray[$i];
         }
         // dd($whereCondition);
         // die;

        $data_hotel = DB::table('hotel')
            ->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
            //->join('master_picture','master_picture.id_hotel','=','hotel.id')
            ->select('hotel.*','hotel_detail.hotel_location','hotel_detail.hotel_address')
            // ->where('rating',$val)
            //->where('master_picture.size_type','=',1)
            ->whereRaw($whereCondition)
            // ->OrderBy('rating',$val)
            ->get();

        // dd($data_hotel);

             $idr = app('App\Http\Controllers\For_testing')->convertPrice();
             $price = (double)filter_var($idr, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

             $book = array();

        foreach($data_hotel as $baris){
            $book[] = DB::table('booking_hotel')
                    ->select('*')
                    ->where('mg_id_hotel',$baris->id_from_mg)
                    ->count();
        }

        

        $result=array();
        $i=0;
        foreach($data_hotel as $baris)
        {
            $data = array(
                'id'    =>    $baris->id,
                'hotel_name'    =>    $baris->hotel_name,
                'priceStartFrom' => number_format(((app('App\Http\Controllers\FrontendController')->cheapestPrice($baris->id)*(100+15))/100)*$price, 0,".","."),
                'hargaCoret' => number_format(((app('App\Http\Controllers\FrontendController')->cheapestPrice($baris->id)*(100+30))/100)*$price, 0,".","."),
                'hotel_address' =>$baris->hotel_address,
                'slug'=>$baris->slug,
                'rating'=>$baris->rating,
                'counter'=>$baris->view_count,
                'book'=>$book[$i]
                );
            $i++;
            array_push($result,$data);
        }
        echo json_encode($result);
        // dd($toArray);
        // return $toArray;
        // dd($id_hotel);

//         for($section_1=0,$static_section=0;$section_1<=count($total_hotel_available);$section_1++)
//         {
//             //$urut=$a+1;
//             $mg_id_hotel=$xml->SearchHotel_Response->Hotel[$section_1]["HotelId"];
//             $mg_hotel_name=$xml->SearchHotel_Response->Hotel[$section_1]["HotelName"];
//             $mg_currency_hotel=$xml->SearchHotel_Response->Hotel[$section_1]["Currency"];
//             $hotel_latitude=$xml->SearchHotel_Response->Hotel[$section_1]["Latitude"];
//             $hotel_longitude=$xml->SearchHotel_Response->Hotel[$section_1]["Longitude"];
//             $hotel_rating=$xml->SearchHotel_Response->Hotel[$section_1]["Rating"];
//             $hotel_market_name=$xml->SearchHotel_Response->Hotel[$section_1]["Market_Name"];
//             $hotel_date_check_in=$xml->SearchHotel_Response->Hotel[$section_1]["dtCheckIn"];
//             $hotel_date_check_out=$xml->SearchHotel_Response->Hotel[$section_1]["dtCheckIn"];
//             $hotel_internal_code=$xml->SearchHotel_Response->Hotel[$section_1]["InternalCode"];
//             $hotel_avail=$xml->SearchHotel_Response->Hotel[$section_1]["avail"];
//             echo "->[loop]section 1 Hotel:".$mg_hotel_name."<br/>";
// /*            $room_category_code=xml->SearchHotel_Response->Hotel->RoomCateg[0]["Code"];*/
//              $room_name = $xml->SearchHotel_Response->Hotel->RoomCateg[$section_1]['Code'];
//              // count per hotel in this looping section not count all result
//             echo count($total_room_categ_in_one_hotel= $xml->SearchHotel_Response->Hotel[$section_1]->RoomCateg);

//             echo "->[loop]section 2 Room Category :";
//             echo "<br/>";
//             for($section_2=0;$section_2<count($total_room_categ_in_one_hotel);$section_2++)
//             {
//                  echo $room_code = $xml->SearchHotel_Response->Hotel[$section_1]->RoomCateg[$section_2]['Code'];

//                 $room_code = $xml->SearchHotel_Response->Hotel[$section_1]->RoomCateg[$section_2]['Code'];

//                 echo "-> [static] section 3 Room Type".$xml->SearchHotel_Response->Hotel[$section_1]->RoomCateg[$section_2]->RoomType[0]['TypeName'];
//                 echo "<br/>";
//                 echo "-> [static] section 4 Rate".$xml->SearchHotel_Response->Hotel[$section_1]->RoomCateg[$section_2]->RoomType->Rate[0]['NightPrice'];
//                 echo "<br/>";

//                for($section_5=0;$section_5<count($xml->SearchHotel_Response->Hotel[$section_1]->RoomCateg[$section_2]->RoomType->Rate->RoomRate->RoomSeq);$section_5++)
//                 {
//                     echo "-> [static] section 5 Room Seq".$xml->SearchHotel_Response->Hotel[$section_1]->RoomCateg[$section_2]->RoomType->Rate->RoomRate->RoomSeq[$section_5]['RoomPrice'];
//                     echo "<br/>";
//                 }

//             }
//             echo "<hr/>";

//         }
        // die("sampai sini");


    }

    
}
