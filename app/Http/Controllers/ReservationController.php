<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use Session;
use App\ReservationModel;
use App\ReservationDetailModel;
use App\HotelModel;
use App\Http\Requests;
use Illuminate\Support\Str;
use DB;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
		 session_start();
	}
	
	
    public function index()
    {

        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>					
			<script>
			$(function() {
					var table = $(\"#listorder\").DataTable({
						processing: true,
						serverSide: true,
						ajax: \"".$url."/listorder/data\",
						columns: [
							{data: 'DT_Row_Index', orderable: false, searchable: false},
							{ data: 'reservation_number'},
							{ data: 'date_booking'},
							{ data: 'fullname'},
							{ data: 'check_in'},
							{ data: 'check_out'},
							{ data: 'person'},
							{ data: 'total_price_idr'},
							{ data: 'status'},
							{ data: 'action'}
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="Reservation List";
		return view('backend.reservation.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
	   // if($request->ajax()){
		$url=url('/');
		$listorder = DB::table('booking_hotel')
					->join('agent_member','agent_member.agent_account_number','booking_hotel.agent_account_number')
					->select('agent_member.*','booking_hotel.*')
					// ->join('agent_detail', 'agent_detail.agent_account_number', '=', 'booking_hotel.agent_account_number')
					// ->join('agent', 'agent.id_agent', '=', 'agent_detail.agent_id')
					// ->select('agent_detail.*','agent.*','booking_hotel.*')
					->orderBy('date_booking', 'desc')
					->get();
		// dd($listorder);
		return Datatables::of($listorder)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('status',function ($listorder) { return
					$listorder->reservation_status == 1 ? '<label class="label label-success"> booked </label>' : ($listorder->reservation_status == 2 ? '<label class="label label-warning">Refund</label>' : ($listorder->reservation_status == 3 ? '<label class="label label-info">Pending</label>' : '<label class="label label-danger">-')) ;
				})
				->addColumn('action',function ($listorder) { return
				'<a href="'.url('/').'/backend/reservation/detail/'.$listorder->id.'" class="btn btn-sm btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
				';})
				->editColumn('total_price_idr', function ($listorder) {
                    return 'Rp.'.number_format($listorder->total_price_idr, 0);
                     })
				->editColumn('person', function ($listorder) {
                    return $listorder->adult_num.' adult <br>'.$listorder->child_num.' child';
                     })
				->addIndexColumn()
				->make(true);
		// } else {
		// 	exit("Not an AJAX request -_-");
		// }
	}

	public function detail($id){
// mg_id_hotel
		// $booking = DB::table('booking_hotel')
		// 			->select('*')
		// 			->where('id',$id)
		// 			->first();
		// 			dd($booking);
		// dd($id);
		$agent = DB::table('booking_hotel')
					->join('agent_member','agent_member.agent_account_number','booking_hotel.agent_account_number')
					->select('*')
					->where('booking_hotel.id',$id)
					->first();
					// dd($agent);
		$agent_detail = DB::table('agent')
					->join('agent_detail','agent_detail.agent_id','agent.id_agent')
					->select('*')
					->where('agent.id_agent',$agent->id_agent)
					->first();
					// dd($agent_detail);
		$hotel_detail = DB::table('booking_hotel')
					->join('hotel','hotel.id_from_mg','booking_hotel.mg_id_hotel')
					->select('*')
					->where('booking_hotel.id',$id)
					->first();
		// dd($hotel_detail);
		return view('backend.reservation.detail')
				->with('agent',$agent)
				->with('agent_detail',$agent_detail)
				->with('hotel_detail',$hotel_detail);
	}
	
	public function create(){
		$url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script>
				$(\'.select2\').select2();
			</script>
			<script src=\''.$url.'/src/backend/plugins/datepicker/js/bootstrap-datepicker.js\'></script>
			<script type="text/javascript">
            $(document).ready(function () {
                $(\'.date\').datepicker({
                    format: \'yyyy-mm-dd\',
                    autoclose:true
                });
            });
        </script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>
			  <link href='".$url."/src/backend/plugins/datepicker/css/bootstrap-datepicker3.css' rel='stylesheet'>";
		$page['title']="Reservation Add";
		$sales = ReservationModel::pluck('name','id');
		$sales->prepend('', '');
		$hotel = HotelModel::pluck('hotel_name','id');
		$hotel->prepend('', ''); 
		return view('backend.reservation.formcreate', compact('sales','hotel'))->with('js',$js)->with('css',$css)->with('page',$page);
	}

	public function store(Request $request){
		$cruds = new ReservationDetailModel();
		$cruds->id_sales	= $request->id_sales;
		$cruds->id_hotel	= $request->id_hotel;
		$cruds->id_room		= $request->id_room;
		$cruds->check_in 	= $request->check_in;
		$cruds->check_out 	= $request->check_out;
		$cruds->price 		= $request->price;
		$cruds->adult 		= $request->adult;
		$cruds->child 		= $request->child;
		$cruds->room_total 	= $request->room_total;
		if($cruds->save()){
			return redirect()->route('reservation.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('reservation.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
	}

	public function edit($id){
		$url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script>
				$(\'.select2\').select2();
			</script>
			<script src=\''.$url.'/src/backend/plugins/datepicker/js/bootstrap-datepicker.js\'></script>
			<script type="text/javascript">
            $(document).ready(function () {
                $(\'.date\').datepicker({
                    format: \'yyyy-mm-dd\',
                    autoclose:true
                });
            });
        </script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>
			  <link href='".$url."/src/backend/plugins/datepicker/css/bootstrap-datepicker3.css' rel='stylesheet'>";
		$page['title']="Reservation Edit";

		$cruds = ReservationDetailModel::find($id);

		$sales = ReservationModel::pluck('name','id');
		$sales->prepend('', '');
		$hotel = HotelModel::pluck('hotel_name','id');
		$hotel->prepend('', ''); 
		return view('backend.reservation.formupdate', compact('sales','hotel'))->with('js',$js)->with('css',$css)->with('page',$page)->with('reservation', $cruds);
	}

	public function update(Request $request){
		$id=$request->id;
        $cruds = ReservationDetailModel::find($id);
		$cruds->id_sales	= $request->id_sales;
		$cruds->id_hotel	= $request->id_hotel;
		$cruds->id_room		= $request->id_room;
		$cruds->check_in 	= $request->check_in;
		$cruds->check_out 	= $request->check_out;
		$cruds->price 		= $request->price;
		$cruds->adult 		= $request->adult;
		$cruds->child 		= $request->child;
		$cruds->room_total 	= $request->room_total;
		if($cruds->save()){
			return redirect()->route('reservation.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('reservation.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
	}

	public function destroy($id)
    {
        // $cruds = cityModel::findOrFail($id);
		$cruds = ReservationDetailModel::find($id);
		if($cruds->delete()){
			  return redirect()->route('reservation.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('reservation.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }

}

