<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use App\Http\Requests;
use Session;
use PDF;
use DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 function __construct()
		{
			$this->middleware('SystemRule');
			session_start();
		 
		}
	 
    public function index()
    {
        $listDaily = DB::table('booking_hotel')
					->join('agent_member','agent_member.agent_account_number','booking_hotel.agent_account_number')
					->select('agent_member.*','booking_hotel.*')
					->orderBy('date_booking', 'desc')
					->whereDay('date_booking', '=', date('d')) 
					->get();

		// dd($listDaily);

		$page['title']="Report";
		return view('backend.report.list')
				->with('page',$page)
				->with('listDaily',$listDaily);
	
    }

	public function displayReport(Request $request) {
	    $listDaily = DB::table('booking_hotel')
					->join('agent_member','agent_member.agent_account_number','booking_hotel.agent_account_number')
					->select('agent_member.*','booking_hotel.*')
					->orderBy('date_booking', 'desc')
					->whereDay('date_booking', '=', date('d')) 
					->get();

        view()->share('listDaily',$listDaily);

        // echo 'sini';
        // die;
        if($request->has('download')){
                    PDF::loadView('backend.report.dailyOrder', [
  'title' => 'Another Title',
  'orientation' => 'L'
])->stream('sample.pdf');
                    // echo 'sini';
                    // die;
        }




        return view('backend.report.list')->with('listDaily',$listDaily);;

	}
		
}
