<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\FacilityModel;
use App\Http\Requests;
use Illuminate\Support\Str;
use Session;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
		session_start();
	}
	
	private static $option=array('0'=>'Room','1'=>'Hotel');
	
    public function index()
    {
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>					
			<script>
			$(function() {
					var table = $(\"#facility\").DataTable({
						processing: true,
						serverSide: true,
						ajax: \"".$url."/facility/data\",
						columns: [
							{ data: 'facility_name'},
							{ data: 'action', 'searchable': false, 'orderable':false }
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="Facility";
		return view('backend.facility.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
	   if($request->ajax()){
		$url=url('/');
		$facility = DB::table('master_facility')->select('*')->get();
		// print_r($facility); exit();
		return Datatables::of($facility)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($facility) { return
				'<a href="'.url('/').'/backend/facility/edit/'.$facility->id.'" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/backend/facility/delete/'.$facility->id.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				';})
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url=url('/');
		$js=" ";
		$css=" ";
		$optionview=self::$option;
		$page['title']="Facility Add";
		return view('backend.facility.formcreate', compact('optionview'))->with('js',$js)->with('css',$css)->with('page',$page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$session=session('login');
        $cruds = new FacilityModel();
		$cruds->facility_name = $request->facility_name;
		// $cruds->facility_type = $request->facility_type;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			return redirect()->route('facility.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('facility.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $url=url('/');
		$js=" ";
		$css=" ";
		$page['title']="Facility Edit";
		// $option=self::$option;
		// $benua = ContinentModel::pluck('continent_name_en','id');
		$optionview=self::$option;
		$cruds = FacilityModel::find($id);
		return view('backend.facility.formupdate', compact('optionview'))->with('js',$js)->with('css',$css)->with('page',$page)->with('facility', $cruds);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$session=session('login');
        $id=$request->id;
        $cruds = FacilityModel::find($id);
		$cruds->facility_name = $request->facility_name;
		// $cruds->facility_type = $request->facility_type;
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			  return redirect()->route('facility.edit',['id'=>$id])->with('alert-success', 'Data Berhasil Diubah.');
		 }
		 else{
			 return redirect()->route('facility.edit',['id'=>$id])->with('alert-danger', 'Data Tidak Berhasil Diubah.');
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $cruds = facilityModel::findOrFail($id);
		$cruds = FacilityModel::find($id);
		if($cruds->delete()){
			  return redirect()->route('facility.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('facility.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }
}
