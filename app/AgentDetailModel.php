<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class AgentDetailModel extends Model
{
    protected $table = 'agent_detail';
    protected $primaryKey = 'id';
	public $timestamps = false;
	
}
