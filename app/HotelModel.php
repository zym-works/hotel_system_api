<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class HotelModel extends Model
{
    protected $table = 'hotel';
	public $timestamps = false;
	 public $fillable = ['hotel_name','id','view_count'];
}
