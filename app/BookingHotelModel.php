<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class BookingHotelModel extends Model
{
    protected $table = 'booking_hotel';
	public $timestamps = false;
	
}
