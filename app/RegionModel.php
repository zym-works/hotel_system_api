<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class RegionModel extends Model
{
    protected $table = 'master_region';
	public $timestamps = false;
	
}
