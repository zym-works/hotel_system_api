<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class AgentMemberModel extends Model
{
    protected $table = 'agent_member';
	public $timestamps = false;
	
}
