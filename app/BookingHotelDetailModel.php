<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class BookingHotelDetailModel extends Model
{
    protected $table = 'booking_hotel_detail';
	public $timestamps = false;
	
}
