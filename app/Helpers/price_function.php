<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/5/2017
 * Time: 3:39 PM
 */

	namespace App\Helpers;

    class CurrencyHelp{
        public $start;
        public $end;

        public static function convertCurrency($start,$end){
            $cur_rate=$start.'/'.$end;
            $rate = Swap::latest($cur_rate);
            return $rate->getValue();
        }

    }

    ?>