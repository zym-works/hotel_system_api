<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<?php
	use App\CustomerModel ;
	use App\WishlistModel ;
	use App\FrontModel ;
	use App\KategoriModel;
	use App\ReviewModel;
	use App\CountryModel;
	use App\LokasiModel;
	use App\ItemModel;
	use App\CurrencyMarginModel;
	$kategorimenu=KategoriModel::getHeaderKategori();
	$userlogin=session('customer_login');
	$textsession =session('text');
	$sortsession =session('sort');
	use Illuminate\Support\Facades\Route;
$currentPath= Route::getFacadeRoot()->current()->uri();
	if($textsession != ''){
		session()->forget('text');
	}
?>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title>비프리투어-싱가폴,홍콩,싱가포르,마카오,패키지,여행,입장권,티켓,자유여행,가격,지도,예약</title>
<meta name="description" content="싱가폴유니버셜스튜디오,가든스바이더베이,나이트사파리,센토사섬,싱가포르케이블카,머라이언파크,리버사파리,루지,동물원,주롱새공원,관광지,경비,코스,일정,가볼만한곳,에어텔,항공권,심포니오브라이트,홍콩디즈니랜드입장권,홍콩빅버스투어,홍콩오션파크,홍콩마담투소,스카이100,홍콩피크트램,홍콩AEL,옥토퍼스카드,홍콩마카오페리,하우스오브댄싱워터,골든릴,마카오타워,베네시안곤돌라.">
<meta property="og:type" content="website">
<meta property="og:image" content="https://befreetour.com/img/meta.jpg" />
<meta property="og:title" content="비프리투어-싱가폴,홍콩,싱가포르,마카오,패키지,여행,입장권,티켓,자유여행,가격,지도,예약">
<meta property="og:description" content="싱가폴유니버셜스튜디오,가든스바이더베이,나이트사파리,센토사섬,싱가포르케이블카,머라이언파크,리버사파리,루지,동물원,주롱새공원,관광지,경비,코스,일정,가볼만한곳,에어텔,항공권,심포니오브라이트,홍콩디즈니랜드입장권,홍콩빅버스투어,홍콩오션파크,홍콩마담투소,스카이100,홍콩피크트램,홍콩AEL,옥토퍼스카드,홍콩마카오페리,하우스오브댄싱워터,골든릴,마카오타워,베네시안곤돌라.">

	<meta property="og:site_name" content="Befree Tour" />
	<META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
	<link rel = "canonical" href = "http://www.befreetour.com/index"> 

    <!-- Favicons-->
    <link rel="icon" type="image/png" href="{{url('/')}}/src/bismillah/img/favicon.ico" />
    <link rel="apple-touch-icon" type="image/x-icon" href="{{url('/')}}/src/bismillah/img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{url('/')}}/src/bismillah/img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{url('/')}}/src/bismillah/img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{url('/')}}/src/bismillah/img/apple-icon-144x144.png">

    <!-- BASE CSS -->
    <link href="{{ URL::asset('src/bismillah/css/base.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('src/bismillah/css/stylebismillah.css') }}" rel="stylesheet">
	
	<!--Panggil Font Awesome -->
	<link rel="stylesheet" href="{{url('/')}}/src/bismillah/fonts/font-awesome/css/font-awesome.min.css">

    <!-- Google web fonts -->
   <link base href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' >
   <link base href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' >
   <link base href='https://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' >

    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{ URL::asset('src/bismillah/rs-plugin/css/settings.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('src/bismillah/css/extralayers.css') }}" rel="stylesheet">
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel='stylesheet' >

    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript" src="https://wcs.naver.net/wcslog.js"></script>
<script type="text/javascript">
if(!wcs_add) var wcs_add = {};
wcs_add["wa"] = "182fde09506932";
wcs_do();
</script>
</head>

<body>
<style>

</style>


<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->

   <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <!-- Header================================================== -->
   @include('bismillah.componen.header')
<section id="hero" style="height:480px">
 	<div class="intro_title">
    	<h3 class="animated fadeInDown">“세상에 단 하나뿐인 나만의 자유여행”</h3>
        <p class="animated fadeInDown">내 마음대로 디자인하는 전 세계 여행지의 인기 액티비티!</p>
		<div class="container">
						<form  method="get" action="{{route('search_data')}}">
                            <div class="search_bar">
                            <span class="nav-facade-active" id="nav-search-in">
                            <span id="nav-search-in-content" style=""><strong>여행지</strong></span>
                            <span class="nav-down-arrow nav-sprite"></span>
                            <select title="Search in" class="searchSelect" id="searchDropdownBox" name="id_lokasi" onchange="getComboA(this)">
                               
								<?php
									$countrys=CountryModel::staticParent();
									
									foreach($countrys as $country){
										$lokasis=LokasiModel::staticParent($country->id);
										echo'
											<optgroup label="'.$country->nama_negara.'">';
												foreach($lokasis as $lokasi){
													echo'<option value="'.$lokasi->id.'" title="'.$lokasi->nama_lokasi.'">'.$lokasi->nama_lokasi.'</option>';
												}
											echo'</optgroup>
										';
									}
								?>
								
                            </select>
                            </span>
                            <div class="nav-searchfield-outer">
                               <input type="text" autocomplete="off" name="hasil" id="twotabsearchtextbox" placeholder="당신의 자유여행지는 어디인가요 ?">
                            </div>
                            <div class="nav-submit-button">
                                <input type="submit" title="Cerca" class="nav-submit-input" value="Search">
                            </div>
                        </div><!-- End search bar-->
						</form>
                    </div>
	</div>
    <!--	 <div id="search_bar_container">
                	
                </div><!-- /search_bar-->
</section><!-- End hero -->
	
	<div id="myCarousel" class="carousel slide" data-ride="carousel" style="width:100%; margin-top:-3px;"> 
  <!-- Indicators -->
  
  <div class="carousel-inner">
    <div class="item active"> 
		<a href="#" title="">
		<img src="<?php echo url('/')?>/src/bismillah/slider/img/banner1.jpg" style="width:100%" alt="First slide">
		</a>
    </div>
    <div class="item"> 
		<a href="<?php echo url('/');?>/event" title="">
		<img src="<?php echo url('/')?>/src/bismillah/slider/img/banner2.jpg" style="width:100%" data-src="" alt="Second    slide">
		</a>
    </div>
    <div class="item"> 
		<a href="http://www.korinatour.com/concierge/" title="">
		<img src="<?php echo url('/')?>/src/bismillah/slider/img/banner3.jpg" style="width:100%"  data-src="" alt="Third slide">
		</a>
    </div>
	<div class="item"> 
		<a href="http://www.korinatour.com/baliresortpoolvilla/" title="">
		<img src="<?php echo url('/')?>/src/bismillah/slider/img/banner4.jpg" style="width:100%"  data-src="" alt="Third slide">
		</a>
    </div>
	<div class="item"> 
		<a href="https://www.befreetour.com/guarantee" title="">
		<img src="<?php echo url('/')?>/src/bismillah/slider/img/banner5.jpg" style="width:100%"  data-src="" alt="Third slide">
		</a>
    </div>
	<!--<div class="item"> 
		<a href="http://cafe.naver.com/hotelbali/1610" title="">
		<img src="<?php echo url('/')?>/src/bismillah/slider/img/reviewr-1-1800x250.jpg" style="width:100%"  data-src="" alt="Third slide">
		</a>
    </div> -->
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="icon-left-circle-1"></i></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="icon-right-circle-1"></i></a> 
  </div>
 
    <div class="container margin_60">
    
		<div class="main_title">
            <h2>Top <span>Destination</span> </h2>
            <p>가장 인기 있는 여행지로 떠나보세요.</p>
        </div>
        
        <div class="row">
			@foreach ($popular as $values)
				 <div class="col-md-4 col-sm-6">
				<!--	<div class="tour_container"> -->
						<div class="ribbon_3 popular"><span></span></div>
						<div class="img_container">							
							<a href="<?php echo url('/')?>/search_data?id_lokasi=<?php echo $values->id_lokasi ; ?>&id_kategori=0" title="">
								<?php if($values->pic_lokasi == null) { ?>
							<img src="<?php echo url('/'); ?>/img/image-not-found.jpg" class="img-responsive" alt="">
								<?php }else { ?>
							<img src="<?php echo url('/'); ?>/img/lokasi_pic/<?php echo $values->pic_lokasi; ?>" class="img-responsive" alt="">
								<?php } ?>
							<div class="short_title">
								<h3><strong><center>{{$values->nama_lokasi}}</center></strong></h3>
							</div>
							</a>
						</div>
						
				<!--	</div>--><!-- End box -->
					
				</div><!-- End col-md-4 -->
			@endforeach
            
        </div><!-- End row -->
       
		<hr>
	
        <div class="main_title">
            <h2>Top <span>Activity</span></h2>
            <p>가장 인기 있는 액티비티를 즐겨보세요.</p>
        </div>
        
        <div class="row">
		<?php 
			$iddata=0; 
			$no=1;
			$cur_convert=session('cur');
			// print_r($cur_convert); exit;
			
		?>
			@foreach ($tampil as $value)
			<?php if($iddata != $value->id_produk){ 
					if($no <=6){
						$datareview=ReviewModel::getdatareviewproduk($value->id_produk);
						$datacurrencyproduk=ItemModel::getcurrencycode($value->id_produk);
						$rata=0;
							if(count($datareview) > 0){
								$total=0;
								$i=0;
								
								foreach($datareview as $review){
									$total=$total+$review->rating;
									$i++;
								}
								$rata=$total/$i;
							}
							$start=$datacurrencyproduk->currency_name;
							$end='KRW';
							$getcurrency=new \App\Helpers\CurrencyHelp();
							$currate=$getcurrency->convertCurrency($start,$end);
							$getcureencymargin=CurrencyMarginModel::getmargin($start,$end);
							$currency_code='₩';
							$currency_publish=round(($currate+$getcureencymargin->currency_margin)*$value->publish_price);
							$currency_sale=round(($currate+$getcureencymargin->currency_margin)*$value->sale_price);
							$currency_price=round(($currate+$getcureencymargin->currency_margin)*$value->produk_harga);
							if (substr($currency_price,-2)>49){
								$currency_price=round($currency_price,-2);
							} else {
								$currency_price=round($currency_price,-2)+100;
							}
							if (substr($currency_publish,-2)>49){
								$currency_publish=round($currency_publish,-2);
							} else {
								$currency_publish=round($currency_publish,-2)+100;
							}
							if($currency_sale != '' && $currency_sale != '0'){
								if (substr($currency_sale,-2)>49){
									$currency_sale=round($currency_sale,-2);
								} else {
									$currency_sale=round($currency_sale,-2)+100;
								}	
							}
						
						
			?>
				<div class="col-md-4 col-sm-6">
					<div class="tour_container">
						<?php if($value->ribbon != ''){ ?>
						<div class="ribbon_3 popular"><span><?php echo $value->ribbon; ?></span></div>
						<?php } ?>
						<div class="img_container">
							<a href="<?php echo url('/')?>/detail/{{$value->id}}/{{$value->slug_judul}}" title="{{$value->produk_nama}}">
								<?php if($value->pic_produk == null) { ?>
							<img src="<?php echo url('/'); ?>/img/image-not-found.jpg" class="img-responsive" alt="">
								<?php }else { ?>
							<img src="<?php echo url('/'); ?>/img/produk/{{$value->slug_judul}}/<?php echo $value->pic_produk; ?>" class="img-responsive" alt="">
								<?php } ?>
							<?php
								if($value->publish_price > '0'){
									$totaldis=$currency_publish-$currency_price;
									$dis=($totaldis*100)/$currency_publish;
								?>
							<div class="badge_save">Save<strong>{{number_format($dis)}}%</strong></div>
							<?php }
							?>
							<div class="short_info">
								<?php if($value->publish_price > '0'){ ?>
								<span class="short_price pull-right"><sup>{{$currency_code}}</sup><del>{{number_format($currency_publish)}}</del></span><br>
                                
								<span class="price"><sup>{{$currency_code}}</sup>{{number_format($currency_price)}}<small> / <?php echo $value->produk_per; ?></small></span>
								<?php }else{ ?>
								
								<span class="price"><sup>{{$currency_code}}</sup>{{number_format($currency_price)}}<small> / <?php echo $value->produk_per; ?></small></span>
								<?php } ?>
							</div>
							</a>
						</div>
						<div class="tour_title">
						<?php $jumlahhuruf = strlen($value->produk_nama); 
							if($jumlahhuruf > 25 ){
								$huruf = substr($value->produk_nama,0,25).'  . . .';
							}else{
								$huruf = $value->produk_nama;
							}
						?>
						@if($value->produk_instan == 1)
							<h3><strong><?php echo $value->produk_nama; ?></strong><font color='#FF5722'><i class="icon-flash"></i></font></h3>
						@else
							<h3><strong><?php echo $value->produk_nama; ?></strong></h3>
						@endif
							<div class="rating">
								<?php
									for($a=1;$a<=5;$a++){
										if($a <= $rata ){
											echo"<i class=\"icon-star voted\"></i>";
										}else{
											echo"<i class=\"icon-star-empty\"></i>";
										}
									}
								?>
								</div><!-- end rating -->
							
							@if($userlogin['id'] == '')
								<div class="wishlist" id="wishlist_{{$value->id_produk}}">
									<a class="tooltip_flip tooltip-effect-1" onclick="addwishlist({{$value->id_produk}});">+<span class="tooltip-content-flip"><span class="tooltip-back">위시리스트 담기</span></span></a>
								</div><!-- End wish list-->
							@else
								<?php 
									$getiduser=CustomerModel::getidcustomer($userlogin['id']);
									$datawishlist=WishlistModel::getDataWishlist($getiduser->id,$value->id_produk);
								?>
									@if(count($datawishlist) > 0)
										<div class="wishlist" id="wishlist_{{$value->id_produk}}">
											<a class="tooltip_flip tooltip-effect-1" onclick="removewishlist({{$value->id_produk}},{{$datawishlist->id}});">-<span class="tooltip-content-flip"><span class="tooltip-back">위시리스트에 제거</span></span></a>
										</div><!-- End wish list-->
									@else
										<div class="wishlist" id="wishlist_{{$value->id_produk}}">
											<a class="tooltip_flip tooltip-effect-1" onclick="addwishlist({{$value->id_produk}});">+<span class="tooltip-content-flip"><span class="tooltip-back">위시리스트 담기</span></span></a>
										</div><!-- End wish list-->
									@endif
							@endif
							<div class="view_viewer"><i class="view_viewer_coba"></i>조회수 {{number_format($value->count_view)}} <span class="view_right">{{number_format($value->count_buy)}} 건 예약</span></div>
						</div>
					</div><!-- End box tour -->
				</div><!-- End col-md-4 -->
				<?php 
						$no++;
						}
					}
					$iddata= $value->id_produk; 
					?>
			@endforeach
            
        </div><!-- End row -->
       
        
    </div><!-- End container -->
    
    
    <section class="promo_full">
    <div class="promo_full_wp magnific">
        <div>
          <!-- <h3>Singapore Panorama</h3>
            <p>
                일상에 지친 당신, 이제 어디서든 비프리투어와 함께 자유롭게 여행을 떠나보세요.
            </p> -->
            <a href="https://www.youtube.com/watch?v=ewX2yY32OHc" class="video"><i class="icon-play-circled2-1"></i></a>
        </div>
    </div>
    </section><!-- End section -->
    
    
    @include('bismillah.componen.footer')

<div id="toTop"></div><!-- Back to top button -->

    <!-- Common scripts -->
	<script src="{{ URL::asset('src/bismillah/slider/js/jssor.slider.mini.js') }}"></script>
    <script src="{{ URL::asset('src/bismillah/js/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ URL::asset('src/bismillah/js/common_scripts_min.js') }}"></script>
    <script src="{{ URL::asset('src/bismillah/js/functions.js') }}"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->

	<script src="{{ URL::asset('src/bismillah/js/modernizr.js') }}"></script>  
	<script src="{{ URL::asset('src/bismillah/js/video_header.js') }}"></script>
	<script src="{{ URL::asset('src/bismillah/js/icheck.js') }}"></script>

		<script>
		<?php $url=url('/'); 
			$userlog=session('customer_login');
				if($userlog['id'] == ''){
				 $hasil =  '0';
				}else{
				 $hasil= $userlog['id'];
				}
		?>
	
	function addwishlist(id){
	 username='<?php echo $hasil; ?>';
	 if(username == '0'){
		 alert('로그인이 필요합니다.');
	 }else{
		 $.ajax({

		 type: "GET",
		 url: '<?php echo $url; ?>/wishlist/addwishlish',
		 data: "id=" + id, // appears as $_GET['id'] @ your backend side
		 success: function(data) {
			   // data is ur summary
			  $('#wishlist_'+id).html(data);
		 }

   });
 }
	 
 }
 
 function removewishlist(id, idwishlist){
	 username='<?php echo $hasil; ?>';
	 // alert(id);
	 if(username == '0'){
		 alert('로그인이 필요합니다.');
	 }else{
		 $.ajax({

		 type: "GET",
		 url: '<?php echo $url; ?>/wishlist/removewishlish',
		 data: 'id=' + id + '&idwishlist=' + idwishlist,// appears as $_GET['id'] @ your backend side
		 success: function(data) {
			   // data is ur summary
			   // alert($data);
			  $('#wishlist_'+id).html(data);
		 }

   });
 }
	 
 }


function getComboA(sel) {
	var strUser=sel.options[sel.selectedIndex].text;
	document.getElementById('nav-search-in-content').innerHTML = strUser;
}
 
 // $(document).ready(function() {

	   // HeaderVideo.init({
		  // container: $('.header-video'),
		  // header: $('.header-video--media'),
		  // videoTrigger: $("#video-trigger"),
		  // autoPlayVideo: false
		// });    

	// });
	
	</script>


</html>