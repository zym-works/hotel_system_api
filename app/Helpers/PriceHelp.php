<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/5/2017
 * Time: 3:39 PM
 */
	namespace App\Helpers;
	use DB;
    class PriceHelp{
        public static function cheapestPrice($idHotel){
            $harga_termurah = DB::table('master_room')
                ->select('master_room.net_price')
                ->where('master_room.id_hotel','=',$idHotel)
                ->where('master_room.id_bed_room','=',1)
                ->first();
            //*13519
            return $harga_termurah->net_price;
        }
    }

    ?>