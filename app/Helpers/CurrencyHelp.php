<?php
	
	namespace App\Helpers;
	
	use Carbon\Carbon;
	use Swap;
	
	class CurrencyHelp{
		public $start;
		public $end;
		
		public static function convertCurrency($start,$end){
			$cur_rate=$start.'/'.$end;
			$rate = Swap::latest($cur_rate);
			return $rate->getValue();
		}
		
	}
	
?>