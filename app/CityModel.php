<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
    protected $table = 'master_city';
	public $timestamps = false;
	
}
