<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class AgentBalanceModel extends Model
{
    protected $table = 'agent_balance';
    // protected $primaryKey = 'id';
	public $timestamps = false;
	
}
