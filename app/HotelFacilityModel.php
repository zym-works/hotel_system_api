<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class HotelFacilityModel extends Model
{
    protected $table = 'hotel_facility';
	public $timestamps = false;
	protected $fillable = [
        'id_hotel', 'id_facility',
    ];
}
