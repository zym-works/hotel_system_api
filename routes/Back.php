<?php

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
|
*/

// Route::get('/home', [
// 		'as' => 'home',
// 		'uses' => 'HomeController@index'
// 	]);

Route::get('/korinaadmin', [
		'as' => 'korinaadmin',
		'uses' => 'HomeController@login'
	]);
	Route::post('/home/login', [
		'as' => 'home.login_submit',
		'uses' => 'HomeController@login_submit'
	]);
	Route::get('/home/', [
		'as' => 'home',
		'uses' => 'HomeController@index'
	])->middleware('SystemRule');
	
	Route::get('/logout', function() {
	Session::flush();
	  if(!Session::has('login'))
	   {
		   return redirect('korinaadmin');
	   }
	 });

Route::get('/backend/profile', [
		'as' => 'home.profile',
		'uses' => 'HomeController@dataprofile'
	]);

Route::get('/backend/country', [
		'as' => 'country.index',
		'uses' => 'CountryController@index'
	]);
	Route::get('/country/data', [
			'as' => 'country.data',
			'uses' => 'CountryController@data_show'
		]);
	Route::get('/backend/country/create', [
		'as' => 'country.create',
		'uses' => 'CountryController@create'
	]);
	Route::get('/backend/country/edit/{id}', [
		'as' => 'country.edit',
		'uses' => 'CountryController@edit'
	]);
	Route::post('/country/update', [
		'as' => 'country.update',
		'uses' => 'CountryController@update'
	]);
	Route::post('/country/store', [
		'as' => 'country.store',
		'uses' => 'CountryController@store'
	]);
	Route::get('/backend/country/delete/{id}', [
		'as' => 'country.delete',
		'uses' => 'CountryController@destroy'
	]);

Route::get('/backend/region', [
		'as' => 'region.index',
		'uses' => 'RegionController@index'
	]);
	Route::get('/region/data', [
			'as' => 'region.data',
			'uses' => 'RegionController@data_show'
		]);
	Route::get('/backend/region/create', [
		'as' => 'region.create',
		'uses' => 'RegionController@create'
	]);
	Route::get('/backend/region/edit/{id}', [
		'as' => 'region.edit',
		'uses' => 'RegionController@edit'
	]);
	Route::post('/region/update', [
		'as' => 'region.update',
		'uses' => 'RegionController@update'
	]);
	Route::post('/region/store', [
		'as' => 'region.store',
		'uses' => 'RegionController@store'
	]);
	Route::get('/backend/region/delete/{id}', [
		'as' => 'region.delete',
		'uses' => 'RegionController@destroy'
	]);

Route::get('/backend/city', [
		'as' => 'city.index',
		'uses' => 'CityController@index'
	]);
	Route::get('/city/data', [
			'as' => 'city.data',
			'uses' => 'CityController@data_show'
		]);
	Route::get('/backend/city/create', [
		'as' => 'city.create',
		'uses' => 'CityController@create'
	]);
	Route::get('/backend/city/edit/{id}', [
		'as' => 'city.edit',
		'uses' => 'CityController@edit'
	]);
	Route::post('/city/update', [
		'as' => 'city.update',
		'uses' => 'CityController@update'
	]);
	Route::post('/city/store', [
		'as' => 'city.store',
		'uses' => 'CityController@store'
	]);
	Route::get('/backend/city/delete/{id}', [
		'as' => 'city.delete',
		'uses' => 'CityController@destroy'
	]);

Route::get('/backend/facility', [
		'as' => 'facility.index',
		'uses' => 'FacilityController@index'
	]);
	Route::get('/facility/data', [
			'as' => 'facility.data',
			'uses' => 'FacilityController@data_show'
		]);
	Route::get('/backend/facility/create', [
		'as' => 'facility.create',
		'uses' => 'FacilityController@create'
	]);
	Route::get('/backend/facility/edit/{id}', [
		'as' => 'facility.edit',
		'uses' => 'FacilityController@edit'
	]);
	Route::post('/facility/update', [
		'as' => 'facility.update',
		'uses' => 'FacilityController@update'
	]);
	Route::post('/facility/store', [
		'as' => 'facility.store',
		'uses' => 'FacilityController@store'
	]);
	Route::get('/backend/facility/delete/{id}', [
		'as' => 'facility.delete',
		'uses' => 'FacilityController@destroy'
	]);

Route::get('/backend/area', [
		'as' => 'area.index',
		'uses' => 'AreaController@index'
	]);
	Route::get('/area/data', [
			'as' => 'area.data',
			'uses' => 'AreaController@data_show'
		]);
	Route::get('/backend/area/create', [
		'as' => 'area.create',
		'uses' => 'AreaController@create'
	]);
	Route::get('/backend/area/edit/{id}', [
		'as' => 'area.edit',
		'uses' => 'AreaController@edit'
	]);
	Route::post('/area/update', [
		'as' => 'area.update',
		'uses' => 'AreaController@update'
	]);
	Route::post('/area/store', [
		'as' => 'area.store',
		'uses' => 'AreaController@store'
	]);
	Route::get('/backend/area/delete/{id}', [
		'as' => 'area.delete',
		'uses' => 'AreaController@destroy'
	]);

Route::get('/backend/hotel', [
		'as' => 'hotel.index',
		'uses' => 'HotelController@index'
	]);
	Route::get('backend/image/delete/{id}/{hotel}','HotelController@img_delete');
	Route::get('/hotel/data', [
			'as' => 'hotel.data',
			'uses' => 'HotelController@data_show'
		]);
	Route::get('/backend/hotel/create', [
		'as' => 'hotel.create',
		'uses' => 'HotelController@create'
	]);
	Route::get('/backend/hotel/edit/{id}/{idhal}/{idedit}', [
		'as' => 'hotel.edit',
		'uses' => 'HotelController@edit'
	]);
	Route::post('/hotel/update', [
		'as' => 'hotel.update',
		'uses' => 'HotelController@update'
	]);
	Route::post('/hotel/store', [
		'as' => 'hotel.store',
		'uses' => 'HotelController@store'
	]);
	Route::get('/backend/hotel/delete/{id}', [
		'as' => 'hotel.delete',
		'uses' => 'HotelController@destroy'
	]);
	Route::post('/backend/hotel/add_detail_hotel', [
		'as' => 'hotel.add_detail_hotel',
		'uses' => 'HotelController@add_detail_hotel'
	]);
	Route::post('/hotel/edit_detail_hotel', [
		'as' => 'hotel.edit_detail_hotel',
		'uses' => 'HotelController@edit_detail_hotel'
	]);
	Route::get('/backend/hotel/delete_detail_hotel/{id}/{id2}', [
		'as' => 'hotel.delete_detail_hotel',
		'uses' => 'HotelController@delete_detail_hotel'
	]);
	Route::post('/backend/hotel/add_room_hotel', [
		'as' => 'hotel.add_room_hotel',
		'uses' => 'HotelController@add_room_hotel'
	]);
	Route::post('/hotel/edit_room_hotel', [
		'as' => 'hotel.edit_room_hotel',
		'uses' => 'HotelController@edit_room_hotel'
	]);
	Route::get('/backend/hotel/delete_room_hotel/{id}/{id2}', [
		'as' => 'hotel.delete_room_hotel',
		'uses' => 'HotelController@delete_room_hotel'
	]);
	Route::get('/backend/hotel/view/{id}', [
		'as' => 'hotel.detail_hotel',
		'uses' => 'HotelController@detail_hotel'
	]);
	Route::post('/backend/hotel/add_facility}', [
		'as' => 'hotel.add_facility_hotel',
		'uses' => 'HotelController@add_facility_hotel'
	]);
Route::post('/hotel/save-image',[
		'as'=>'hotel.save-image',
		'uses'=>'HotelController@upload_image_hotel'
	]);
Route::post('/room/save-image',[
		'as'=>'room.save-image',
		'uses'=>'HotelController@upload_image_room'
	]);

Route::get('/backend/room', [
		'as' => 'room.index',
		'uses' => 'RoomController@index'
	]);
	Route::get('/room/data', [
			'as' => 'room.data',
			'uses' => 'RoomController@data_show'
		]);
	Route::get('/backend/room/create', [
		'as' => 'room.create',
		'uses' => 'RoomController@create'
	]);
	Route::get('/backend/room/edit/{id}', [
		'as' => 'room.edit',
		'uses' => 'RoomController@edit'
	]);
	Route::post('/room/update', [
		'as' => 'room.update',
		'uses' => 'RoomController@update'
	]);
	Route::post('/room/store', [
		'as' => 'room.store',
		'uses' => 'RoomController@store'
	]);
	Route::get('/backend/room/delete/{id}', [
		'as' => 'room.delete',
		'uses' => 'RoomController@destroy'
	]);

Route::get('/backend/roombed', [
		'as' => 'roombed.index',
		'uses' => 'RoomBedController@index'
	]);
	Route::get('/roombed/data', [
			'as' => 'roombed.data',
			'uses' => 'RoomBedController@data_show'
		]);
	Route::get('/backend/roombed/create', [
		'as' => 'roombed.create',
		'uses' => 'RoomBedController@create'
	]);
	Route::get('/backend/roombed/edit/{id}', [
		'as' => 'roombed.edit',
		'uses' => 'RoomBedController@edit'
	]);
	Route::post('/roombed/update', [
		'as' => 'roombed.update',
		'uses' => 'RoomBedController@update'
	]);
	Route::post('/roombed/store', [
		'as' => 'roombed.store',
		'uses' => 'RoomBedController@store'
	]);
	Route::get('/backend/roombed/delete/{id}', [
		'as' => 'roombed.delete',
		'uses' => 'RoomBedController@destroy'
	]);

Route::get('/backend/currency', [ 
		'as' => 'currency.index',
		'uses' => 'CurrencyController@index'
	]);
	Route::get('/currency/data', [
			'as' => 'currency.data',
			'uses' => 'CurrencyController@data_show'
		]);
	Route::get('/backend/currency/create', [
		'as' => 'currency.create',
		'uses' => 'CurrencyController@create'
	]);
	Route::get('/backend/currency/edit/{id}', [
		'as' => 'currency.edit',
		'uses' => 'CurrencyController@edit'
	]);
	Route::post('/currency/update', [
		'as' => 'currency.update',
		'uses' => 'CurrencyController@update'
	]); 
	Route::post('/currency/store', [
		'as' => 'currency.store',
		'uses' => 'CurrencyController@store'
	]); 
	Route::get('/backend/currency/delete/{id}', [
		'as' => 'currency.delete',
		'uses' => 'CurrencyController@destroy'
	]);

Route::get('/listreservation', [ 
		'as' => 'reservation.index',
		'uses' => 'ReservationController@index'
	]);
	Route::get('/listorder/data', [
			'as' => 'listorder.data',
			'uses' => 'ReservationController@data_show'
		]);

	Route::get('/listreservation/create', [
		'as' => 'reservation.create',
		'uses' => 'ReservationController@create'
	]);
	Route::post('/listreservation/store', [
		'as' => 'reservation.store',
		'uses' => 'ReservationController@store'
	]);
	Route::get('/listorder/edit/{id}', [
			'as' => 'listorder.edit',
			'uses' => 'ReservationController@edit'
		]);
	Route::post('/listorder/update', [
			'as' => 'listorder.update',
			'uses' => 'ReservationController@update'
		]);
	Route::get('/listorder/delete/{id}', [
			'as' => 'listorder.delete',
			'uses' => 'ReservationController@destroy'
		]);
	Route::get('/backend/reservation/detail/{id}','ReservationController@detail');

Route::get('/backend/agent', [ 
		'as' => 'agent.index',
		'uses' => 'AgentController@index'
	]);
	Route::get('/agent/data', [
				'as' => 'agent.data',
				'uses' => 'AgentController@data_show'
		]);
	Route::get('backend/agent/create', [
				'as' => 'agent.create',
				'uses' => 'AgentController@create'
		]);
	Route::post('backend/agent/store', [
				'as' => 'agent.store',
				'uses' => 'AgentController@store'
		]);
	Route::get('backend/agent/edit/{id}', [
				'as' => 'agent.edit',
				'uses' => 'AgentController@edit'
		]);
	Route::post('/agent/update', [
			'as' => 'agent.update',
			'uses' => 'AgentController@update'
		]);
	Route::get('/backend/agent/delete/{id}', [
		'as' => 'agent.delete',
		'uses' => 'AgentController@destroy'
	]);
	// Route::get('/backend/agent/test/{id}', [
	// 	'as' => 'agent.test',
	// 	'uses' => 'AgentController@show_test'
	// ]);
	Route::get('/backend/agent/detail/{id}', [
		'as' => 'agent.detail',
		'uses' => 'AgentController@detail'
	]);
	Route::get('/backend/agent/topup/{id_history}/{id_agent}', [
		'as' => 'agent.topup',
		'uses' => 'AgentController@topup'
	]);
	Route::get('/backend/agent/topup/cancel/{id_history}/{id_agent}', [
		'as' => 'agent.topup_cancel',
		'uses' => 'AgentController@topup_cancel'
	]);
	Route::get('backend/agent/detail/show_topdown/{id}','AgentController@show_topdown');
	Route::get('backend/agent/detail/show_topup/{id}','AgentController@show_topup');
	Route::get('backend/agent/detail/member/{id}','AgentController@show_member');
	Route::get('backend/agent/detail/refund/{id}','AgentController@show_refund');
	
Route::get('backend/report','ReportController@index');
	Route::get('backend/report/sample',[
		'as' => 'backend.report',
		'uses' => 'ReportController@displayReport'
	]);
	// Route::get('/agent/data_detail/{id}', [
	// 			'as' => 'agent.data_detail',
	// 			'uses' => 'AgentController@data_show_detail'
	// 	]);
