<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// <tested>

Route::get('testing/testMemberArea', 'For_testing@testMemberArea');

Route::get('booking/confirm/{id_rsvn}/{idBHotel}/{id}', 'For_testing@cancelReservation');


Route::get('testing/getResponHotelDetail', 'For_testing@getResponHotelDetail');


Route::get('testing/getResponHotelSearch', 'For_testing@getResponHotelSearch');

Route::get('testing/getResponHotelTanpaRoom', 'For_testing@getResponHotelTanpaRoom');

Route::get('testing/getDataHotel', 'For_testing@getDataHotel');

Route::get('testing/getAllData', 'For_testing@getAllData');

Route::get('testing/rikuesBeberapaKamar', 'For_testing@rikuesBeberapaKamar');

Route::get('testing/rikuesSatuKamarSatuId', 'For_testing@rikuesSatuKamarDenganIDHotelParam');

Route::get('testing/testKueriFas', 'For_testing@testKueriFas');

Route::get('testing/sortFacility/{param}', 'For_testing@sortMixByStarFacilityPrice');

Route::get('testing/testingDuluYah', 'For_testing@testingDuluYah');


Route::get('testing/detailHotelnya', 'For_testing@getDetailHotelnya');



Route::get('testing/testerAjax', 'For_testing@tesRoomAjax');

Route::get('testing/jsonTest', 'FrontendController@testAjax');

Route::get('testing/cekAvail/{dewasa}/{cekin}/{cekout}/{mgcode}/{jumlahkamar}', 'For_testing@cekAvail');

Route::get('testing/cekrsv', 'For_testing@cekReservasi');

Route::get('testing/amend', 'For_testing@amendHotel');

Route::get('book', 'For_testing@bookKamar');

Route::get('acceptbook', 'For_testing@acceptBooking');

Route::post('testing/get_room_hotel', 'For_testing@get_room_hotel');

Route::get('testing/search_hotel','For_testing@search_hotel');

Route::any('testing/search_agent','For_testing@searchAgent');

Route::post('testing/search_agent_clone','For_testing@searchAgentClone');

Route::post('testing/search_all','For_testing@searchAgent_All');

Route::post('testing/search_all_clone','For_testing@searchAgent_All_clone');

Route::get('testing/APIS_getDetailHotel','For_testing@APIS_getDetailHotel');

Route::post('ServicesPHP/APIS_getDetailHotel', 'APIHotel_Korina@APIS_getDetailHotel');

Route::any('ServicesPHP/APIS_getDetailHotelFinal', 'APIHotel_Korina@APIS_getDetailHotelFinal');

Route::any('RequestAPI/getListHotel', 'APIHotel_Korina@CURL_jsonDataRequest');

Route::get('ServicesPHP/APIS_SearchHotelByDestination/{dewasa}/{cekin}/{cekout}/{mg_code}/{kamar}/{sumChild}/{ageJoin}','APIHotel_Korina@APIS_SearchHotelByDestination');

Route::get('ServicesPHP/CURL_jsonDataRequest3','APIHotel_Korina@CURL_jsonDataRequest3');

// Route::get('testing/cekAvailAlternative/{dewasa}/{cekin}/{cekout}/{mgcode}/{jumlahkamar}', 'For_testing@cekAvailAlternative');

Route::get('testing/cekAvailAlternative/{dewasa}/{cekin}/{cekout}/{mgcode}/{jumlahkamar}/{jumlahAnak}/{umurAnak}', 'For_testing@cekAvailAlternative');

Route::get('testing/SearchHotelByDestination','For_testing@SearchHotelByDestination');


// </tested>

// kuery change

Route::get('kueri/updateMarkup', 'For_testing@update_markup_price');




// </kuery change
Route::get('testRoom/', 'APIController@execKuery');

Route::get('get_all_data/', 'APIController@getAllDataToDB');

Route::get('execute_all_data/', 'APIController@executeAllData');

Route::get('getCurr/', 'APIController@getCurrency');



Route::get('listHotel/', 'APIController@list_hotel');

Route::get('testing/getData', 'For_testing@CURL_jsonDataRequest');

Route::get('testing/ViewCancelPolicy','For_testing@ViewCancelPolicy');
Route::get('testing/GetCancelPolicy','For_testing@GetCancelPolicy');







