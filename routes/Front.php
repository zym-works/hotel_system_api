<?php
/**
 * Created by PhpStorm.
 * User: Korina Developer
 * Date: 12/5/2017
 * Time: 9:34 AM
 */
Route::get('/', 'FrontendController@index');
Route::get('homeFront', function () {
    return view('frontend.home');
});
Route::get('pay', function () {
    return view('citytour.payment_hotel');
});

Route::get('testTemplate', function () {
    return view('citytour.testing');
});


Route::get('detail', function () {
    return view('citytour.detail');
});

Route::get('hargaMulai/', 'FrontendController@price_starting_from');

Route::get('hotel/booking', function () {
    session_start();
    $sesi=session('login_user');
     $hotel = DB::table('hotel')
        ->select('id')
        ->where('id_from_mg',$_SESSION['dataOrder'][8])
        ->first();
    // $sesi=session('login_user');
    // if(empty($sesi)){
    //         return redirect('/')->with('alert-warning', 'Anda harus login dulu');
    //     }
    return view('citytour.payment_hotel')->with('hotel',$hotel)->with('session_user',$sesi);
});

Route::get('hotel/finish/{id_res}', 'FrontendController@hotelFinish');

Route::post('hotel/post-pay', 'For_testing@postPayHotel');

Route::get('list/hotel/bangkok', 'FrontendController@list_hotel');

Route::get('list/hotel/bangkok/{dewasa}/{cekin}/{cekout}/{mg_code}/{kamar}/{sumChild}/{ageJoin}', 'FrontendController@list_hotel_avail');

Route::get('list/hotel/bangkok/{orderBy}', 'FrontendController@list_hotel_orderBy');

Route::post('hotel/booking', 'For_testing@hotelBook');

Route::get('detail_hotel/', 'FrontendController@detail_hotel');

Route::get('hotel/', 'FrontendController@home');

Route::get('testAjax/', 'FrontendController@testAjax');

Route::get('testAjax2/{tgl}', 'FrontendController@testAjax2');

Route::get('detail/hotel/{hotel_id}-{hotel_slug}.html', 'FrontendController@detail_hotel');

// Route::get('detail/hotel/{hotel_id}-{hotel_slug}.html/{param}', 'FrontendController@detail_hotel');

Route::get('detail/hotel/', 'FrontendController@detail_hotel');

Route::get('delete/', 'FrontendController@delete_semua');

Route::get('lihat_room/', 'FrontendController@detail_room_hotel');

Route::get('ajax/sort/{param}', 'FrontendController@sortMixByStarFacilityPrice');
Route::get('ajax2/sort/{param}', 'FrontendController@sortMixByStarFacilityPrice2');

Route::get('ajax/byRate/{val}','FrontendController@orderByRate');
Route::get('ajax/byPrice/{val}','FrontendController@orderByPrice');
Route::get('ajax/byFacility/{param}','FrontendController@orderByFacilty');

Route::post('/agent/signin', [
			'as' => 'agent.signin',
			'uses' => 'FrontendController@agent_signin'
		]);

Route::get('/agent', [
			'as' => 'agent.home',
			'uses' => 'FrontendController@agent_home'
		])->middleware('SystemRuleUser');

Route::get('/agent/logout', function() {
	Session::flush();
	  if(!Session::has('login_user'))
	   {
		   return redirect('/');
	   }
	 });

Route::get('/register', function () {
    return view('citytour.register');
});

Route::post('/register/user', [
			'as' => 'agent.register',
			'uses' => 'FrontendController@register'
		]);

//Route::get('/hotelnya', 'For_testing@getDetailHotelnya');
Route::post('/agent/top_up', 'FrontendController@top_up');
Route::get('/agent/topup/data', 'FrontendController@data_topup_show');
Route::get('/agent/topdown/data', 'FrontendController@data_topdown_show');
Route::get('/agent/member/data', 'FrontendController@data_member_show');
Route::get('/agent/member/add', function () {
	$sessionlogin=session('login_user');

        if(empty($sessionlogin)){
           return view('citytour.home');
            //return 'login dulu';
        }
    return view('citytour.create_member')->with('session_user',$sessionlogin);
});
Route::post('/agent/member/create', 'FrontendController@create_member');

Route::get('search',array('as'=>'search','uses'=>'FrontendController@search'));
Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'FrontendController@autocomplete'));

Route::get('get/currency/{from}/{to}/{value}','For_testing@convertPrice');









